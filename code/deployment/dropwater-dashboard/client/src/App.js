import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect, withRouter } from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import setAuthToken from './utils/setAuthToken';

import { setCurrentUser, logoutUser } from './actions/authActions';
import { Provider } from 'react-redux';
import store from './store';

import FloatingActionButton from './components/layout/FloatingActionButton';
import Register from './components/auth/Register';
import Login from './components/auth/Login2';
import PrivateRoute from './components/private-route/PrivateRoute';
import Overview from './components/overview/Overview';
import Header from './components/layout/Header';
import Landing from './components/layout/Landing';
import './App.css';
import Userlist from './components/overview/Userlist';
import OrderList from './components/overview/OrderList';
import UserProfile from './components/profiles/UserProfile';
import Reports from './components/reports/Reports';
import FreeOrders from './components/orders/FreeOrders';
import PaidOrders from './components/orders/PaidOrders';
import AllOrders from './components/orders/AllOrders';
import NavDrawer from './components/drawer/NavDrawer2';

import InventoryList from './components/overview/InventoryList';
import OrderCreate from './components/kiosk_order/OrderCreate';
import Kiosk from './components/kiosk/Kiosk';
import ErrorBoundary from './components/error/ErrorBoundary';
import MiscTests from './components/misc/MiscTests';
import { connectWSTest } from './wsTest';
import { withStyles } from '@material-ui/core/styles';

import * as Sentry from '@sentry/react';
import { Integrations } from '@sentry/tracing';

import LogRocket from 'logrocket';
LogRocket.init('cwkbbm/dashboard');

const getEnvironment = () => {
	switch (process.env.ENV) {
		case 'dev':
			// code block
			console.log('Setting up dev');
			return 'development';
		case 'staging':
			// code block
			console.log('Setting up staging');
			return 'staging';
		case 'prod':
			console.log('Setting up prod');
			return 'production';
		default:
			return 'development';
	}
};

Sentry.init({
	dsn: 'https://6071d902eb2f4d4caf7584eac955ed1e@o445416.ingest.sentry.io/5421785',
	release: 'dropwater-dashboard@' + process.env.npm_package_version,
	integrations: [ new Integrations.BrowserTracing() ],
	tracesSampleRate: process.env.DEBUG === 'true' ? 1.0 : 0.1,
	environment: getEnvironment()
});

// Check for token to keep user logged in
if (localStorage.jwtToken) {
    console.log('Localstorage: ' + JSON.stringify(localStorage));
	// Set auth token header auth
	const token = localStorage.jwtToken;
	setAuthToken(token);
	// Decode token and get user info and exp
	const decoded = jwt_decode(token);
	// Set user and isAuthenticated
	store.dispatch(setCurrentUser(decoded));
	// Check for expired token
	const currentTime = Date.now() / 1000; // to get in milliseconds
	if (decoded.exp < currentTime) {
		// Logout user
		store.dispatch(logoutUser());

		// Redirect to login
		window.location.href = './';
	}
} else {
    console.log('Localstorage without: ' + JSON.stringify(localStorage))
}

const LoginContainer = () => (
	<div style={{ height: "100vh", backgroundColor: '#E5E5E5' }}>
		{/* <Navbar /> */}
		{/* <Header /> */}

		<Route exact path="/" component={Login} />
		{/* <Route exact path="/" render={() => <Redirect to="/login" />} /> */}
		<Route exact path="/register" component={Register} />
		<Route exact path="/login" component={Login} />
		<Route exact path="/create" component={OrderCreate} />
		<Route exact path="/additive_tests" component={MiscTests} />
		<Route exact path="/create/:kiosk_nickname" component={OrderCreate} />
		{/* <Route exact path="*" render={() => <Redirect to="/" />} /> */}
	</div>
);

const drawerWidth = 200;

const useStyles = (theme) => ({
	root: {
		display: 'flex'
	},
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
		transition: theme.transitions.create([ 'width', 'margin' ], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen
		})
	},
	appBarShift: {
		marginLeft: drawerWidth,
		width: `calc(100% - ${drawerWidth}px)`,
		transition: theme.transitions.create([ 'width', 'margin' ], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen
		})
	},
	menuButton: {
		marginRight: 36
	},
	hide: {
		display: 'none'
	},
	drawer: {
		width: drawerWidth,
		flexShrink: 0,
		whiteSpace: 'nowrap'
	},
	drawerOpen: {
		width: drawerWidth,
		transition: theme.transitions.create('width', {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen
		})
	},
	drawerClose: {
		transition: theme.transitions.create('width', {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen
		}),
		overflowX: 'hidden',
		width: theme.spacing(7) - 1,
		[theme.breakpoints.up('sm')]: {
			width: theme.spacing(9) - 1
		}
	},
	toolbar: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'flex-end',
		padding: theme.spacing(0, 1),
		// necessary for content to be below app bar
		...theme.mixins.toolbar
	},
	content: {
		flexGrow: 1,
		padding: theme.spacing(3)
	}
});

const DefaultContainer = () => (
	<div>
		{/* <div style={{ overflow: "hidden", position: "absolute", width: "100%" }}>
          <svg viewBox="0 0 1300 170" preserveAspectRatio="none" style={{ width: "100%", height: "100%", backgroundSize: "100% 100%"}}>
            <path d="M0.00,49.98 C149.99,150.00 271.49,-49.98 500.00,49.98 L500.00,0.00 L0.00,0.00 Z" style={{ stroke: "none", fill: "#08f" }}></path>
          </svg>
        </div> */}
		{/* <Header />
		<NavBarOpus /> */}
		{/* <NavDrawer /> */}
		<PrivateRoute exact path="/overview" component={Overview} />
		<PrivateRoute exact path="/dashboard" render={() => <Redirect to="/overview" />} />
		<PrivateRoute exact path="/all_users" component={Userlist} />
		<PrivateRoute exact path="/orders/" render={() => <Redirect to="/orders/paid" />} />
		<PrivateRoute exact path="/orders/paid" component={PaidOrders} />
		<PrivateRoute exact path="/orders/free" component={FreeOrders} />
		<PrivateRoute exact path="/orders/all" component={AllOrders} />
		<PrivateRoute exact path="/all_orders" component={OrderList} /> {/*  to be deprecated */}
		<PrivateRoute exact path="/inventory" component={InventoryList} />
		<Route exact path="/create" component={OrderCreate} />
		{/* <Route exact path="/additive_tests" component={ MiscTests } /> */}
		<Route exact path="/create/:kiosk_nickname" component={OrderCreate} />
		<ErrorBoundary>
			<PrivateRoute exact path="/kiosk/:uuid" component={Kiosk} />
			<PrivateRoute exact path="/kiosk/" render={() => <Redirect to="/overview" />} />
			<PrivateRoute exact path="/account/:uuid" component={UserProfile} />
			<PrivateRoute exact path="/account/" render={() => <Redirect to="/overview" />} />
			{/* <PrivateRoute exact path="/profile/:uuid" component={ UserProfile } /> */}

			<PrivateRoute exact path="/reports" component={Reports} />
		</ErrorBoundary>
		<Route
			path="/about-us"
			component={() => {
				window.location.href = 'https://dropwater.co';
				return null;
			}}
		/>
		<FloatingActionButton />
		{/* <NavigationBar /> */}
		{/* <Sidebar items={items} /> */}
		{/* <NavBar /> */}
		{/* <Sidebar /> */}
	</div>
);

class App extends Component {
	render() {
		const { classes, theme } = this.props;

		// console.log("Main classes: " + JSON.stringify(this.props.theme))
		return (
			<Provider store={store} >
				<Router>
					<div className={classes.root} style={{ backgroundColor: '#E5E5E5', height: '100vh', overflowX:
					'auto'
			}}>
						<NavDrawer classes={classes} theme={theme} store={store} />
						<main className={classes.content} style={{ backgroundColor: '#E5E5E5'
			}}>
							<div className={classes.toolbar} />
							<div className="App" style={{ backgroundColor: '#E5E5E5'
			}}>
								<Switch>
									{/* <PageTransition preset="moveToLeftFromRight" transitionKey={store.path}> */}
									<Route exact path="/login" component={LoginContainer} />
									<Route exact path="/" component={LoginContainer} />
									<Route exact path="/register" component={LoginContainer} />
									{/* <Route exact path="/register" component={Register} /> */}

									<Route exact path="/createorder" component={LoginContainer} />
									<Route component={DefaultContainer} />
									{/* <Route exact path="/home" render={() => (window.location = "https://dropwater.co")} /> */}
									{/* <Route exact path="/home" render={() => <Redirect to="/overview" />} /> */}
									<Route exact path="*" render={() => <Redirect to="/" />} />
								</Switch>
							</div>
						</main>
					</div>
				</Router>
			</Provider>
		);
	}
}
// export default App;
export default withStyles(useStyles, { withTheme: true })(App);
