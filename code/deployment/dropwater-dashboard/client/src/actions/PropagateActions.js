import axios from "axios";
import { GET_ERRORS, KIOSK_DETAILS, KIOSK_ID } from "./types";

// Load al users
export const loadKioskDetails = (kioskNickname) => (dispatch) => {
  axios
    .post("/api/kiosk/info/", kioskNickname)
    .then((res) => {
      dispatch(loadKioskDetailsHelper(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      })
    );
};

export const loadKioskDetailsHelper = (details) => {
  return {
    type: KIOSK_DETAILS,
    payload: details,
  };
};

export const authenticateKiosk = (kioskAlias) => (dispatch) => {
  axios
    .post("/api/kiosk/authenticate/", kioskAlias)
    .then((res) => {
      dispatch(authenticateKioskHelper(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      })
    );
};

export const authenticateKioskHelper = (sent) => {
  return {
    type: KIOSK_ID,
    payload: sent,
  };
};
