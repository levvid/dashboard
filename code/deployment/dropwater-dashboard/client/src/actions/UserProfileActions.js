import axios from "axios";
import { GET_ERRORS, USER_PROFILE } from "./types";

export const loadUserProfile = (filter) => (dispatch) => {
  axios
    .post("/api/users/account", filter)
    .then((res) => {
      dispatch(getUserProfile(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: null,
      })
    );
};

export const getUserProfile = (userProfile) => {
  console.log("User account: " + JSON.stringify(userProfile));
  return {
    type: USER_PROFILE,
    payload: userProfile,
  };
};


export const saveUserProfile = (filter) => (dispatch) => {
  axios
    .post("/api/users/account/save", filter)
    .then((res) => {
      dispatch(saveUserAccount(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: null,
      })
    );
};

export const saveUserAccount = (userProfile) => {
  console.log("User account: " + JSON.stringify(userProfile));
  return {
    type: USER_PROFILE,
    payload: userProfile,
  };
};


export const updateUserPassword = (filter) => (dispatch) => {
  axios
    .post("/api/users/account/password", filter)
    .then((res) => {
      dispatch(updatePassword(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: null,
      })
    );
};

export const updatePassword = (userProfile) => {
  console.log("User account: " + JSON.stringify(userProfile));
  return {
    type: USER_PROFILE,
    payload: userProfile,
  };
};