import axios from "axios";
import { GET_ERRORS, INVENTORY } from "./types";

export const loadInventoryList = (dateRange) => (dispatch) => {
  axios
    .post("/api/inventory/inventory", dateRange)
    .then((res) => {
      dispatch(inventoryData(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      })
    );
};

export const inventoryData = (inventory_data) => {
  return {
    type: INVENTORY,
    payload: inventory_data,
  };
};

