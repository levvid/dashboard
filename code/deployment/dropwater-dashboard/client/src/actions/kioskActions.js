import axios from "axios";
import { GET_ERRORS, KIOSK } from "./types";
import { KIOSK_DETAILS } from "./types";

// Load al users
export const loadKioskDetails = (filter) => (dispatch) => {
  axios
    .post("/api/kiosk/info", filter)
    .then((res) => {
      dispatch(loadKioskDetailsHelper(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      })
    );
};

export const loadKioskDetailsHelper = (details) => {
  return {
    type: KIOSK_DETAILS,
    payload: details,
  };
};

export const loadKiosk = (filter) => (dispatch) => {
  axios
    .post("/api/kiosk/kiosk", filter)
    .then((res) => {
      dispatch(getKioskDataObject(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: null,
      })
    );
};

export const getKioskDataObject = (kioskData) => {
  console.log("Kiosk actions: " + JSON.stringify(kioskData));
  return {
    type: KIOSK,
    payload: kioskData,
  };
};



