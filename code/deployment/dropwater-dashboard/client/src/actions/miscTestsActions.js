import axios from "axios";
import { GET_ERRORS, MISC } from "./types";

export const startAdditiveDispense = (filters) => (dispatch) => {
  axios
    .post("/api/misc_tests/additive_tests/", filters)
    .then((res) => {
      dispatch(miscData(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      })
    );
};

export const miscData = (misc_data) => {
  return {
    type: MISC,
    payload: misc_data,
  };
};

export const getCounter = () => (dispatch) => {
  axios
    .get("/api/misc_tests/additive_counter/")
    .then((res) => {
      dispatch(counterHelper(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      })
    );
};

export const counterHelper = (misc_data) => {
  return {
    type: MISC,
    payload: misc_data,
  };
};

