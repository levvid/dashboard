import axios from "axios";
import { GET_ERRORS, ALL_ORDERS } from "./types";

// Load all orders
export const loadAllOrders = (dateRange) => (dispatch) => {
  axios
    .post("/api/orders/all/", dateRange)
    .then((res) => {
      dispatch(loadOrders(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      })
    );
};

export const loadOrders = (orders) => {
  return {
    type: ALL_ORDERS,
    payload: orders,
  };
};



export const loadPaidOrders = (dateRange) => (dispatch) => {
  axios
    .post("/api/orders/paid/", dateRange)
    .then((res) => {
      dispatch(loadPaidOrdersHelper(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      })
    );
};

export const loadPaidOrdersHelper = (orders) => {
  return {
    type: ALL_ORDERS,
    payload: orders,
  };
};


export const loadFreeOrders = (dateRange) => (dispatch) => {
  axios
    .post("/api/orders/free/", dateRange)
    .then((res) => {
      dispatch(loadfreeOrdersHelper(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      })
    );
};

export const loadfreeOrdersHelper = (orders) => {
  return {
    type: ALL_ORDERS,
    payload: orders,
  };
};

