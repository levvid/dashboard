import axios from "axios";
import { GET_ERRORS, ORDER_CREATE} from "./types";

export const createOrder = (orderDetails) => (dispatch) => {
  axios
    .post("/api/orders/create/", orderDetails)
    .then((res) => {
      dispatch(createOrderHelper(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err,
      })
    );
};

export const createOrderHelper = (order_details) => {
  console.log("Order create actions: " + JSON.stringify(order_details));
  return {
    type: ORDER_CREATE,
    payload: order_details,
  };
};