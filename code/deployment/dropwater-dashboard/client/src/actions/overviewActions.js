import axios from "axios";
import { GET_ERRORS, DASHBOARD_METRICS} from "./types";

export const loadDashboardMetrics = (dateRange) => (dispatch) => {
  axios
    .post("/api/overview/overview", dateRange)
    .then((res) => {
      dispatch(dashBoardMetrics(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      })
    );
};

export const dashBoardMetrics = (dashboard_metrics) => {
  return {
    type: DASHBOARD_METRICS,
    payload: dashboard_metrics,
  };
};

