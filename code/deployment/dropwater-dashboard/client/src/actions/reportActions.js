import axios from "axios";
import { GET_ERRORS, REPORTS } from "./types";

// Load al users
export const loadReports = (filter) => (dispatch) => {
  axios
    .post("/api/reports/reports", filter)
    .then((res) => {
      dispatch(loadReportsHelper(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      })
    );
};

export const loadReportsHelper = (details) => {
    console.log("reports actions called. ")
  return {
    type: REPORTS,
    payload: details,
  };
};

