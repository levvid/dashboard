import axios from "axios";
import { GET_ERRORS, USER_INTERACTIONS, USER_INTERACTION_VIDEO } from "./types";

export const loadUserInteractionsList = (dateRange) => (dispatch) => {
  axios
    .post("/api/user_interactions/interactions", dateRange)
    .then((res) => {
      dispatch(userInteractionData(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      })
    );
};

export const userInteractionData = (user_interaction_data) => {
  return {
    type: USER_INTERACTIONS,
    payload: user_interaction_data,
  };
};


export const loadUserInteractionVideo = (s3URL) => (dispatch) => {
  process.env.DEBUG === "true" && console.log("Dispatch: " + JSON.stringify(s3URL.s3URL));
  axios
    .post("/api/user_interactions/interaction_video", s3URL)
    .then((res) => {
      dispatch(userInteractionVideo(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      })
    );
};

export const userInteractionVideo = (user_interaction_video) => {
  return {
    type: USER_INTERACTION_VIDEO,
    payload: user_interaction_video,
  };
};

