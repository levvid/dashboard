import axios from "axios";
import { GET_ERRORS, ALL_USERS, EMAIL_QRCODE } from "./types";

// Load al users
export const loadAllUsers = () => (dispatch) => {
  axios
    .get("/api/users/all_users")
    .then((res) => {
      dispatch(loadUsers(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      })
    );
};

export const loadUsers = (users) => {
  return {
    type: ALL_USERS,
    payload: users,
  };
};

export const emailQRCode = (userUuid) => (dispatch) => {
  axios
    .post("/api/users/email_qrcode/", userUuid)
    .then((res) => {
      // dispatch(emailQRCode(res.data));
    })
    .catch((err) =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      })
    );
};

export const emailSent = (sent) => {
  return {
    type: EMAIL_QRCODE,
    payload: sent,
  };
};
