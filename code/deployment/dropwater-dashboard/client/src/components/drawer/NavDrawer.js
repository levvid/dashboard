import React from 'react';
import clsx from 'clsx';
import { createStyles, makeStyles, useTheme, Theme } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Drawer from '@material-ui/core/Drawer';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import DashboardIcon from '@material-ui/icons/Dashboard';
import logoImage from '../../images/drop_logo_white_shadow.png';
import { Link } from 'react-router-dom';
import header_img from '../../images/wave-header@2x.png';
import store from '../../store';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch, Redirect, withRouter, useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';


import OrdersIcon from '@material-ui/icons/FormatListNumberedRtl';
import InventoryIcon from '@material-ui/icons/Store';
import AccountIcon from '@material-ui/icons/AccountCircle';
import Logout from '@material-ui/icons/ExitToApp';
import CreateIcon from '@material-ui/icons/AddCircleOutline';
import AboutUsIcon from '@material-ui/icons/PeopleAlt';
import ReportsIcon from '@material-ui/icons/LibraryBooks';
import PaidOrdersIcon from '@material-ui/icons/AttachMoney';
import FreeOrdersIcon from '@material-ui/icons/MoneyOff';
import AllOrdersIcon from '@material-ui/icons/Ballot';





import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';



import App from '../../App';
import Header from '../layout/Header';
import { logoutUser } from '../../actions/authActions';


const drawerWidth = 200;


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    // offset: theme.mixins.toolbar,
    root: {
      display: 'flex',
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: 'none',
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: 'nowrap',
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: 'hidden',
      width: theme.spacing(7) - 1,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(9) - 1,
      },
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);

const _onListItemClick = (history, text, event) => {
    console.log("Clicked");
    console.log(text)
    history.push('/' + text.toLowerCase());
      
}


const _getLink = (index, menu, subindex=0) => {
    if(menu === 1) {  //menu 1
        if(index === 0) {
            // return <ListItemIcon><DashboardIcon /></ListItemIcon>;
            return '/overview';
        } else if(index === 1) {
          if(subindex === 0) {
            return '/orders/paid';
          } else if(subindex === 1) {
            return '/orders/free';
          } else if(subindex === 2) {
            return '/orders/all';
          }
        } else if(index === 2) {
            return  '/inventory';
        }
    }  else if(menu === 2) {
        if(index === 0) {
            return '/create/';
        } 
    }   else if(menu === 3) {
        if(index === 0) {
            let uuid = localStorage.getItem("uuid");
            console.log("Get uuid from localStorage: " + uuid);
            if (uuid) {
              // _handleShowMenu(true);

            }
            
            return '/account/' + uuid;
        } else if (index === 1) {
            return '/reports';
        }
    }
    
}

const _getIcon = (index, menu, subindex=0) => {
    if(menu === 1) {
        if(index === 0) {
            // return <ListItemIcon><DashboardIcon /></ListItemIcon>;
            return <ListItemIcon><DashboardIcon style={{ color: "#00d2fa" }}/></ListItemIcon>;
        } else if(index === 1) {
            if(subindex === 0) {
              return  <ListItemIcon><PaidOrdersIcon style={{ color: "#00d2fa" }}/></ListItemIcon>;
            } else if(subindex === 1) {
              return  <ListItemIcon><FreeOrdersIcon style={{ color: "#00d2fa" }}/></ListItemIcon>;
            } else if(subindex === -1) {
              return  <ListItemIcon><OrdersIcon style={{ color: "#00d2fa" }}/></ListItemIcon>;
            } else if(subindex === 2) {
              return  <ListItemIcon><AllOrdersIcon style={{ color: "#00d2fa" }}/></ListItemIcon>;
            }
            
        } else if(index === 2) {
            return  <ListItemIcon><InventoryIcon style={{ color: "#00d2fa" }}/></ListItemIcon>;
        }
    } else if(menu === 2) {
        if(index === 0) {
            // return <ListItemIcon><DashboardIcon /></ListItemIcon>;
            return <ListItemIcon><CreateIcon style={{ color: "#00D2FA" }}/></ListItemIcon>;
        } 
    } else if(menu === 3) {
        if(index === 0) {
            // return <ListItemIcon><DashboardIcon /></ListItemIcon>;
            return <ListItemIcon><AccountIcon style={{ color: "#00D2FA" }}/></ListItemIcon>;
        } else if (index === 1) {
            return <ListItemIcon><ReportsIcon style={{ color: "#00D2FA" }}/></ListItemIcon>;
        }
    } else if (menu === 4) {
        if (index === 0) {
            return <ListItemIcon><Logout style={{ color: "#00D2FA" }}/></ListItemIcon>;
        }
    }
    
}


function HideOnScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({ target: window ? window() : undefined });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

HideOnScroll.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default function NavDrawer(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [active, setActive] = React.useState('//TODO');
  const [orders_open, setOrdersOpen] = React.useState(false);
  const menu1 = ['Overview', 'Orders', 'Inventory'];
  const menu2 = ['Create Order'];
  // const menu3 = ['Account', 'Reports'];
  const menu3 = ['Account'];
  const menu4 = ['Logout'];
  console.log("Checking this...");
  
  const [showMenu, setShowMenu] = React.useState(true);


  const orders_submenu = ["Paid", "Free", 'All'];

  
  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const _handleOrdersOpen = () => {
    setOrdersOpen(!orders_open);
  };

  const _handleShowMenu = () => {
    let uuid = localStorage.getItem("uuid");
    if(uuid) {
      console.log("Setting uuid ...");
      setShowMenu(true);
    }
    
  }

  const handleClick = (text) => {
      console.log("Setting active: " + JSON.stringify(text));
    //   setActive(text.text);
  }

  const _logOut = () => {
    // Logout user
	store.dispatch(logoutUser());
    setActive('Logout')
		// Redirect to login
	window.location.href = './';
}

// _handleShowMenu();
  
  return (
    // <Provider store={store}>
    <Provider store={store}>
		<Router>
        <div className={classes.root}>
        <Header />
        <CssBaseline />
        {/* { localStorage.getItem('uuid') && localStorage.getItem('role') &&  (
            <> */}
        { localStorage.getItem("uuid") && (
          <>
          <HideOnScroll {...props}>
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                [classes.appBarShift]: open,
                })}
                style={{ backgroundColor: "transparent", boxShadow: "none" }}
            >
                {/* <Header /> */}
                <Toolbar>
                <IconButton
                    color="primary"
                    aria-label="open drawer"
                    onClick={handleDrawerOpen}
                    edge="start"
                    className={clsx(classes.menuButton, {
                    [classes.hide]: open,
                    })}
                >
                    <MenuIcon />
                </IconButton>
                <div className="col s2">
                    <img
                        className="logo-image"
                        src={logoImage}
                        width="200"
                        style={{
                            marginTop: '9px',
                            marginBottom: '0px',
                            marginLeft: '-5%',
                            overflow: "hidden"
                        }}
                        alt="Logo"
                    />
                </div>
                </Toolbar>
                {/* <Header /> */}
            </AppBar>
            </HideOnScroll>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open,
                })}
                classes={{
                paper: clsx({
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                }),
                }}
                onMouseOver={handleDrawerOpen}
                onMouseLeave={handleDrawerClose}
                anchor="left"
                id="drawer"
                // className={classes.drawer}
                // variant="permanent"
                // classes={{
                //     paper: classes.drawerPaper,
                // }}
                // anchor="left"
                

            >
                <div className={classes.toolbar} >
                { open && (
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                    </IconButton>
                )}
                
                </div>
                <Divider />
                <List  style={{ backgroundColor: "transparent !important" }}>
                {menu1.map((text, index) => (
                  <>
                  { index === 1 && (
                    <>
                    <ListItem button onClick={_handleOrdersOpen}>
                      <ListItemIcon>
                        {_getIcon(index, 1, -1)}
                      </ListItemIcon>
                      <ListItemText primary="Orders" />
                      {orders_open ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={orders_open} unmountOnExit>
                      <List component="div" disablePadding>
                        {orders_submenu.map((subtext, subindex) => (
                          <Link to={_getLink(index, 1, subindex)} style={{ color: "#000000" }} onClick={() => handleClick({text})}>
                            <ListItem button className="" style={{ "marginLeft": "1em" }}>
                              {_getIcon(index, 1, subindex)}
                              <ListItemText primary={subtext} />
                            </ListItem>
                          </Link>
                        ))}
                      </List>
                    </Collapse>
                    </ >
                  )}
                  { index !== 1 && (
                    <Link to={_getLink(index, 1)} style={{ color: "#000000" }} onClick={() => handleClick({text})}>
                        <ListItem button key={text} style={{ backgroundColor: `${ active === text ? '#DCDCDC': '' }` }}>
                            {_getIcon(index, 1)}
                            <ListItemText>
                            {text}
                            </ListItemText>
                        </ListItem>
                    </Link>
                  )}
                    
                    </ >
                ))}
                </List>

                <Divider />
                <List>
                {menu2.map((text, index) => (
                    <Link to={_getLink(index, 2)} style={{ color: "#000000"}} onClick={() => handleClick({text})}>
                        <ListItem button key={text} style={{ backgroundColor: `${ active === text ? '#DCDCDC': '' }` }}>
                            {_getIcon(index, 2)}
                            <ListItemText>
                            {text}
                            </ListItemText>
                        </ListItem>
                    </Link>
                ))}
                </List>

                <Divider />
                <List>
                {menu3.map((text, index) => (
                    <Link to={_getLink(index, 3)} style={{ color: "#000000"}} onClick={() => handleClick({text})}>
                        <ListItem button key={text} style={{ backgroundColor: `${ active === text ? '#DCDCDC': '' }` }}>
                            {_getIcon(index, 3)}
                            <ListItemText>
                            {text}
                            </ListItemText>
                        </ListItem>
                    </Link>
                ))}
                <a href="https://dropwater.co">
                        <ListItem button key="about-us" style={{ color: "#000000", backgroundColor: `${ active === 'About Us' ? '#DCDCDC': '' }` }} onClick={() => handleClick('About Us')}>
                            <ListItemIcon><AboutUsIcon style={{ color: "#00d2fa" }}/></ListItemIcon>
                            <ListItemText>
                            {`About Us`}
                            </ListItemText>
                        </ListItem>
                    </a>
                </List>

                <Divider />
                <List>
                {menu4.map((text, index) => (
                    <ListItem button key={text} onClick={() => _logOut()}>
                        {_getIcon(index, 4)}
                        <ListItemText>
                        {text}
                        </ListItemText>
                    </ListItem>
                ))}
                </List>
            </Drawer>
            </ >
        )}
        <main className={classes.content}>
            <div className={classes.toolbar} />
            {/* <Typography paragraph>
            </Typography> */}
            <App />
            
        </main>
        
        </div>
        </Router>
    </Provider>
  );
}


// export default withRouter(NavDrawer);
