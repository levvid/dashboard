import React, { Component } from 'react';
import clsx from 'clsx';
import { createStyles, makeStyles, useTheme, Theme } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Drawer from '@material-ui/core/Drawer';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import DashboardIcon from '@material-ui/icons/Dashboard';
import logoImage from '../../images/img.png';
import { Link } from 'react-router-dom';
import header_img from '../../images/wave-header@2x.png';
import store from '../../store';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch, Redirect, withRouter, useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';

import InputBase from '@material-ui/core/InputBase';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/PermIdentity';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';


import OrdersIcon from '@material-ui/icons/FormatListNumberedRtl';
import InventoryIcon from '@material-ui/icons/Store';
import AccountIcon from '@material-ui/icons/AccountCircle';
import Logout from '@material-ui/icons/ExitToApp';
import CreateIcon from '@material-ui/icons/AddCircleOutline';
import AboutUsIcon from '@material-ui/icons/PeopleAlt';
import ReportsIcon from '@material-ui/icons/LibraryBooks';
import PaidOrdersIcon from '@material-ui/icons/AttachMoney';
import FreeOrdersIcon from '@material-ui/icons/MoneyOff';
import AllOrdersIcon from '@material-ui/icons/Ballot';





import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';
import styles from '../../css/nav_drawer.css';


import App from '../../App';
import Header from '../layout/Header';
import { logoutUser } from '../../actions/authActions';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';


const drawerWidth = 200;

function HideOnScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({ target: window ? window() : undefined });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

const useStyles = theme => ({
    root: {
      display: 'flex',
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: 'none',
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: 'nowrap',
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: 'hidden',
      width: theme.spacing(7) - 1,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(9) - 1,
      },
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  });

class NavDrawer2 extends Component {
	constructor(props) {
		super(props);
		this.state = {
            open: false,
            active: 'overview',
            orders_open: false,
            showMenu: true,
            uuid: '',
            anchorEl: null,
            mobileMoreAnchorEl: null,
    };

    // this.handleToggle = this.handleToggle.bind(this);
    // this.handleOutsideClick = this.handleOutsideClick.bind(this);
    this._getIcon = this._getIcon.bind(this);
    this._getLink = this._getLink.bind(this);
    this.handleDrawerOpen = this.handleDrawerOpen.bind(this);
    this.handleDrawerClose = this.handleDrawerClose.bind(this);
    this._handleOrdersOpen = this._handleOrdersOpen.bind(this);
    this._logOut = this._logOut.bind(this);
    
	}
	

  
  componentDidMount() {

    // window.addEventListener('resize', this.updateWindowDimensions);
    // console.log("Updating..." + JSON.stringify(this.props.auth));
    // var uuid = this.props.match.params;
    console.log("UUID: " + JSON.stringify(window.location.href));
    let active = 'overview';
    let pageLink = window.location.href;

    if(pageLink.includes('overview')) {
      active = 'overview';
    } else if(pageLink.includes('orders/paid')) {
      active = 'paid';
    } else if(pageLink.includes('orders/free')) {
      active = 'free';
    } else if(pageLink.includes('orders/all')) {
      active = 'all';
    } else if(pageLink.includes('create')) {
      active = 'create';
    } else if(pageLink.includes('account')) {
      active = 'account';
    }

    console.log("UUID: " + active);

    this.setState({
      uuid: this.props.auth.user.uuid,
      active: active,
    })
  }


  handleDrawerOpen = () => {
    this.setState({
        open: true
    })
  };

  handleDrawerClose = () => {
    this.setState({
        open: false
    });
  };

  _handleOrdersOpen = () => {
    this.setState({
        orders_open: !this.state.orders_open
    })
  };

  _logOut = (e) => {
		e.preventDefault();
		// this.props.logoutUser();
    this.props.store.dispatch(logoutUser());
    // // setActive('Logout')
		// // Redirect to login
	  window.location.href = './';
  };


  _getLink = (index, menu, subindex=0) => {
    if(menu === 1) {  //menu 1
        if(index === 0) {
            // return <ListItemIcon><DashboardIcon /></ListItemIcon>;
            return '/overview';
        } else if(index === 1) {
          if(subindex === 0) {
            return '/orders/paid';
          } else if(subindex === 1) {
            return '/orders/free';
          } else if(subindex === 2) {
            return '/orders/all';
          }
        } else if(index === 2) {
            return  '/inventory';
        }
    }  else if(menu === 2) {
        if(index === 0) {
            return '/create/';
        } 
    }   else if(menu === 3) {
        if(index === 0) {
            // let uuid = localStorage.getItem("uuid");
            // console.log("Get uuid from localStorage: " + uuid);
            // if (uuid) {
            //   // _handleShowMenu(true);

            // }
            
            return '/account/' + this.state.uuid;
        } else if (index === 1) {
            return '/reports';
        }
    }
    
}

 handleProfileMenuOpen = (event) => {
    this.setState({
        anchorEl: event.currentTarget
    });
  };

  handleMobileMenuClose = () => {
    this.setState({
        mobileMoreAnchorEl: null
    })
  };

  handleMenuClose = () => {
    this.setState({
        anchorEl: null,
        mobileMoreAnchorEl: null
    });
//    handleMobileMenuClose();
  };

  handleMobileMenuOpen = (event) => {
        this.setState({
            mobileMoreAnchorEl: event.currentTarget
        });
  };


handleClick = (text) => {
    console.log("Setting active: " + JSON.stringify(text));
    this.setState({
      active: text.toLowerCase()
    });
}

_getIcon = (index, menu, subindex=0) => {
    if(menu === 1) {
        if(index === 0) {
            // return <ListItemIcon><DashboardIcon /></ListItemIcon>;
            return <ListItemIcon><DashboardIcon style={{ color: this.state.active === 'overview' ? "#ffffff":"#00d2fa" }}/></ListItemIcon>;
        } else if(index === 1) {
            if(subindex === 0) {
              return  <ListItemIcon><PaidOrdersIcon style={{ color: this.state.active === 'paid' ? "#ffffff":"#00d2fa" }}/></ListItemIcon>;
            } else if(subindex === 1) {
              return  <ListItemIcon><FreeOrdersIcon style={{ color: this.state.active === 'free' ? "#ffffff":"#00d2fa" }}/></ListItemIcon>;
            } else if(subindex === -1) {
              return  <ListItemIcon><OrdersIcon style={{ color: ['paid', 'free', 'all'].includes(this.state.active) && !this.state.orders_open ? "#ffffff":"#00d2fa" }}/></ListItemIcon>;
            } else if(subindex === 2) {
              return  <ListItemIcon><AllOrdersIcon style={{ color: this.state.active === 'all' ? "#ffffff":"#00d2fa" }}/></ListItemIcon>;
            }
            
        } else if(index === 2) {
            return  <ListItemIcon><InventoryIcon style={{ color: this.state.active === 'inventory' ? "#ffffff":"#00d2fa" }}/></ListItemIcon>;
        }
    } else if(menu === 2) {
        if(index === 0) {
            // return <ListItemIcon><DashboardIcon /></ListItemIcon>;
            return <ListItemIcon><CreateIcon style={{ color: this.state.active === 'create' ? "#ffffff":"#00d2fa"}}/></ListItemIcon>;
        } 
    } else if(menu === 3) {
        if(index === 0) {
            // return <ListItemIcon><DashboardIcon /></ListItemIcon>;
            return <ListItemIcon><AccountIcon style={{ color: this.state.active === 'account' ? "#ffffff":"#00d2fa" }}/></ListItemIcon>;
        } else if (index === 1) {
            return <ListItemIcon><ReportsIcon style={{ color: "#00D2FA" }}/></ListItemIcon>;
        }
    } else if (menu === 4) {
        if (index === 0) {
            return <ListItemIcon><Logout style={{ color: "#00D2FA" }}/></ListItemIcon>;
        }
    }
    
}

    renderMenu = () => {
		return (
			<Menu
              anchorEl={this.state.anchorEl}
              anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
              id={'menuId'}
              keepMounted
              transformOrigin={{ vertical: 'top', horizontal: 'right' }}
              open={this.state.isMenuOpen}
              onClose={this.handleMenuClose}
            >
              <MenuItem onClick={this.handleMenuClose}>Profile</MenuItem>
              <MenuItem onClick={this.handleMenuClose}>My account</MenuItem>
            </Menu>
		);
	};


  
  
	render() {
        const menu1 = ['Overview', 'Orders', 'Inventory'];
        const menu2 = ['Create Order'];
        // const menu3 = ['Account', 'Reports'];
        const menu3 = ['Account'];
        const menu4 = ['Logout'];
        const orders_submenu = ["Paid", "Free", 'All'];
        const orders_submenu_lowercase = orders_submenu.map(v => v.toLowerCase());
        const { classes, theme } = this.props;
        const menuId = 'primary-search-account-menu';
        const mobileMenuId = 'primary-search-account-menu-mobile';
        const isMenuOpen = Boolean(this.state.anchorEl);
        const isMobileMenuOpen = Boolean(this.state.mobileMoreAnchorEl);

      const renderMobileMenu = (
        <Menu
          anchorEl={this.state.mobileMoreAnchorEl}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          id={mobileMenuId}
          keepMounted
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={this.state.isMobileMenuOpen}
          onClose={this.handleMobileMenuClose}
        >
          <MenuItem>
            <IconButton aria-label="show 4 new mails" color="inherit">
              <Badge badgeContent={4} color="secondary">
                <MailIcon />
              </Badge>
            </IconButton>
            <p>Messages</p>
          </MenuItem>
          <MenuItem>
            <IconButton aria-label="show 11 new notifications" color="inherit">
              <Badge badgeContent={11} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
            <p>Notifications</p>
          </MenuItem>
          <MenuItem onClick={this.handleProfileMenuOpen}>
            <IconButton
              aria-label="account of current user"
              aria-controls="primary-search-account-menu"
              aria-haspopup="true"
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
            <p>Profile</p>
          </MenuItem>
        </Menu>
      );
        // console.log("Rerender" + JSON.stringify(this.props.auth));

		return (
            // <Provider store={store}>
            // <Router>
            <>
            <CssBaseline />
            {/* { localStorage.getItem('uuid') && localStorage.getItem('role') &&  (
                <> */}
            
            <HideOnScroll {...this.props}>
                <AppBar
                    position="fixed"
                    className={clsx(classes.appBar, {
                    [classes.appBarShift]: this.state.open,
                    })}
                    style={{ backgroundColor: "white", boxShadow: "5px 10px" }}
                >
                    
                    <Toolbar>
                    { this.props.auth.isAuthenticated && (
                    <IconButton
                        color="primary"
                        aria-label="open drawer"
                        onClick={this.handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, {
                        [classes.hide]: this.state.open,
                        })}
                    >
                        <MenuIcon />
                    </IconButton>
                    )}
                    {/* <Header /> */}
                    <div className="col s2 l2 m2">
                      
                        <Link to='/overview' style={{ color: "#000000" }}>
                            <img
                                className="logo-image"
                                src={logoImage}
                                width="200"
                                style={{
                                    marginTop: '9px',
                                    marginBottom: '0px',
                                    paddingLeft: `${this.props.auth.isAuthenticated ? '': '7%'}`,
                                    marginLeft: `${this.props.auth.isAuthenticated ? '-5%': ''}`,
                                    overflow: "hidden"
                                }}
                                alt="Logo"
                            />    
                        </Link>
                        
                    </div>
                    <div className="col l6 m6 " style={{ flexGrow: '1' }}></div>
                    <div className="col l4 m4 s4" >

                        {/*<IconButton aria-label="show 17 new notifications" color="inherit" style={{ backgroundColor:
                        'blue', color: 'black'}}>
                          <Badge badgeContent={17} color="secondary">
                            <NotificationsIcon />
                          </Badge>
                        </IconButton> */}
                        <IconButton
                          edge="end"
                          aria-label="account of current user"
                          aria-controls={menuId}
                          aria-haspopup="true"
                          size='large'
                          onClick={this.handleProfileMenuOpen}
                          color="inherit"
                          style={{ backgroundColor: 'white', color: '#363844'}}
                        >
                          <AccountCircle />
                        </IconButton>
                      </div>
                      {/*<div className={styles.sectionMobile}>
                        <IconButton
                          aria-label="show more"
                          aria-controls={mobileMenuId}
                          aria-haspopup="true"
                          onClick={this.handleMobileMenuOpen}
                          color="red"
                          className={styles.profileIcon}
                          style={{ backgroundColor: 'blue', color: 'black'}}
                        >
                        </IconButton>
                      </div> */}
                    </Toolbar>
                    {/* <Header /> */}
                </AppBar>
               { /* {this.renderMenu} */}
                </HideOnScroll>
                { this.props.auth.isAuthenticated && (
                  <>
                <Drawer
                    variant="permanent"
                    className={clsx(classes.drawer, {
                    [classes.drawerOpen]: this.state.open,
                    [classes.drawerClose]: !this.state.open,
                    })}
                    classes={{
                    paper: clsx({
                        [classes.drawerOpen]: this.state.open,
                        [classes.drawerClose]: !this.state.open,
                    }),
                    }}
                    onMouseOver={this.handleDrawerOpen}
                    onMouseLeave={this.handleDrawerClose}
                    anchor="left"
                    id="drawer"
                    // className={classes.drawer}
                    // variant="permanent"
                    // classes={{
                    //     paper: classes.drawerPaper,
                    // }}
                    // anchor="left"
                    

                >
                    <div className={classes.toolbar} >
                    { this.state.open && (
                        <IconButton onClick={this.handleDrawerClose}>
                            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                        </IconButton>
                    )}
                    
                    </div>
                    <Divider />
                    <List  style={{ backgroundColor: "transparent !important" }}>
                    {menu1.map((text, index) => (
                    <>
                    { index === 1 && (
                        <>
                        <ListItem button onClick={this._handleOrdersOpen} style={{ backgroundColor: `${ orders_submenu_lowercase.includes(this.state.active) && !this.state.orders_open ? '#00D2FA': '' }` }}>
                        <ListItemIcon>
                            {this._getIcon(index, 1, -1)}
                        </ListItemIcon>
                        <ListItemText primary="Orders" style={{ color: `${ orders_submenu_lowercase.includes(this.state.active) && !this.state.orders_open ? '#ffffff': '#000000' }` }}/>
                        {this.state.orders_open ? <ExpandLess /> : <ExpandMore />}
                        </ListItem>
                        <Collapse in={this.state.orders_open} unmountOnExit>
                        <List component="div" disablePadding>
                            {orders_submenu.map((subtext, subindex) => (
                            <Link to={this._getLink(index, 1, subindex)} style={{ color: `${ this.state.active === text.toLowerCase() ? '#ffffff': '#000000' }` }} onClick={() => this.handleClick(subtext)}>
                                <ListItem button className="" style={{ "marginLeft": "1em", backgroundColor: `${ this.state.active === subtext.toLowerCase() ? '#00D2FA': '' }` }}>
                                {this._getIcon(index, 1, subindex)}
                                <ListItemText primary={subtext} style={{ color: `${ this.state.active === subtext.toLowerCase() ? '#ffffff': '#000000' }` }}/>
                                </ListItem>
                            </Link>
                            ))}
                        </List>
                        </Collapse>
                        </ >
                    )}
                    { index !== 1 && (
                        <Link to={this._getLink(index, 1)} style={{ color: `${ this.state.active === text.toLowerCase() ? '#ffffff': '#000000' }` }} onClick={() => this.handleClick(text)}>
                            <ListItem button key={text} style={{ backgroundColor: `${ this.state.active === text.toLowerCase() ? '#00D2FA': '' }` }}>
                                {this._getIcon(index, 1)}
                                <ListItemText>
                                {text}
                                </ListItemText>
                            </ListItem>
                        </Link>
                    )}
                        
                        </ >
                    ))}
                    </List>

                    <Divider />
                    <List>
                    {menu2.map((text, index) => (
                        <Link to={this._getLink(index, 2)} style={{  color: `${ this.state.active === text.split(' ')[0].toLowerCase() ? '#ffffff': '#000000' }`}} onClick={() => this.handleClick(text.split(' ')[0])}>
                            <ListItem button key={text} style={{ backgroundColor: `${ this.state.active === text.split(' ')[0].toLowerCase() ? '#00D2FA': '' }` }}>
                                {this._getIcon(index, 2)}
                                <ListItemText>
                                {text}
                                </ListItemText>
                            </ListItem>
                        </Link>
                    ))}
                    </List>

                    <Divider />
                    <List>
                    {menu3.map((text, index) => (
                        <Link to={this._getLink(index, 3)} style={{ color: `${ this.state.active === text.toLowerCase() ? '#ffffff': '#000000' }` }} onClick={() => this.handleClick(text)}>
                            <ListItem button key={text} style={{ backgroundColor: `${ this.state.active === text.toLowerCase() ? '#00D2FA': '' }` }}>
                                {this._getIcon(index, 3)}
                                <ListItemText>
                                {text}
                                </ListItemText>
                            </ListItem>
                        </Link>
                    ))}
                    <a href="https://dropwater.co">
                            <ListItem button key="about-us" style={{ color: "#000000", backgroundColor: `${ this.state.active === 'About Us' ? '#DCDCDC': '' }` }} >
                                <ListItemIcon><AboutUsIcon style={{ color: "#00d2fa" }}/></ListItemIcon>
                                <ListItemText>
                                {`About Us`}
                                </ListItemText>
                            </ListItem>
                        </a>
                    </List>

                    <Divider />
                    <List>
                    {menu4.map((text, index) => (
                        <ListItem button key={text} onClick={(e) => this._logOut(e)}>
                            {this._getIcon(index, 4)}
                            <ListItemText>
                            {text}
                            </ListItemText>
                        </ListItem>
                    ))}
                    </List>
                </Drawer>
                </ >
            )}
            </ >
		);
	}
}

const mapStateToProps = (state) => ({
	errors: state.errors,
	auth: state.auth,
});

// export default (withStyles(useStyles)(NavDrawer2));
// export default withStyles(useStyles, { withTheme: true })(NavDrawer2);

// export default connect(mapStateToProps, {
// 	logoutUser
// })(withRouter(withStyles(useStyles, { withTheme: true })(NavDrawer2)));


export default connect(mapStateToProps)(NavDrawer2);


// export default NavDrawer2;