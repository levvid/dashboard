import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { loadKiosk, loadKioskDetails } from '../../actions/kioskActions';
import { Card, Button } from 'react-bootstrap';
import { withStyles } from '@material-ui/core/styles';
import ReactLoading from 'react-loading';
import { Pie } from 'react-chartjs-2';
import { getUTCDateTime, getTimezoneAbbreviation } from '../../js/orders_functions';


import M from 'materialize-css';
import Tooltip from '@material-ui/core/Tooltip';
// import { M } from 'materialize-css/dist/js/materialize.min.js';



// dialog
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
// import Switch from '@material-ui/core/Switch';
import ToggleButton from 'react-toggle-button';

// import Switch from '@material-ui/core/Switch';
// import { useAirbnbSwitchStyles } from '@mui-treasury/styles/switch/airbnb';
// import ToggleButton from '@material-ui/lab/ToggleButton';



import { Carousel } from 'react-materialize';


import { validateUser } from '../../actions/authActions';

// images
import icn_compostable from '../../images/icn-compostable.svg';
import icn_refill from '../../images/icn-refill.svg';

var LOSANT_URL = 'https://triggers.losant.com/webhooks/n-hQrMvIh-4YbpZF3PHxCtlOKTAyxYpTIk8NcXEQ/';
var RESTART_WEBHOOK = 'https://triggers.losant.com/webhooks/RNO-o7fIRy8FjfxDp6RdbIyW65wYMPe6vS7EgGGW/';
// for testing
// RESTART_WEBHOOK = '';

LOSANT_URL += '?losantId=';
RESTART_WEBHOOK += '?losantId=';

// const switchStyles = useAirbnbSwitchStyles();


const LightTooltip = withStyles((theme) => ({
	tooltip: {
		//   backgroundColor: theme.palette.common.blue,
		backgroundColor: '#DDF2FF',
		//   color: 'rgba(0, 0, 0, 0.87)',
		color: '#000000',
		boxShadow: theme.shadows[1],
		fontSize: 12,
		textAlign: 'center'
	}
}))(Tooltip);


const RedTooltip = withStyles((theme) => ({
	tooltip: {
		//   backgroundColor: theme.palette.common.blue,
		backgroundColor: '#FFEFEB',
		//   color: 'rgba(0, 0, 0, 0.87)',
		color: '#000000',
		boxShadow: theme.shadows[1],
		fontSize: 12,
		textAlign: 'center',
		borderColor: '#CD4528',
		borderWidth: '10px'
	}
}))(Tooltip);


class Kiosk extends Component {
	constructor() {
		super();
		this.state = {
			uuid: '',
			kiosk: {},
			showModal: false,
			errors: {},
			online: true,
			password: '',
			isValid: true,
			command: '',
			refill: "On",
			compostable: "On",
			toggleActive: true,
			showScreenshotModal: false,
		};

		this._handleOpenModal = this._handleOpenModal.bind(this);
		this._handleCloseModal = this._handleCloseModal.bind(this);
		this._restartKiosk = this._restartKiosk.bind(this);
		this._handlePasswordChange = this._handlePasswordChange.bind(this);
		this.onToggle = this.onToggle.bind(this);
		this._handleSubmit = this._handleSubmit.bind(this);
	}

	componentDidMount() {
		var uuid = this.props.match.params.uuid;
		this.setState({
			uuid: uuid,
			email: this.props.auth.user.email,
			userId: this.props.auth.user.uuid,
			compostable: "On",
			refill: "On"
		});

		console.log("Module Status: "  + JSON.stringify(this.props.kiosk.kiosk.moduleStatus))

		// console.log('Auth new one: ' + JSON.stringify(this.props.auth.user));
		const filters = {
			uuid: uuid,
			refill: this.state.refill,
			compostable: this.state.compostable,
			timezone: getTimezoneAbbreviation(),
			limit: 5
		};

		this._checkDeviceStatus();

		setTimeout(() => {
			filters.limit = 0
			this.props.loadKiosk(filters);
		}, 1000);
		this.props.loadKiosk(filters);
		// this.props.loadKioskDetails(filters);
	}

	_checkDeviceStatus = () => {
		// console.log("Checking status..." + this.state.uuid);
		fetch(LOSANT_URL + this.state.uuid).then((res) => res.json()).then(
			(result) => {
				if (result.device) {
					// sometimes device is null esp when component is first rendered
					this.setState({
						online: result.status === 'connected' ? true : false
					});
				}
			},
			// Note: it's important to handle errors here
			// instead of a catch() block so that we don't swallow
			// exceptions from actual bugs in components.
			(error) => {}
		);

		setTimeout(this._checkDeviceStatus, 30000); // check the device status every 30 seconds
	};

	// _sendCommand
	_sendRestartResetCommand = () => {
		// console.log("Sending command.......");
		// return;
		if (this.state.command === '') {
			// epmty command, tell user?
			console.log('Empty command.');
			return;
		}
		console.log("Sending cmd." + this.state.command);

		var finalCommand =
			RESTART_WEBHOOK + this.state.uuid + '&command=' + this.state.command + '&uuid=' + this.state.userId;
		// console.log('Final comamnd: ' + finalCommand);
		// console.log("Checking status..." + this.state.uuid);
		fetch(finalCommand).then((res) => res.json()).then(
			(result) => {
				console.log("Sent CMD.")
				if (result && result.restartStatus === 'success') {
					switch (this.state.command) {
						case 'Reset':
							M.toast({ html: 'Successfully reset ' + this.state.kiosk.kiosk_ID, classes: 'rounded' });
							break;
						case 'Restart':
							M.toast({
								html: 'Successfully restarted ' + this.state.kiosk.kiosk_ID,
								classes: 'rounded'
							});
							break;
						case 'Compostable':
							M.toast({
								html: 'Successfully toggled Compostable workflow on ' + this.state.kiosk.kiosk_ID,
								classes: 'rounded'
							});
							break;
						case 'Refill':
							M.toast({
								html: 'Successfully toggled Refill workflow on  ' + this.state.kiosk.kiosk_ID,
								classes: 'rounded'
							});
							break;
						case 'SyncDB':
							M.toast({
								html: 'Successfully synced ' + this.state.kiosk.kiosk_ID,
								classes: 'rounded'
							});
							break;
						default:
							break;
					}

					if (this.state.command === 'SyncDB') {
						this.setState({
							password: '',
							command: ''
						});
					} else {
						this.setState({
							online: false,
							password: '',
							command: ''
						});
					}
					
				} else {
					if (result && result.status === 'disconnected') {
						M.toast({
							html: this.state.command + ' failed! ' + this.state.kiosk.kiosk_ID + ' is offline.',
							classes: 'rounded'
						});
						this.setState({
							online: false,
							password: '',
							command: ''
						});
					} else {
						console.log('Unknown error.');
					}
				}
			},
			// Note: it's important to handle errors here
			// instead of a catch() block so that we don't swallow
			// exceptions from actual bugs in components.
			(error) => {
				M.toast({
					html: this.state.command + ' failed! Error connecting to the kiosk.',
					classes: 'rounded'
				});
				this.setState({
					password: '',
					command: ''
				});
			}
		);
	};

	componentWillReceiveProps(nextProps) {
		// console.log('Returned' + JSON.stringify(nextProps.auth.user));
		// console.log('Errors: ' + JSON.stringify(nextProps.errors));

		if (nextProps.auth.user.isValid) {
			// console.log('User valid. Resetting.');
			nextProps.auth.user.isValid = false;
			this._sendRestartResetCommand();
		} else if (nextProps.errors) {
			this.setState({
				errors: nextProps.errors
			});
			if (nextProps.errors.passwordincorrect) {
				M.toast({ html: 'Sorry, the password you entered is incorrect. Please try again', classes: 'rounded' });
				this.setState({
					isValid: false,
					showModal: true,
					password: ''
				});
			} else if (nextProps.errors.invalidcredentials) {
				// M.toast({ html: 'Sorry, your session has timed out. Please logout then log back in.', classes: 'rounded' });
				console.log('Empty email field.');
			}
		}
	}

	_handleSubmit(event) {
		// alert('A name was submitted: ' + this.state.value);
		event.preventDefault();
		this._restartKiosk();
	}

	_handleOpenModal = (kiosk, command) => {
		// console.log('Opening modal: ' + command);
		this.setState({
			showModal: true,
			kiosk: kiosk,
			command: command
		});
	};

	_handleCloseModal() {
		// console.log('Closing modal...');
		this.setState({
			showModal: false,
			isValid: true
		});
		this._resetPassword();
	}

	_formatDate = (date) => {
		return new Date(date).toLocaleDateString();
	}

	_resetPassword = () => {
		this.setState({
			password: '',
		})
	}

	_restartKiosk = () => {
		console.log("Restart called.")
		var data = {
			email: this.state.email,
			password: this.state.password
		};

		switch (this.state.command) {
			case 'Restart':
				console.log('Restarting ....' + this.state.kiosk.losantId + ' pwd: ' + this.state.password);
				this.props.validateUser(data); // validate user. Will return in component will receive props
				break;
			case 'Reset':
				console.log('Resetting ....' + this.state.kiosk.losantId);
				this.props.validateUser(data);
				break;
			case 'Compostable':
				console.log('Compostable toggle ...' + this.state.kiosk.losantId);
				this.props.validateUser(data);
				break;
			case 'Refill':
				console.log('Refill toggle ...' + this.state.kiosk.losantId);
				this.props.validateUser(data);
				break;
			case 'SyncDB':
				console.log('Sync DB toggle ...' + this.state.kiosk.losantId);
				this.props.validateUser(data);
				break;
			default:
				console.log('Default: Unknown command');
				break;
		}
		this._handleCloseModal();
		// this._resetPassword();
	};

	_handlePasswordChange = (event) => {
		this.setState({
			password: event.target.value
		});
	};


	_getPieChart = (chart_data) => {
		return (
			<Pie
				data={chart_data}
				height={ this.state.width < 800?280:150 }
				options={{
					maintainAspectRatio: true,
					tooltips: {
						callbacks: {
							// https://stackoverflow.com/questions/43604597/how-to-customize-the-tooltip-of-a-chart-js-2-0-doughnut-chart
							// https://jsfiddle.net/m7s43hrs/
							//   title: function(tooltipItem, data) {
							// 	return data['labels'][tooltipItem[0]['index']];
							//   },
							//   label: function(tooltipItem, data) {
							// 	return data['datasets'][0]['data'][tooltipItem['index']];
							//   },
							afterLabel: function(tooltipItem, data) {
								var dataset = data['datasets'][0];
								var dataTmp = dataset['data'];

								var total = dataTmp.reduce((a, b) => a + b, 0); // get sum of data array
								var percent = Math.round((dataset['data'][tooltipItem['index']] / total) * 100)
								return '(' + percent + '%)';
							}
						},
						backgroundColor: '#000',
						titleFontSize: 15,
						titleFontColor: '#fff',
						bodyFontColor: '#fff',
						bodyFontSize: 15,
						displayColors: true
					}
				}}
			/>
		)
	}

	_renderCarousel = (screenshotList) => {
		return (
			screenshotList &&
			screenshotList.map(
				({
					name,
					date_created,
					image
				}) => (
					<div className='carousel-item'>
						<img src={ image } alt="Screenshot" onClick={() => this._test(date_created, image)} className="screenshot-image"></img>
						{/* <h2>{ date_created }</h2> */}
						<p className='' style={{ marginTop: "-400px" }}>{ new Date(date_created).toLocaleString([], {month: '2-digit', day: '2-digit', year: '2-digit', hour: '2-digit', minute:'2-digit'}) }</p>
					</div>
				)
			)
		);
	}

	_handleToggle = (event) => {
		console.log("Event: " + JSON.stringify(event));
		this.setState({ ...this.state, graphType: event });
	};

	onToggle = () => {
		this.setState({ toggleActive: !this.state.toggleActive });
	}

	_test = (date_created, image) => {
		console.log("herererereererrere");
		this.setState({
            screenshotImage: image,
			date_created: new Date(date_created).toLocaleString([], {month: '2-digit', day: '2-digit', year: '2-digit', hour: '2-digit', minute:'2-digit'})
        }, () => {
            this.setState({
                showScreenshotModal: true
            });
        });
		
	}

	_handleCloseScreenshotModal = () => {
		this.setState({
			showScreenshotModal: false
		});
	}

	_handleRefillToggle = (value, kiosk) => {
		this._handleOpenModal(kiosk, 'Refill');
	}

	_handleCompostableToggle = (value, kiosk) => {
		console.log("Calling props compost");
		this._handleOpenModal(kiosk, 'Compostable');
	}


	render() {
		const summary = this.props.kiosk.kiosk;
		console.log("render");
		// console.log('Summary: ' + JSON.stringify(summary));
		// console.log('This is the props: ' + JSON.stringify(this.props));
		
		const kiosk = summary.kiosk;
		const inventory = summary.inventory;
		const drinkPrice = summary.drinkPrice;
		const daysInOperation = summary.daysInOperation;
		const compostableTransactions = summary.compostableTransactions;
		const compostableSales = summary.compostableSales;
		const refillPaidTransactions = summary.refillPaidTransactions;
		const refillSales = summary.refillSales;
		const refillTransactions = summary.refillTransactions;
		const averageCompostableTransactions = summary.averageCompostableTransactions;
		const averageRefillTransactions = summary.averageRefillTransactions;
		const averageCompostableSales = summary.averageCompostableSales;
		const averageRefillSales = summary.averageRefillSales;
		const sales = summary.sales;
		const transactions = summary.transactions;
		const waterOunces = summary.waterOunces;
		const screenshotList = summary.screenshotList;
		const moduleStatus = summary.moduleStatus;
		


		// graphs
		const flavor_graph = summary.flavor_graph;
		const caffeine_flavor_graph = summary.caffeine_flavor_graph;
		
		if (!kiosk || !inventory || !drinkPrice || !screenshotList) {
			return (
				<div>
					<ReactLoading
						type={'spinningBubbles'}
						color={'#58cbe1'}
						height={this.state.width < 800 ? '55%' : '20%'}
						width={this.state.width < 800 ? '55%' : '20%'}
						className="center-align dash-progress-spinner"
					/>
				</div>
			);
		} else {
			// process.env.DEBUG === "true" && console.log('All orders list ' + all_orders.mapped_orders[0]);
		}
		console.log("Module status: " + JSON.stringify(moduleStatus));
		// var elem = document.querySelector('.carousel');
   		// var instance = M.Carousel.init(elem, { duration: 200 });
		// console.log("screenshotList: " + JSON.stringify(screenshotList[0].image))
		var title = '';
		var text = '';
		if (this.state.command) {
			if(this.state.command === "Reset" || this.state.command === "Restart") {
				title = this.state.command + " " + kiosk.kiosk_ID;
				text = "To " + this.state.command.toLowerCase() + " " + kiosk.kiosk_ID + ", please enter your password below.";
			} else if (this.state.command === "Compostable"){
				title = "Toggle Compostable Bottle - " + kiosk.kiosk_ID;
				if (this.state.compostable) {
					text = "To turn the drop bottle workflow off, please enter your password below.";
				} else {
					text = "To turn the drop bottle workflow on, please enter your password below.";
				}
			} else if (this.state.command === "Refill"){
				title = "Toggle Refill - " + kiosk.kiosk_ID;
				if (this.state.refill) {
					text = "To turn the refill workflow off, please enter your password below.";
				} else {
					text = "To turn the refill workflow on, please enter your password below.";
				}
			} else if (this.state.command === "SyncDB")	 {
				title = "Sync " + " " + kiosk.kiosk_ID + "'s data";
				text = "To sync data from " + kiosk.kiosk_ID + ", please enter your password below.";
			}
		}
		
		return (
			kiosk && (
				<div className="valign-wrapper  animate__animated animate__slideInLeft">
					<div className="row">
						<>
							<Dialog open={this.state.showModal} onClose={this._handleCloseModal} aria-labelledby="form-dialog-title">
										<DialogTitle id="form-dialog-title">
											{ title }
										</DialogTitle>
										<DialogContent>
										<DialogContentText>
											{ text }
										</DialogContentText>
										<form onSubmit={this._handleSubmit}>
											<TextField
												autoFocus
												margin="dense"
												id="password"
												label="Password"
												type="password"
												fullWidth
												value={this.state.password}
												onChange={this._handlePasswordChange}
											/>
											
											{this.state.isValid === false && (
												<label for="password" style={{ color: 'red' }} className="center">
													Incorrect Password
												</label>
												)}
											{/* {this.state.isValid === true && (
												<label for="password" style={{}} className="center">
													Password
												</label>
											)} */}
										</form>

										</DialogContent>
										<DialogActions>
										<Button onClick={this._handleCloseModal} className="small transparent"
											style={{
												color: "#707B7C",
												fontWeight: "500"
											}}
										>
											Cancel
										</Button>
										<Button onClick={this._restartKiosk} className="secondary"
											style={{
												backgroundColor: '#2E86C1'
											}}
										>
											{`${this.state.command}`}
										</Button>
										</DialogActions>
						</Dialog>
						
						<Dialog
							open={this.state.showScreenshotModal}
							onClose={this._handleCloseScreenshotModal}
							aria-labelledby="alert-dialog-title"
							aria-describedby="alert-dialog-description"
							className="center screenshot-modal"
							onClick={this._handleCloseScreenshotModal}
						>
							<DialogTitle id="alert-dialog-title" className="center screenshot-modal-header" style={{  width: "50%", margin: "auto" }}>{this.state.date_created}</DialogTitle>
							<DialogContent >
							<DialogContentText id="alert-dialog-description" className="center">
								<img src={ this.state.screenshotImage } alt="Screenshot" style={{ width: "65%" }} className="screenshot-modal"></img>
							</DialogContentText>
							</DialogContent>
							{/* <DialogActions>
							<Button onClick={this._handleCloseScreenshotModal} color="secondary" autoFocus>
								Dismiss
							</Button>
							</DialogActions> */}
						</Dialog>


						<Card style={{ width: '100%', borderRadius: '20px' }} className="col s12 l12 m12 filter-card">
							<Card.Title style={{ fontWeight: '400', marginTop: '2%', marginBottom: '-1%' }}>
								<div className="col s6 l6 m6">
									<strong>{kiosk.kiosk_ID}</strong>
								</div>
								{this.state.online && (
									<div className="col s6 l6 m6 right-align online-status">
										<span className="indicator online" /> Online
									</div>
								)}
								{!this.state.online && (
									<div className="col s6 l6 m6 right-align online-status ">
										<span className="indicator offline" /> Offline
									</div>
								)}
							</Card.Title>
							<Card.Body>
							<Card.Text className="s12 l12 m12 right-align">
								<div className="col s12 l12 m12 right-align">
									<div className="col s0 l6 m6">

										{/* <Card.Text className="col s1 l1 m1">Compostable</Card.Text>
										<div className="col s1 l1 m1"></div>
										<Card.Text className="col s2 l1 m1 right-align">
											<ToggleButton
												className="col s2 l2 m2 right-align"
												value={ this.state.refill || false }
													onToggle={(value) => {
													this._handleRefillToggle(value)
												}} />
										</Card.Text>
										<Card.Text className="col s2 l1 m1">Refill</Card.Text> */}
											
									</div>
										{this.props.auth.user.role === 'drop-super-admin' && (
											<>
											<div className="col s12 l2 m2">
												<LightTooltip
													title="Synchronizes the data between the local and AWS databases."
													placement="top"
													arrow
												>
													<button
														style={{
															width: '150px',
															borderRadius: '3px',
															letterSpacing: '1px',
															marginTop: '1rem',
															marginBottom: '1rem',
															// marginLeft: '1rem',
															fontSize: '13px',
															// marginRight: '10px'
															// backgroundColor: '#2471a3'
														}}
														className="btn btn-small hoverable blue "
														onClick={() => this._handleOpenModal(kiosk, 'SyncDB')}
													>
														Sync Data
													</button>
												</LightTooltip>
											</div>
											<div className="col s12 l2 m2">
												<LightTooltip
													title="Resets all the modules prompting the gantry and slider to have a bottle each."
													placement="top"
													arrow
												>
													<button
														style={{
															width: '150px',
															borderRadius: '3px',
															letterSpacing: '1px',
															marginTop: '1rem',
															marginBottom: '1rem',
															// marginLeft: '1rem',
															fontSize: '13px',
															// marginRight: '10px'
															// backgroundColor: '#2471a3'
														}}
														className="btn btn-small hoverable blue "
														onClick={() => this._handleOpenModal(kiosk, 'Reset')}
													>
														Reset
													</button>
												</LightTooltip>
											</div>
											</ >
										)}
										{this.props.auth.user.role !== 'drop-super-admin' && (
											
											<div className="col s0 l2 m2">
											</div>
										)}
										{(this.props.auth.user.role === 'drop-super-admin' ||
											this.props.auth.user.role === 'drop-admin') && (
											<div className="col s12 l2 m2">
												<RedTooltip
													title="Reboots the Jetson and resets all the modules."
													placement="top"
													arrow
												>
													<button
														style={{
															width: '150px',
															borderRadius: '3px',
															letterSpacing: '1px',
															marginTop: '1rem',
															marginBottom: '1rem',
															fontSize: '13px',
															backgroundColor: '#ff5733'
														}}
														className="btn btn-small hoverable"
														onClick={() => this._handleOpenModal(kiosk, 'Restart')}
													>
														Reboot
													</button>
												</RedTooltip>
											</div>
										)}
									</div>
								</Card.Text>
								

								<Card.Text className="col s6 l3 m3">
									<strong>Installation Date:&nbsp;</strong>
									{this._formatDate(kiosk.date_created)}
								</Card.Text>
								<Card.Text className="col s6 l3 m3">
									<strong>Time in Operation:&nbsp;</strong>
									{daysInOperation}
								</Card.Text>

								<Card.Text className="col s12 l6 m6">
									<strong>Location:&nbsp;</strong>
									{kiosk.location}&nbsp;({kiosk.locationAlias})
								</Card.Text>
								<Card.Text className="col s6 l3 m3">
									<strong>Alias:&nbsp;</strong>
									{kiosk.nickname}
								</Card.Text>
								<Card.Text className="col s6 l3 m3">
									<strong>Total Transactions:&nbsp;</strong>
									{transactions}
								</Card.Text>
								<Card.Text className="col s6 l3 m3">
									<strong>Total Sales:&nbsp;</strong>
									&nbsp;$&nbsp;{sales}
								</Card.Text>
								<Card.Text className="col s6 l3 m3">
									<strong>Total Water Dispensed:&nbsp;</strong>
									&nbsp;{waterOunces}&nbsp;liters
								</Card.Text>
								<Card.Text className="col s12 m6 l6">
									<div className="col s6 l6 m6">
										{/* <p className="center-align menu-item">COMPOSTBALE BOTTLES.</p> */}
										<img border="0" alt="DropWater Logo" src={icn_compostable} width="100%" />
										<p className="center-align menu-item" style={{ marginLeft: "40%" }}>
											{/* <Switch classes={switchStyles} /> */}
											<ToggleButton
												className=" center-align"
												
												value={ moduleStatus.dropBottleStatus === "On" || false }
													onToggle={(value) => {
														moduleStatus.dropBottleStatus = moduleStatus.dropBottleStatus === "On" ? "Off": "On"
													this._handleCompostableToggle(value, kiosk)
											}} />
											{/* <Switch
												checked={this.state.refill}
												onChange={this._handleRefillToggle}
												name="checkedA"
												inputProps={{ 'aria-label': 'secondary checkbox' }}
											/> */}
										</p>
										<p className="center-align menu-item">
											<strong>Transactions:</strong>&nbsp;&nbsp;{compostableTransactions}
										</p>
										<p className="center-align menu-item-2">
											<strong>Sales:</strong>&nbsp;&nbsp;$&nbsp;{compostableSales}
										</p>
										<p className="center-align menu-item-2">
											<strong>Transactions/Day:</strong>&nbsp;&nbsp;{averageCompostableTransactions}
										</p>
										<p className="center-align menu-item-2">
											<strong>Sales/Day:</strong>&nbsp;&nbsp;$&nbsp;{averageCompostableSales}
										</p>
									</div>
									<div className="col s6 l6 m6">
										{/* <p className="center-align menu-item">REFILLS.</p> */}
										<img border="0" alt="DropWater Logo" src={icn_refill} width="100%" />
										<p className="center-align menu-item" style={{ marginLeft: "40%" }}>
											<ToggleButton
												className=""
												value={ moduleStatus.refillStatus === "On" || false }
													onToggle={(value) => {
														moduleStatus.refillStatus = moduleStatus.refillStatus === "On" ? "Off": "On"
													this._handleRefillToggle(value, kiosk)
												}} />
										</p>
										<p className="center-align menu-item">
											<strong>Transactions:</strong>&nbsp;&nbsp;{refillTransactions}
										</p>
										<p className="center-align menu-item-2">
											<strong>Sales:</strong>&nbsp;&nbsp;$&nbsp;{refillSales}
										</p>
										<p className="center-align menu-item-2">
											<strong>Transactions/Day:</strong>&nbsp;&nbsp;{averageRefillTransactions}
										</p>
										<p className="center-align menu-item-2">
											<strong>Sales/Day:</strong>&nbsp;&nbsp;$&nbsp;{averageRefillSales}
										</p>
										<p className="center-align menu-item-2">
											<strong>Paid Transactions:</strong>&nbsp;&nbsp;{refillPaidTransactions}
										</p>
									</div>

									{/* </div> */}
								</Card.Text>
								<Card.Text className="col s6 l3 m3">
									<table style={{ tableLayout: 'fixed' }}>
										<tr className="inventory-summary">
											<th className="center-align inventory-summary-header">Active Tower</th>
											<td className="center-align">
												{inventory.towerSelected === 'L' ? 'Left' : 'Right'}
											</td>
										</tr>
										<tr>
											<th className="center-align inventory-summary-header">Right Tower</th>
											<td className="center-align">{inventory.rightTowerRemaining}</td>
										</tr>
										<tr className="inventory-summary">
											<th className="center-align inventory-summary-header">Left Tower</th>
											<td className="center-align">{inventory.leftTowerRemaining}</td>
										</tr>
										<tr>
											<th className="center-align inventory-summary-header">Drain Tank</th>
											<td className="center-align">{inventory.drainTankStatus}</td>
										</tr>
									</table>
								</Card.Text>
								<Card.Text className="col s6 l3 m3">
									<table style={{ tableLayout: 'fixed' }}>
										<tr className="inventory-summary">
											<th className="center-align inventory-summary-header">Caffeine</th>
											<td className="center-align">{inventory.caffeine.toFixed(2)}</td>
										</tr>
										<tr className="inventory-summary">
											<th className="center-align inventory-summary-header">Cucumber</th>
											<td className="center-align">{inventory.cucumber.toFixed(2)}</td>
										</tr>
										<tr>
											<th className="center-align inventory-summary-header">Guava</th>
											<td className="center-align">{inventory.guava.toFixed(2)}</td>
										</tr>
										<tr>
											<th className="center-align inventory-summary-header">Lemonade</th>
											<td className="center-align">{inventory.lemonade.toFixed(2)}</td>
										</tr>
									</table>
								</Card.Text>

								<Card.Text className="col s12 l6 m6">
									<table style={{ tableLayout: 'fixed' }}>
										<tr>
											<th />
											<th className="center-align">Water</th>
											<th className="center-align">Flavor</th>
											<th className="center-align">Supplement</th>
										</tr>
										<tr>
											<th>Drop Bottle</th>
											<td className="center-align">
												$ {(drinkPrice.dropBottleWater / 100).toFixed(2)}
											</td>
											<td className="center-align">
												$ {(drinkPrice.dropBottleFlavor / 100).toFixed(2)}
											</td>
											<td className="center-align">
												$ {(drinkPrice.dropBottleSupplement / 100).toFixed(2)}
											</td>
										</tr>
										<tr>
											<th>Refill</th>
											<td className="center-align">
												$ {(drinkPrice.refillWater / 100).toFixed(2)}
											</td>
											<td className="center-align">
												$ {(drinkPrice.refillFlavor / 100).toFixed(2)}
											</td>
											<td className="center-align">
												$ {(drinkPrice.refillSupplement / 100).toFixed(2)}
											</td>
										</tr>
									</table>
								</Card.Text>
							</Card.Body>
						</Card>

						<Card style={{ borderRadius: "15px" }} className="col s12 l12 m12 center-align">
							<Card.Body>
								<Card.Title style={{ fontWeight: '400' }}>Latest Screenshots</Card.Title>
								
								<Card.Text style={{  }} className="">
								{ screenshotList.length > 0 && (
									<Carousel 
										options={{ fullWidth: false, fullHeight: true, indicators: true, shift: 30, padding: 30 }}
										// fixedItem={<button className='btn'>MORE</button>}
										// images={ screenshotList.map((item)=> item.image)}  
										// className="carousel-slider"
										// pressed={this._test}
										
										>
										{ this._renderCarousel(screenshotList)}
									</Carousel>
								)}

								{ screenshotList.length === 0 && (
									<div>No Screenshots Available.</div>
								)}
									
								</Card.Text>
								
							</Card.Body>
						</Card>

						<Card style={{ borderRadius: "15px", width: "49.5%" }} className="col s12 l6 m6 center-align mr-2 pie-chart">
							<Card.Body>
							{/* Toggle buttons */}
								{/* <Card.Text className="col s2 l2 m2 right-align">
									<ToggleButton
									className="col s2 l3 m3 right-align"
										value={ this.state.compostable || false }
										onToggle={(value) => {
											this._handleCompostableToggle(value)
										
									}} />
								</Card.Text>
								<Card.Text className="col s3 l3 m3">Compostable</Card.Text>
								<div className="col s1 l1 m1"></div>
								<Card.Text className="col s2 l2 m2 right-align">
									<ToggleButton
									className="col s3 l3 m3 right-align"
									// inactiveLabel='Compostable'
									// width="50%"
  									// activeLabel='Compostable'
										value={ this.state.refill || false }
										onToggle={(value) => {
											this._handleRefillToggle(value)
										
									}} />
								</Card.Text>
								<Card.Text className="col s3 l3 m3">Refill</Card.Text> */}
								{/* /Toggle Btns */}
								<Card.Title style={{ fontWeight: '400' }}>Screen Servings by Flavor</Card.Title>
								
								<Card.Text style={{  }} className="">
									
									{ this._getPieChart(flavor_graph) }
								</Card.Text>
								
							</Card.Body>
						</Card>
						<Card style={{  borderRadius: "15px", width: "49.5%", marginLeft: "1%" }} className="col s12 l6 m6 center-align ml-5 pie-chart">
							{/* <Card.Header>
	Summary
	</Card.Header> */}
							<Card.Body className="center-align">
							{/* Toggle buttons */}
								{/* <Card.Text className="col s2 l2 m2 right-align">
									<ToggleButton
									className="col s2 l3 m3 right-align"
										value={ this.state.compostable || false }
										onToggle={this._handleCompostableToggle} />
								</Card.Text>
								<Card.Text className="col s3 l3 m3">Compostable</Card.Text>
								<div className="col s1 l1 m1"></div>
								<Card.Text className="col s2 l2 m2 right-align">
									<ToggleButton
									className="col s3 l3 m3 right-align"
									// inactiveLabel='Compostable'
									// width="50%"
  									// activeLabel='Compostable'
										value={ this.state.refill || false }
										onToggle={(value) => {
											this._handleRefillToggle(value)
										
									}} />
								</Card.Text>
								<Card.Text className="col s3 l3 m3">Refill</Card.Text> */}
								{/* /Toggle Btns */}
								<Card.Title style={{ fontWeight: '400' }}>Caffeine Servings (Screen)</Card.Title>
								<Card.Text style={{ fontWeight: '400' }} className="">
									{ this._getPieChart(caffeine_flavor_graph) }
								</Card.Text>

								
							</Card.Body>
						</Card>
						{/* <Modfffffffffff */}
						</>
					</div>
				</div>
			)
		);
	}
}

Kiosk.propTypes = {
	loadKiosk: PropTypes.func.isRequired,
	loadKioskDetails: PropTypes.func.isRequired,
	validateUser: PropTypes.func.isRequired,
	kiosk: PropTypes.object.isRequired,
	errors: PropTypes.object.isRequired,
	auth: PropTypes.object.isRequired,
	user: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
	kiosk: state.kiosk,
	errors: state.errors,
	auth: state.auth,
	user: state.user
});
export default connect(mapStateToProps, {
	loadKiosk,
	loadKioskDetails,
	validateUser
})(withRouter(Kiosk));
