import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { createOrder } from '../../actions/orderCreateActions';
import { loadKioskDetails } from '../../actions/PropagateActions';
import { Card, Image } from 'react-bootstrap';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-dropdown/style.css';
import '../../css/date_picker.css';
import ReactLoading from 'react-loading';
import M from 'materialize-css';


// Images
import icn_compostable from '../../images/icn-compostable.svg';
import icn_refill from '../../images/icn-refill.svg';
import icn_caffeine_0_mg from '../../images/icn-caffeine-0-mg.svg';
import icn_caffeine_20_mg from '../../images/icn-caffeine-20-mg.svg';
import icn_caffeine_40_mg from '../../images/icn-caffeine-40-mg.svg';
import icn_caffeine_70_mg from '../../images/icn-caffeine-70-mg.svg';
import icn_cucumber from '../../images/icn-cucumber.svg';
import icn_guava from '../../images/icn-guava.svg';
import icn_lemonade from '../../images/icn-lemonade.svg';
import icn_water from '../../images/icn-water.svg';
import icn_room from '../../images/icn-room.png';
import icn_cold from '../../images/icn-cold.png';
import card_reader from '../../images/card-reader.svg';
import success_badge from '../../images/success-badge.svg';
import Link from '@material-ui/core/Link';


import Fab from '@material-ui/core/Fab';
import NavigationIcon from '@material-ui/icons/Navigation';


import { v4 as uuidv4 } from 'uuid';


const qrcode = require('qrcode');
const URL = 'wss://i93po5s0lh.execute-api.us-west-1.amazonaws.com/Prod';
const MINUTES = 9;

const SECONDS = 59;
const CUT_OFF = 20;


var SHOW_WELCOME_SCREEN = false;
var SHOW_ORDER_LAYOUT = true;
var SHOW_PROGRESS_BAR = false;
var SHOW_REFILL_SCREEN = false;
var SHOW_QRCODE = false;
var SHOW_SUCCESS_SCREEN = false;
var SHOW_PAYMENT_SCREEN = false;

class OrderCreate extends Component {
	constructor() {
        super();
		this.state = {
			order: {
                isOrder: true,
                dropBottle: false,
                dropBottleString: "false",
				flavor: 'water',
                caffeine: 0,
                caffeineTmp: 'none',
                // room: 1, cold: 2
                temperature: 'cold',
                temperatureNumber: 2,
                price: 0,
			},
            kioskUsable: true,
            connectionId: '',
            kioskConnectionId: '',
            qrCodeURI: '',
            showWelcomeScreen: SHOW_WELCOME_SCREEN,
            showProceedBtn: false,
            showOrderLayout: SHOW_ORDER_LAYOUT,
            showProgressBar: SHOW_PROGRESS_BAR,
            showRefillScreen: SHOW_REFILL_SCREEN,
            showSuccessScreen: SHOW_SUCCESS_SCREEN,
            showQRCode: SHOW_QRCODE,
            showPaymentScreen: SHOW_PAYMENT_SCREEN,
            minutes: MINUTES,
            seconds: SECONDS,
            width: window.innerWidth, 
            height: window.innerHeight,
            touchstartTime: new Date(),
            progress: 0,
            errors: {},
            orderInProgress: false,
            isValid: true,
            kioskDetails: {
                kiosk: {
                    location: 'Drop Water'
                },
                inventory: {

                },
                drinkprice: {
                    dropBottleWater: 0,
                    refillWater: 0,
                    refillFlavor: 0,
                    refillSupplement: 0,
                },
                moduleStatus: {
                    dropBottleStatus: 'On',
                    refillStatus: 'On'
                }
            },
            validCount: 0,
            kioskNickname: '',
            animate: {
                compostable: false,
                refill: false,
                guava: false,
                cucumber: false,
                lemonade: false,
                temperature: "none",

                caffeine: -1,
            },
            uuid: uuidv4()
        };
        this._updateWindowDimensions = this._updateWindowDimensions.bind(this);
        this._onSelectBottleType = this._onSelectBottleType.bind(this);
        this._showWelcomeScreen = this._showWelcomeScreen.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
	}

	ws = undefined;
    
    _showProgressBar = () => {
        this._hideAllLayouts();
        this.setState({
            showProgressBar: true,
        });
    }

    _hideAllLayouts = () => {
        this.setState({
            showOrderLayout: false,
            showProgressBar: false,
            showRefillScreen: false,
            showQRCode: false,
            showPaymentScreen: false,
            showSuccessScreen: false,
            showWelcomeScreen: false,
        });
    }

    _resetOrderCreate = () => {
        console.log("Resetting screen");
        this.setState({
            order: {
                isOrder: true,
                dropBottle: false,
                dropBottleString: "false",
				flavor: 'water',
                caffeine: 0,
                caffeineTmp: 'none',
                // room - 1, cold - 2
                temperature: 'cold',
                temperatureNumber: 2,
                price: 0
			},
            connectionId: '',
            kioskConnectionId: '',
            qrCodeURI: ''
        });
        this._showOrderLayout();
    }


    _showOrderLayout = () => {
        this._hideAllLayouts();
        console.log("Showing refill screen");
        this.setState({
            showOrderLayout: true,
            orderInProgress: false,
        });
    }

    _showWelcomeScreen = () => {
        this._hideAllLayouts();
        console.log("Showing welcome screen");
        this.setState({
            showWelcomeScreen: true,
        });
    }

    _showQRCodeLayout = () => {
        this._hideAllLayouts();
        console.log("Showing refill screen");
        this.setState({
            showQRCode: true
        });
    }

    _showRefillScreen = () => {
        this._hideAllLayouts();
        process.env.DEBUG === "true" && console.log("Showing refill screen");
        this.setState({
            showRefillScreen: true,
        });
    }

    _updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    _showFillingScreen = () => {
        this._hideAllLayouts();
        // then
        // show pour btns or progress bar
        if ( this.state.order.dropBottle ) { // compostable
            this._showProgressBar();
        } else { // Refill
            this._showRefillScreen();
        }
    }

    _showPaymentScreen = () => {
        this.setState({
            showOrderLayout: false,
            showProgressBar: false,
            showRefillScreen: false,
            showQRCode: false,
            showPaymentScreen: true
        });
    }

    onSubmit = (e) => {
        e.preventDefault();
        this._loadKioskData();
        
        // since we handle the redirect within our component, we don't need to pass in this.props.history as a parameter
    };


    _loadKioskData = () => {
        if (this.state.kioskNickname){
            const filters = {
                kioskNickname: this.state.kioskNickname.trim().toLowerCase(),  // strip whitespace in case of user error
            };
            this.props.loadKioskDetails(filters);
        }      
    }


	componentDidMount() {
        window.addEventListener('resize', this._updateWindowDimensions);
        var kioskNickname = this.props.match.params.kiosk_nickname;

        if (typeof kioskNickname !== 'undefined') { // kiosk nickname not provided
            this.setState({
                kioskNickname: kioskNickname
            });
            // this._hideAllLayouts();
            // this._showOrderLayout();
        } else {
            this._showWelcomeScreen();
        }
        console.log("Kiosk nickname: " + kioskNickname);

        const filters = {
            kioskNickname: kioskNickname,
        };

		// this.props.createOrder(filters);
        this.props.loadKioskDetails(filters);
    }
    
    componentWillUnmount() {
        window.removeEventListener('resize', this._updateWindowDimensions);
    }

    _isEmpty(obj) {
        return Object.keys(obj).length === 0;
    }

	componentWillReceiveProps(nextProps) {
        // console.log("New props: " + JSON.stringify(nextProps));
        // console.log("Kiosk not found: " + nextProps.errors.data.kiosknotfound);
        // console.log("Kiosk details: " + JSON.stringify(nextProps.kioskDetails.kioskDetails))
		if (nextProps.errors) {
			this.setState({
				errors: nextProps.errors
			});


            if (nextProps.errors.data && nextProps.errors.data.kiosknotfound && this._isEmpty(nextProps.kioskDetails.kioskDetails)) {
                
                console.log("Setting count: ");

                this.setState({
                    validCount: this.state.validCount + 1,
                }, () => {
                    if (this.state.validCount > 1) {
                        console.log("Setting valid count: " + this.state.validCount);
                        this.setState({
                            isValid: false
                        });
                    }
                    console.log("Success doing it");
                    
                });
            } else {
                
            }
		}

        if (nextProps.kioskDetails && nextProps.kioskDetails.kioskDetails && !this._isEmpty(nextProps.kioskDetails.kioskDetails)) {
            console.log("Showing order screen.");
            let kioskDetails = nextProps.kioskDetails.kioskDetails;
            let dropBottle = kioskDetails.moduleStatus.refillStatus === 'On' ? false : true;
            let kioskUsable = true;
            if (kioskDetails.moduleStatus.dropBottleStatus === 'Off' && dropBottle) {
                kioskUsable = false;
            }
            this.setState({
                kioskDetails: kioskDetails,
                order: { ...this.state.order, dropBottle: dropBottle },
                kioskUsable: kioskUsable
            });
            this._hideAllLayouts();
            this._showOrderLayout();
            console.log("This is kiosk details: " + JSON.stringify(this.state.kioskDetails));
        }


        console.log("received new props: " );
	}

    _getTimeRemaining = (endtime) => {
        const total = Date.parse(endtime) - Date.parse(new Date());
        const seconds = Math.floor((total / 1000) % 60);
        const minutes = Math.floor((total / 1000 / 60) % 60);
        const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
        const days = Math.floor(total / (1000 * 60 * 60 * 24));
        
        return {
          total,
          days,
          hours,
          minutes,
          seconds
        };
      }
    
    
    _initializeClock = (endtime) => {
        const updateClock = () => {
          const t = this._getTimeRemaining(endtime);
      
          this.setState({
              seconds: ('0' + t.seconds).slice(-2),
              minutes: ('0' + t.minutes).slice(-2)
          });
      
          if (t.total <= 0) {
            clearInterval(timeinterval);
            this._showOrderLayout();
          } 
        }
      
        updateClock();
        const timeinterval = setInterval(updateClock, 1000);
      }
      
      
    _getConnectionId = () => {
        const params = {
            action: 'getConnectionId'
        };
        
        this.ws.send(JSON.stringify(params));
    }

    _connectToWS = () => {
        process.env.DEBUG === "true" && console.log('connecting to WS');
        this.ws = new WebSocket(URL);
        this.ws.onopen = () => {
            // on connecting, do nothing but log it to the console
            process.env.DEBUG === "true" && console.log('connected');
            process.env.DEBUG === "true" && console.log(this.ws);
            //     process.env.DEBUG === "true" && console.log("This is the connection Id: " + JSON.stringify(test));
            // });
            this._getConnectionId();
        };

        this.ws.onmessage = (event) => {
            // on receiving a message, add it to the list of messages
            // const message = JSON.parse(evt.data);
            console.log('Message received');
            // process.env.DEBUG === "true" && console.log(JSON.stringify(evt));
            console.log(JSON.stringify(event));
            var data = JSON.parse(event.data);
            if ('kioskConnected' in data) {  // kiosk connected
                console.log("kioskConnected");
                this.setState({
                    kioskConnectionId: data['kioskConnectionId']
                });
                // send order details
                process.env.DEBUG === "true" && console.log("Sending order details to kiosk: " + this.state.kioskConnectionId);
                const params = JSON.stringify({
                    action: 'chat',
                    data: this.state.order,
                    recipientId: this.state.kioskConnectionId
                });
                this.ws.send(params);
            } else if ('connectionId' in data) {
                console.log("Connection ID.")
                this.setState({
                    connectionId: data['connectionId']
                });
                process.env.DEBUG === "true" && console.log('ConnectionID: ' + this.state.connectionId);
                this._generateQRCode();
            } else if ('orderType' in data){
                this.setState({
                    orderType: data['orderType']
                });
                if (this.state.orderType === 'paid') {  // paid order, show payment screen
                    this._showPaymentScreen();
                } else {  // free drink, move to filling screen
                    this._showFillingScreen();
                }
            } else if('paymentStatus' in data) {
                if (data['paymentStatus'] === 'complete') {
                    this._showFillingScreen();
                }
            } else if ('orderStatus' in data) {

                // orderStatus - [ 'success', 'failed', 'cancelled', 'payment-error']
                if(data['orderStatus'] === 'success') {
                    this._successScreenHandleOpen();
                } else {
                    // show error screen
                    this._showOrderLayout(); // TODO - change to ERROR
                }
                console.log("Order complete: " + JSON.stringify(data));
                // error to be handled here
            }
            else {
                console.log("Message is not mapped");
            }
        };

        this.ws.onclose = () => {
            process.env.DEBUG === "true" && console.log('disconnected');
            // automatically try to reconnect on connection loss
            this.setState({
                ws: new WebSocket(URL)
            });
        };
    }

	_generateQRCode = (classes) => {
		process.env.DEBUG === "true" && console.log('This is the conn ID: ' + this.state.connectionId);
		qrcode
			.toDataURL(this.state.connectionId, { errorCorrectionLevel: 'H' })
			.then((url) => {
				process.env.DEBUG === "true" && console.log('URL');
                process.env.DEBUG === "true" && console.log(url);
                this._hideAllLayouts();
                this.setState({
                    qrCodeURI: url,
                    showQRCode: true
                });
                const deadline = new Date(Date.parse(new Date()) + MINUTES * SECONDS * 1000);
                this._initializeClock(deadline);
			})
			.catch((err) => {
				console.error(err);
			});
	};

    _getCaffeineReceiptDetails = () => {
        var tmp = {};
        switch (this.state.order.caffeine) {
            case 0:
                tmp = {
                    text: 'None / 0MG',
                    price: this._getPrice('supplement', 0),
                }
                break;
            case 1:
                tmp = {
                    text: 'Some / 20MG',
                    price: this._getPrice('supplement', 1),
                }
                break;
            case 2:
                tmp = {
                    text: 'More / 40MG',
                    price: this._getPrice('supplement', 2),
                }
                break;
            case 3:
                tmp = {
                    text: 'Extra / 70MG',
                    price: this._getPrice('supplement', 3),
                }
                break;
            default:
                break;
        }

        return tmp;
    }


    _getOrderTotalPrice = () => {
        var priceTmp = 0;

        // bottle type 
        if (this.state.order.dropBottle) {
            priceTmp += parseFloat(this._getPrice('dropbottle'));
        } else {
            priceTmp += parseFloat(this._getPrice('refill'));
        }
        // flavor
        if (this.state.order.flavor !== 'water') {
            priceTmp += parseFloat(this._getPrice('flavor'));
        }
        // supplement
        if (this.state.order.caffeine > 0) {
            console.log("caffeine called: " + this.state.order.caffeine);
            priceTmp += parseFloat(this._getPrice('supplement', this.state.order.caffeine));
            console.log("Price for caffeine: " + priceTmp);
        }
        
        console.log("Price is: " + priceTmp);
        return priceTmp;
    }

    _onSelectBottleType = (bottleType) => {
		console.log('Select bottle called');
        console.log(bottleType);

        this.setState({
            order: { ...this.state.order, dropBottleString: bottleType, dropBottle: bottleType === 'Compostable'},
            animate: { ...this.state.animate, compostable: bottleType === 'Compostable', refill: bottleType === 'Refill' },
        }, () => {
            this.setState({
                order: { ...this.state.order, price: this._getOrderTotalPrice() }
            });
        });
	};


	_onSelectFlavor = (flavor) => {
        console.log('Select flavor called');
        console.log(flavor);
        this.setState({
            order: { ...this.state.order, flavor: flavor},
            animate: {
                ...this.state.animate,
                guava: flavor === 'guava' ? true: false,
                lemonade: flavor === 'lemonade' ? true: false,
                cucumber: flavor === 'cucumber' ? true: false,
                water: flavor === 'water' ? true: false,
            }
        }, () => {
            this.setState({
                order: { ...this.state.order, price: this._getOrderTotalPrice() }
            });
        });

	};

	_onSelectCaffeine = (caffeine) => {
        console.log('Select caffeine called');
        console.log(caffeine);

        this.setState({
            order: { ...this.state.order, caffeine: caffeine },
            animate: {
                ...this.state.animate,
                caffeine: caffeine,
            }
        }, () => {
            var priceTot = this._getOrderTotalPrice();
            console.log("Price caffeine: " + priceTot)
            this.setState({
                order: { ...this.state.order, price: priceTot }
            });
        });
	};

	_onSelectTemperature = (temperature) => {
        // room - 1, cold - 2
		this.setState({
			order: { ...this.state.order, temperature: temperature, temperatureNumber: temperature === 'room' ? 1 : 2 },
            animate: {
                ...this.state.animate,
                temperature: temperature
            }
		});
	};

    _successScreenHandleClose = () => {
        this.setState({
            showSuccessScreen: false,
            showOrderLayout: true,
        });
    }

    _successScreenHandleOpen = () => {
        this._hideAllLayouts();
        this.setState({
            showSuccessScreen: true,
        });
        
    }

    _paymentScreenHandleClose = () => {
        this._hideAllLayouts();
        this.setState({
            showPaymentScreen: false
        });
    }

    _paymentScreenHandleOpen = () => {
        this._hideAllLayouts();
        this.setState({
            showPaymentScreen: true
        });
        
    }

	_sendNewOrder = () => {
		process.env.DEBUG === "true" && console.log('Sending new order...' + JSON.stringify(this.state.order));
    };
    
    _pourDrink = () => {
		process.env.DEBUG === "true" && console.log('Pouring');
    };
    
    _donePouring = () => {
        process.env.DEBUG === "true" && console.log('Done pouring');
        const params = JSON.stringify({
            action: 'chat',
            data: { 
                'dispenseCompleted': true 
            },
            recipientId: this.state.kioskConnectionId
        });
        
        this.ws.send(params);
        this._successScreenHandleOpen();
    };
    
    

    _handleTouchStart = () => {
        console.log("Touch start.")
        this.setState({
            touchstartTime: new Date()
        });
        const params = JSON.stringify({
            action: 'dispense',
            data: {
                'dispenseStart': true,
                'dispenseStatus': 'startSent',
                'uuid': this.state.uuid,
            },
            recipientId: this.state.kioskConnectionId
        });
        this.ws.send(params);
    }

    _handleTouchEnd = () => {
        console.log("Touch end");

        const params = JSON.stringify({
            action: 'dispense',
            data: {
                'dispenseEnd': true,
                'dispenseStatus': 'endSent',
                uuid: this.state.uuid,
            },
            recipientId: this.state.kioskConnectionId,
        });
        this.ws.send(params);
        this.setState({
            uuid: uuidv4()
        })
    }

    _onMouseDown = () => {
        console.log('omouse start');
        this._handleTouchStart();
    }

    _onMouseUp = () => {
        console.log('omouse end stop');
        this._handleTouchEnd();
    }

    _handleKioskChange = (event) => {
		this.setState({
			kioskNickname: event.target.value
		});

        if (this.state.kioskNickname && this.state.kioskNickname.length > 1) {
            this.setState({
                showProceedBtn: true
            });
        } else {
            this.setState({
                showProceedBtn: false
            });
        }

        console.log("Kiosk nickie: " + this.state.kioskNickname);
    }

    _onLoginClicked = () => {
        this.props.history.push("/login");
    }

    _getPrice = (valueStr, supplementLevel=1) => {
        var tmp = 0;
        try {
            switch (valueStr) {
                case "dropbottle":
                    tmp = this.state.kioskDetails.drinkprice.dropBottleWater;
                    break;
                case "refill":
                    tmp = this.state.kioskDetails.drinkprice.refillWater;
                    break;
                case "flavor":
                    if (this.state.order.dropBottle){
                        tmp = this.state.kioskDetails.drinkprice.dropBottleFlavor - this.state.kioskDetails.drinkprice.dropBottleWater;
                    } else {
                        tmp = this.state.kioskDetails.drinkprice.refillFlavor - this.state.kioskDetails.drinkprice.refillWater;
                    }
                    break;
                case "supplement":
                    if (this.state.order.dropBottle) {
                        tmp = this.state.kioskDetails.drinkprice.dropBottleSupplement * supplementLevel;
                    } else {
                        tmp = this.state.kioskDetails.drinkprice.refillSupplement * supplementLevel;
                    }
                    // console.log("here: " + tmp);
                    break;
                default:
                    break;
            }
        } catch (err) {
            console.log("Error communicating with kiosk.")
        }
        // console.log("Tmp for price is: " + tmp)
        tmp *= 1.0; // make temp into float
        if (typeof tmp === 'undefined') {
            tmp = 0.0;
        }

        if (tmp > 0.0) {
            tmp /= 100.0;
        }

        return tmp.toFixed(2);
    }

    _getPriceInt = (price) => {
        return parseFloat(price);
    }

	render() {

        console.log('Kiosk details dropBottleStat: ' + this.state.kioskDetails.moduleStatus.dropBottleStatus )
		return (
			<div className="valign-wrapper  animate__animated animate__slideInLeft noselect">
				<div className="row ">
                    { this.state.showOrderLayout && (
                        <>
                            <p style={{ marginLeft: "5%" }} className="menu-location">{`Pickup location: ` + this.state.kioskDetails.kiosk.location  }</p>
                            {/* Select bottle type */}

                            <div>
                                <h5 style={{ marginLeft: "5%" }} className="menu-select">Select Bottle Type</h5>
                                <p style={{ marginLeft: "5%" }} className="menu-subselect">Purchase a 100% compostable bottle or refill your own water bottle</p>
                                <Card style={{ width: "40%", marginRight: "5%", marginLeft: "5%", borderRadius: "7px" }} onClick={() => this._onSelectBottleType('Compostable')} className={`col s6 l6 m6 ${this.state.animate.compostable ? "animate__animated animate__pulse ": ""}  ${this.state.kioskDetails.moduleStatus.dropBottleStatus  === 'On' ? "" : 'out-of-stock'} ${this.state.order.dropBottle ? "selected" : ""}`} onAnimationEnd={() => this.setState({ animate: { ...this.state.animate, compostable: false } })}>
                                    <Card.Body>
                                        <div className="l5 s5 m5">
                                            <img border="0" alt="DropWater Logo" src={ icn_compostable } width="100%"  className="bottle-icon"/>    
                                            <p className="center-align menu-item">COMPOSTABLE</p>
                                            <p className="center-align menu-item-price">+ ${ this._getPrice('dropbottle') }</p>
                                        </div>
                                    </Card.Body>
                                </Card>
                                <Card style={{ width: "40%", marginRight: "5%", marginLeft: "5%", borderRadius: "7px" }} onClick={() => this._onSelectBottleType('Refill')}  className={`col s6 l6 m6 ${this.state.animate.refill ? "animate__animated animate__pulse": "" } ${ this.state.kioskDetails.moduleStatus.refillStatus  === 'On' ? "" : 'out-of-stock'} ${this.state.order.dropBottle ? "" : "selected"}`} onAnimationEnd={() => this.setState({ animate: { ...this.state.animate, refill: false }})}>
                                    <Card.Body>
                                        <div className="l5 s5 m5">
                                            <img border="0" alt="DropWater Logo" src={ icn_refill } width="100%"  className="bottle-icon"/>
                                            <p className="center-align menu-item">REFILL</p>
                                            <p className="center-align menu-item-price">+ ${ this._getPrice('refill') }</p>
                                        </div>
                                    </Card.Body>
                                </Card>
                                <p className="separator">&nbsp;</p>
                            </div>

                            
                            {/* Select bottle type */}

                            {/* Select flavor */}
                            <div>
                                <h5 style={{ marginLeft: "5%" }} className="menu-select">Select Your Flavor</h5>
                                <p style={{ marginLeft: "5%" }} className="menu-subselect">Choose from our selection of flavors. If you’re keeping it simple, just select water</p>
                                <Card style={{ width: "40%", marginRight: "5%", marginLeft: "5%", borderRadius: "7px" }} onClick={() => this._onSelectFlavor('guava')} className={`col s6 l6 m6 ${ this.state.kioskDetails.inventory.guava < CUT_OFF ? 'out-of-stock': '' } ${this.state.animate.guava ? "animate__animated animate__pulse" : ""} ${this.state.order.flavor === 'guava' ? "selected" : ""}`} onAnimationEnd={() => this.setState({ animate: { ...this.state.animate, guava: false } })}>
                                    <Card.Body>
                                        <div className="l5 s5 m5">
                                            <img border="0" alt="DropWater Logo" src={ icn_guava } width="100%" className="bottle-icon" />
                                            <p className="center-align menu-item">guava</p>
                                            <p className="center-align menu-item-price">+ ${ this._getPrice('flavor') }</p>
                                        </div>
                                    </Card.Body>
                                </Card>
                                <Card style={{ width: "40%", marginRight: "5%", marginLeft: "5%", borderRadius: "7px" }} onClick={() => this._onSelectFlavor('lemonade')}  className={`col s6 l6 m6 ${ this.state.kioskDetails.inventory.lemonade < CUT_OFF ? 'out-of-stock': '' } ${this.state.animate.lemonade ? "animate__animated animate__pulse": "" } ${this.state.order.flavor === 'lemonade' ? "selected" : ""}`} onAnimationEnd={() => this.setState({ animate: { ...this.state.animate, lemonade: false } })}>
                                    <Card.Body>
                                        <div className="l5 s5 m5">
                                            <img border="0" alt="DropWater Logo" src={ icn_lemonade } width="100%"  className="bottle-icon"/>
                                            <p className="center-align menu-item">lemonade</p>
                                            <p className="center-align menu-item-price">+ ${ this._getPrice('flavor') }</p>
                                        </div>
                                    </Card.Body>
                                </Card>
                                <Card style={{ width: "40%", marginRight: "5%", marginLeft: "5%", borderRadius: "7px" }} onClick={() => this._onSelectFlavor('cucumber')} className={`col s6 l6 m6 ${ this.state.kioskDetails.inventory.cucumber < CUT_OFF ? 'out-of-stock': '' } ${this.state.animate.cucumber ? "animate__animated animate__pulse": "" } ${this.state.order.flavor === 'cucumber' ? "selected" : ""}`} onAnimationEnd={() => this.setState({ animate: { ...this.state.animate, cucumber: false } })}>
                                    <Card.Body>
                                        <div className="l5 s5 m5">
                                            <img border="0" alt="DropWater Logo" src={ icn_cucumber } width="100%" className="bottle-icon" />
                                            <p className="center-align menu-item">cucumber</p>
                                            <p className="center-align menu-item-price">+ ${ this._getPrice('flavor') }</p>
                                        </div>
                                    </Card.Body>
                                </Card>
                                <Card style={{ width: "40%", marginRight: "5%", marginLeft: "5%", borderRadius: "7px" }} onClick={() => this._onSelectFlavor('water')}  className={`col s6 l6 m6 ${this.state.animate.water ? "animate__animated animate__pulse": "" } ${this.state.order.flavor === 'water' ? "selected" : ""}`} onAnimationEnd={() => this.setState({ animate: { ...this.state.animate, water: false } })}>
                                    <Card.Body>
                                        <div className="l5 s5 m5">
                                            <img border="0" alt="DropWater Logo" src={ icn_water } width="100%" className="bottle-icon" />
                                            <p className="center-align menu-item">water</p>
                                            <p className="center-align menu-item-price">+ $0.00</p>
                                        </div>
                                    </Card.Body>
                                </Card>
                                <p className="separator">&nbsp;</p>
                            </div>
                                
                            {/* Select flavor */}

                            {/* Select Caffeine level */}
                            <div>
                                <h5 style={{ marginLeft: "5%" }} className="menu-select">Select Caffeine Level</h5>
                                <p style={{ marginLeft: "5%" }} className="menu-subselect">Need an energy boost? Add some caffeine to your custom drink</p>
                                <Card style={{ width: "40%", marginRight: "5%", marginLeft: "5%", borderRadius: "7px" }} onClick={() => this._onSelectCaffeine(0)} className={`col s6 l6 m6 ${this.state.animate.caffeine === 0 ? "animate__animated animate__pulse" : "" } ${this.state.order.caffeine === 0 ? "selected" : ""}`} onAnimationEnd={() => this.setState({ animate: { ...this.state.animate, caffeine: -1 } })}>
                                    <Card.Body>
                                        <div className="l5 s5 m5">
                                            <img border="0" alt="DropWater Logo" src={ icn_caffeine_0_mg } width="100%"  className="bottle-icon"/>
                                            <p className="center-align menu-item">none / 0mg</p>
                                            <p className="center-align menu-item-price">+ $0.00</p>
                                        </div>
                                    </Card.Body>
                                </Card>
                                <Card style={{ width: "40%", marginRight: "5%", marginLeft: "5%", borderRadius: "7px" }} onClick={() => this._onSelectCaffeine(1)}  className={`col s6 l6 m6 ${ this.state.kioskDetails.inventory.caffeine < CUT_OFF ? 'out-of-stock': '' } ${this.state.animate.caffeine === 1 ? "animate__animated animate__pulse": "" } ${this.state.order.caffeine === 1 ? "selected" : ""}`} onAnimationEnd={() => this.setState({ animate: { ...this.state.animate, caffeine: -1 } })}>
                                    <Card.Body>
                                        <div className="l5 s5 m5">
                                            <img border="0" alt="DropWater Logo" src={ icn_caffeine_20_mg } width="100%" className="bottle-icon" />
                                            <p className="center-align menu-item">some / 20mg</p>
                                            <p className="center-align menu-item-price">+ ${ this._getPrice('supplement', 1) }</p>
                                        </div>
                                    </Card.Body>
                                </Card>
                                <Card style={{ width: "40%", marginRight: "5%", marginLeft: "5%", borderRadius: "7px" }} onClick={() => this._onSelectCaffeine(2)} className={`col s6 l6 m6 ${ this.state.kioskDetails.inventory.caffeine < CUT_OFF ? 'out-of-stock': '' } ${this.state.animate.caffeine === 2 ? "animate__animated animate__pulse": ""} ${this.state.order.caffeine === 2 ? "selected" : ""}`} onAnimationEnd={() => this.setState({ animate: { ...this.state.animate, caffeine: -1 } })}>
                                    <Card.Body>
                                        <div className="l5 s5 m5">
                                            <img border="0" alt="DropWater Logo" src={ icn_caffeine_40_mg } width="100%" className="bottle-icon" />
                                            <p className="center-align menu-item">more / 40mg</p>
                                            <p className="center-align menu-item-price">+ ${ this._getPrice('supplement', 2) }</p>
                                        </div>
                                    </Card.Body>
                                </Card>
                                <Card style={{ width: "40%", marginRight: "5%", marginLeft: "5%", borderRadius: "7px" }} onClick={() => this._onSelectCaffeine(3)}  className={`col s6 l6 ${ this.state.kioskDetails.inventory.caffeine < CUT_OFF ? 'out-of-stock': '' } ${this.state.animate.caffeine === 3 ? "animate__animated animate__pulse": "" } m6 ${this.state.order.caffeine === 3 ? "selected" : ""}`} onAnimationEnd={() => this.setState({ animate: { ...this.state.animate, caffeine: -1 } })}>
                                    <Card.Body>
                                        <div className="l5 s5 m5">
                                            <img border="0" alt="DropWater Logo" src={ icn_caffeine_70_mg } width="100%" className="bottle-icon" />
                                            <p className="center-align menu-item">extra / 70mg</p>
                                            <p className="center-align menu-item-price">+ ${ this._getPrice('supplement', 3) }</p>
                                        </div>
                                    </Card.Body>
                                </Card>
                                <p className="separator">&nbsp;</p>
                            </div>


                            {/* select caffeine level */}

                            {/* Select temperature */}
                            <div>
                                <h5 style={{ marginLeft: "5%" }} className="menu-select">Select Your Temperature</h5>
                                <p style={{ marginLeft: "5%" }} className="menu-subselect">Make your drink the perfect temperature for you</p>
                                <Card style={{ width: "40%", marginRight: "5%", marginLeft: "5%", borderRadius: "7px" }} onClick={() => this._onSelectTemperature('room')} className={`col s6 l6 m6 ${this.state.animate.temperature === 'room' ? "animate__animated animate__pulse": ""} ${this.state.order.temperature  === 'room' ? "selected" : ""}`} onAnimationEnd={() => this.setState({ animate: { ...this.state.animate, temperature: 'none' } })}>
                                    <Card.Body>
                                        <div className="l5 s5 m5">
                                            <img border="0" alt="DropWater Logo" src={ icn_room } width="100%" className="bottle-icon" />
                                            <p className="center-align menu-item">room temp</p>
                                            <p className="center-align menu-item-price">+ $0.00</p>
                                        </div>
                                    </Card.Body>
                                </Card>
                                <Card style={{ width: "40%", marginRight: "5%", marginLeft: "5%", borderRadius: "7px" }} onClick={() => this._onSelectTemperature('cold')}  className={`col s6 l6 m6 ${this.state.animate.temperature === 'cold' ? "animate__animated animate__pulse": "" } ${this.state.order.temperature  === 'cold' ? "selected" : ""}`} onAnimationEnd={() => this.setState({ animate: { ...this.state.animate, temperature: 'none' } })}>
                                    <Card.Body>
                                        <div className="l5 s5 m5">
                                            <img border="0" alt="DropWater Logo" src={ icn_cold } width="100%"  className="bottle-icon"/>
                                            <p className="center-align menu-item">ice cold</p>
                                            <p className="center-align menu-item-price">+ $0.00</p>
                                        </div>
                                    </Card.Body>
                                </Card>
                                <p className="separator">&nbsp;</p>
                            </div>
                            {/* Select temperature */}

                            <hr ></hr>
                            <div className="center-align order-price">
                                Total: ${ this.state.order.price.toFixed(2) }
                            </div>
                            {/* Order Btns */}
                            <Card className={`col s12 l12 m12 center-align confirm-btn btn-flat ${ this.state.kioskUsable ? '' : 'out-of-stock' }`} style={{ marginLeft: "6%", marginRight: "6%", width: "88%", borderRadius: "30px", height: "55px", background: "#6ce3ff" }} onClick={() => this._connectToWS()}>
                                <Card.Body>
                                    <Card.Text className="" style={{ marginTop: "2%" }}>
                                        CONFIRM
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </ >
                    )}
                    { this.state.showQRCode  && (
                        <div className="center">
                            <h5 style={{ marginLeft: "5%", marginTop: "15%" }} className="menu-select center-align">Order Created</h5>
                            <p style={{ width: "88%", marginLeft: "6%", marginRight: "6%" }} className="menu-subselect center-align wrap">{`Pickup location : ` + this.state.kioskDetails.kiosk.location + `. Scan QR code at kiosk to continue`}</p>
                            <Card className="col s12 l12 m12 receipt center" style={{ borderRadius: "10px", width: "87%", marginLeft: "6.5%", marginRight: "6.5%"  }}>
                                <Card.Body className="center-align center">
                                    <Image src={this.state.qrCodeURI} thumbnail style={{ width: "45%" }}/>
                                    {/* <p>{ this.state.connectionId } </p> */}
                                    <table style={{ width: "70%", marginTop: "5%", marginLeft: "", marginRight: "auto" }} className="receipt-tbl center-align">
                                        <tbody>
                                            <tr className="no-bottom-border">
                                                <td className="right-receipt-item">{ this.state.order.dropBottle ? 'Compostable': 'Refill' }</td><td className="left-receipt-item">+ ${ this.state.order.dropBottle ? this._getPrice('dropbottle'): this._getPrice('refill') }</td>
                                            </tr>
                                            <tr className="no-bottom-border">
                                                <td className="right-receipt-item">{ this.state.order.flavor }</td><td className="left-receipt-item">+ ${ this.state.order.flavor === 'water'? '0.00': this._getPrice('flavor') }</td>
                                            </tr>
                                            <tr className="no-bottom-border">
                                                <td className="right-receipt-item">{ this._getCaffeineReceiptDetails().text }</td><td className="left-receipt-item">+ ${ this._getCaffeineReceiptDetails().price }</td>
                                            </tr>
                                            <tr className="no-bottom-border">
                                                <td className="right-receipt-item">{ this.state.order.temperature === 'cold' ? 'Ice Cold': 'Room Temp' }</td><td className="left-receipt-item">+ $0.00</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    {/* <p>{ this.state.connectionId }</p> */}
                                    <hr ></hr>
                                    <h5 style={{ marginLeft: "5%", marginBottom: "10%" }} className="menu-select">Total : ${ this.state.order.price.toFixed(2) }</h5>
                                    
                                </Card.Body>
                                {/* <Card.Footer className="text-muted center" style={{ fontSize: "15px", marginTop: "5%", marginBottom: "5%" }}>Expires in { this.state.minutes }:{ this.state.seconds }</Card.Footer> */}
                            </Card>
                            <Card className="col s12 l12 m12 center-align edit-order-btn" style={{  borderRadius: "30px", height: "55px", width: "88%", marginLeft: "6%", marginRight: "6%" }} onClick={() => this._showOrderLayout()}>
                                <Card.Body>
                                    <Card.Text className="">
                                        EDIT ORDER
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </div>
                    )}

                    {/* Welcome screen  */}
                    { this.state.showWelcomeScreen  && (
                        <div>
                            <h5 style={{ marginLeft: "5%", marginTop: "15%" }} className="menu-select center-align">Contactless Order</h5>
                            <p style={{ width: "88%", marginLeft: "6%", marginRight: "6%" }} className="menu-subselect center-align wrap">{`Please use your camera to scan the QR Code displayed on the machine or simply type in the machine alias below.`}</p>
                            <Card className="col s12 l12 m12 welcome" style={{ borderRadius: "10px", width: "88%", marginLeft: "6%", marginRight: "6%" }}>
                                <Card.Body className="center-align">
                                    <div className="col s7 drop-link" style={{ marginRight: "0px", paddingRight: "0px" }}>
                                        dropwater.co/create/
                                    </div>
                                    <div className="input-field col s5">
										<form noValidate onSubmit={this.onSubmit}>
                                        <input
                                                id="kiosk-nickname"
                                                type="text"
                                                className="validate"
                                                value={this.state.kioskNickname}
                                                onChange={this._handleKioskChange}
                                                // className="col s6"
                                                placeholder="machine alias"
                                        />
                                        </form>
										{!this.state.isValid && (
											<label htmlFor="kiosk-nickname" style={{ color: "red", marginTop: "-15%" }}>
												Alias incorrect.
											</label>
										)}
									</div>
                                    {(!this.props.auth.isAuthenticated && (
                                        <div className="col s6 right-align " style={{ paddingLeft: "11.250px", marginTop: "1.5rem" }}>
                                            
                                            <Link href="#" onClick={() =>
											this._onLoginClicked()} color="blue" 
                                            style={{
                                                // width: "150px",
                                                // borderRadius: "3px",
                                                letterSpacing: "1.5px",
                                                marginTop: "1rem",
                                                marginBottom: '1rem',
                                            }}
                                            > 
                                                Login
                                            </Link>
                                        </div>
                                    ))}
                                    {/* { this.state.showProceedBtn &&  */}
                                    {/* lassName={`collapsed ${isExpanded ? 'is-expanded animate__animated animate__slideInRight' : '' + this.state.width < 800 ? ' is-expanded animate__animated animate__slideOutRight': ''}`} */}
                                    <div className={`col ${this.props.auth.isAuthenticated ? 's12': 's6'} right-align`}>
                                        <button
												style={{
													width: '150px',
													borderRadius: '3px',
													letterSpacing: '1px',
													marginTop: '1rem',
													marginBottom: '1rem',
													fontSize: '13px',
                                                    color: "white"
													// backgroundColor: '#ff5733'
												}}
												className="btn btn-small blue btn-flat"
												onClick={() => this._loadKioskData()}
											>
												Proceed
											</button>
                                    </div>
                                    {/* } */}
                                    
                                    
                                </Card.Body>
                                {/* <Card.Footer className="text-muted center" style={{ fontSize: "15px", marginTop: "5%", marginBottom: "5%" }}>Expires in { this.state.minutes }:{ this.state.seconds }</Card.Footer> */}
                            </Card>
                        </div>
                    )}
                    {/* End of welcome screen */}

                    {/* Compostable progress bar  */}
                    { this.state.showProgressBar && 
                    <div>
                        <h5 style={{ marginLeft: "5%", marginTop: "15%" }} className="menu-select center-align">Preparing your drink.</h5>
                        <p style={{ marginLeft: "5%" }} className="menu-subselect center-align wrap">Please wait to pick up your bottle.</p>
                        <Card className="col s12 l12 m12 center-align drop-progress" style={{ flexGrow: 1, width: this.state.width * .8, borderRadius: "10px" }}>
                            <Card.Body>
                                
                                <ReactLoading type={"spinningBubbles"} color={"#58cbe1"} height={'70%'} width={'70%'} className="center-align drop-progress-spinner"/>
                            </Card.Body>
                            {/* <Card.Footer className="text-muted center" style={{ fontSize: "15px" }}>Expires in { this.state.minutes }:{ this.state.seconds }</Card.Footer> */}
                        </Card>
                    </div>
                    }
                    
                    {/* Refill screen btns */}
                    { this.state.showRefillScreen &&
                        <div className="center-align s12 l12 m12">
                            <h5 style={{ marginLeft: "5%", marginTop: "15%" }} className="menu-select center-align">Dispense Drink</h5>
                            <p style={{ marginLeft: "5%" }} className="menu-subselect center-align wrap">{`Place container under machine and use your device to dispense drink`}</p>
                            <Card className="col s12 l6 m6 refill-card" style={{ flexGrow: 1,  borderRadius: "10px" }}>
                            <Card.Body>
                                <Card.Title style={{ fontWeight: '500', fontSize: "20px", textAlign: 'center' }} >
                                    {/* Place your bottle in the refill */}
                                </Card.Title>
                                <div className="col s12 l12 m12 center-align">
                                    <button
                                        style={{
                                            width: "175px",
                                            height: "175px",
                                            borderRadius: "50%",
                                            letterSpacing: "1px",
                                            marginTop: "0.5rem",
                                            // fontSize: "25px",
                                            marginBottom: "1rem",
                                            // userSelect: "none"
                                        }}
                                        className="btn btn-large pour-button"
                                        // onClick={() =>
                                        //     this._pourDrink()
                                        // }
                                        onTouchStart={ this._handleTouchStart }
                                        onTouchEnd={this._handleTouchEnd}
                                        onMouseDown={this._onMouseDown}
                                        onMouseUp={this._onMouseUp}
                                    >
                                        Pour
                                    </button>
                                </div>
                                <div className="col s12 l12 m12 center-align">
                                    <button
                                        style={{
                                            width: "175px",
                                            height: "175px",
                                            borderRadius: "50%",
                                            letterSpacing: "1px",
                                            marginTop: "0.5rem",
                                            // fontSize: "25px",
                                            marginBottom: "1rem"
                                            // marginRight: "10px"
                                            // backgroundColor: "#2471a3",
                                        }}
                                        className="btn btn-large done-button"
                                        onClick={() =>
                                            this._donePouring()
                                        }
                                    >
                                        done
                                    </button>
                                    </div>
                            </Card.Body>
                            {/* <Card.Footer className="text-muted center" style={{ fontSize: "15px" }}>Expires in { this.state.minutes }:{ this.state.seconds }</Card.Footer> */}
                        </Card>
                        </div>
                    }

                    { this.state.showSuccessScreen && 
                    <div>
                        <Card className="col s12 l12 m12 receipt" style={{ borderRadius: "10px",  marginTop: "20%", width: "88%", marginLeft: "6%", marginRight: "6%" }}>
                            <Card.Body className="center-align">
                                <div className="l12 s12 m12" style={{ marginTop: "20%", marginBottom: "15%" }}>
                                            <img border="0" alt="DropWater Logo" src={ success_badge }  />
                                        </div>
                                <h5 style={{  }} className="pop-up-select">Order Complete</h5>
                                <p className="pop-up-subselect">Thank you for your order.</p>
                                <p className="pop-up-subselect2">Enjoy your drink!</p>
                            </Card.Body>
                            {/* <Card.Footer className="text-muted center" style={{ fontSize: "15px", marginTop: "5%", marginBottom: "5%" }}>Expires in { this.state.minutes }:{ this.state.seconds }</Card.Footer> */}
                        </Card>
                        <Card className="col s12 l12 m12 center-align done-btn btn-flat" style={{ borderRadius: "30px", height: "55px", width: "88%", marginLeft: "6%", marginRight: "6%" }}>
                            <Card.Body>
                                <Card.Text className="">
                                    DONE
                                </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card className="col s12 l12 m12 center-align order-again-btn" style={{ borderRadius: "30px", height: "55px", width: "88%", marginLeft: "6%", marginRight: "6%" }} onClick={() => this._resetOrderCreate()}>
                            <Card.Body>
                                <Card.Text className="">
                                    ORDER AGAIN
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </div>
                    // <>
                    // <Dialog open={this.state.showSuccessScreen} onClose={this._handleCloseModal} aria-labelledby="form-dialog-title" className="col s12 l12 m12">
					// 					<DialogTitle id="form-dialog-title" className="center">
					// 						<img border="0" alt="DropWater Logo" src={ success_badge }  width="30%" style={{ marginLeft: "25%", marginRight: "25%", marginTop: "20%" }}/>
					// 					</DialogTitle>
					// 					<DialogContent className="center">
					// 					<DialogContentText>
					// 						<h5 style={{  }} className="pop-up-select">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Order Complete&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h5>
                    //                         <p className="pop-up-subselect">Thank you for your order.</p>
                    //                         <p className="pop-up-subselect2">Enjoy your drink!</p>
					// 					</DialogContentText>
					// 					</DialogContent>
					// 	</Dialog>
                    //     <Dialog open={this.state.showSuccessScreen} onClose={this._handleCloseModal} aria-labelledby="form-dialog-title" className="col s12 l12 m12">
					// 					{/* <DialogTitle id="form-dialog-title2" className="center">
					// 						<img border="0" alt="DropWater Logo" src={ success_badge }  width="30%" style={{ marginLeft: "25%", marginRight: "25%", marginTop: "20%" }}/>
					// 					</DialogTitle> */}
					// 					<DialogContent className="center">
					// 					<DialogContentText>
					// 						<h5 style={{  }} className="pop-up-select">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Order Complete&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h5>
                    //                         <Card className="col s12 l12 m12 center-align edit-order-btn-2" style={{ borderRadius: "30px", height: "55px", width: "100%" }} onClick={() => this._showOrderLayout()}>
                    //                         <Card.Body>
                    //                             <Card.Text className="pc-edit-order-btn">
                    //                                 DONE
                    //                             </Card.Text>
                    //                         </Card.Body>
                    //                     </Card>
                    //                     <Card className="col s12 l12 m12 center-align edit-order-btn-2" style={{ borderRadius: "30px", height: "55px", width: "100%" }} onClick={() => this._showOrderLayout()}>
                    //                         <Card.Body>
                    //                             <Card.Text className="pc-edit-order-btn">
                    //                                 DONE
                    //                             </Card.Text>
                    //                         </Card.Body>
                    //                     </Card>
					// 					</DialogContentText>
					// 					</DialogContent>
										
					// 	</Dialog>
                        
                    //     </>
                    }

                    { this.state.showPaymentScreen && 
                        <div>
                        <h5 style={{ marginLeft: "5%", marginTop: "15%"}} className="menu-select center-align">Confirm And Pay</h5>
                        <p style={{ marginLeft: "5%", width: "80%"  }} className="menu-subselect center-align">{`Please use the card reader on the kiosk to make the payment`}</p>
                        <div className="center-align col s12 l12 m12" style={{ marginTop: "25%", marginBottom: "10%" }}>
                            <Image src={ card_reader } thumbnail style={{ width: "35%" }}/>  
                        </div>
                        

                        <Card className="col s12 l12 m12 center-align cancel-order-btn" style={{  borderRadius: "30px", height: "55px", width: "87%", marginLeft: "6%", marginRight: "6%" }} onClick={() => this._showOrderLayout()}>
                            <Card.Body>
                                <Card.Text className="">
                                    CANCEL ORDER
                                </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card className="col s12 l12 m12 center-align edit-order-btn-2" style={{ borderRadius: "30px", height: "55px", width: "87%", marginLeft: "6%", marginRight: "6%"  }} onClick={() => this._showOrderLayout()}>
                            <Card.Body>
                                <Card.Text className="pc-edit-order-btn">
                                    EDIT ORDER
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </div>
                    }
				</div>
			</div>
		);
	}
}

OrderCreate.propTypes = {
    loadKioskDetails: PropTypes.func.isRequired,
    // inventory: PropTypes.object.isRequired,
    kioskDetails: PropTypes.object.isRequired,
	createOrder: PropTypes.func.isRequired,
	errors: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,

};

const mapStateToProps = (state) => ({
    kioskDetails: state.kioskDetails,
	errors: state.errors,
    auth: state.auth,
});
export default connect(mapStateToProps, {
    loadKioskDetails,
	createOrder
})(withRouter(OrderCreate));
