import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { createOrder, loadKiosk } from '../../actions/orderCreateActions';
import { loadKioskDetails } from '../../actions/PropagateActions';
import { Card, FormLabel, Image } from 'react-bootstrap';
import 'react-datepicker/dist/react-datepicker.css';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import '../../css/date_picker.css';
import { FormControl, InputLabel, Input, FormHelperText, RadioGroup, FormControlLabel, Radio } from '@material-ui/core';
import LinearProgress from '@material-ui/core/LinearProgress';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


// Images
import icn_compostable from '../../images/icn-compostable.svg';
import icn_refill from '../../images/icn-refill.svg';
import icn_caffeine_0_mg from '../../images/icn-caffeine-0-mg.svg';
import icn_caffeine_20_mg from '../../images/icn-caffeine-20-mg.svg';
import icn_caffeine_40_mg from '../../images/icn-caffeine-40-mg.svg';
import icn_caffeine_70_mg from '../../images/icn-caffeine-70-mg.svg';
import icn_cucumber from '../../images/icn-cucumber.svg';
import icn_guava from '../../images/icn-guava.svg';
import icn_lemonade from '../../images/icn-lemonade.svg';
import icn_water from '../../images/icn-water.svg';
import icn_room from '../../images/icn-room.png';
import icn_cold from '../../images/icn-cold.png';
import card_reader from '../../images/card-reader.svg';
import success_badge from '../../images/success-badge.svg';
import { TrainRounded } from '@material-ui/icons';


class Welcome extends Component {
	constructor() {
        super();
		this.state = {
            errors: {},
            isValid: true,
        };

        this._updateWindowDimensions = this._updateWindowDimensions.bind(this);
	}



    _loadKioskData = () => {
        const filters = {
			order: this.state.order,
            kioskNickname: this.state.kioskNickname,
        };

		// var test = this.props.loadKiosk(filters);
        // console.log("Test: " + test);
        // this.forceUpdate();  // remove this as it's dangerous

    }


	componentDidMount() {
        window.addEventListener('resize', this._updateWindowDimensions);
        const filters = {
            kioskNickname: "aqua"
        }
		this.props.loadKioskDetails(filters);
    }
    
    componentWillUnmount() {
        window.removeEventListener('resize', this._updateWindowDimensions);
    }

	componentWillReceiveProps(nextProps) {
		if (nextProps.errors) {
			this.setState({
				errors: nextProps.errors
			});
		}

        console.log("received new props: " );
	}

    _handleKioskChange = (event) => {
		this.setState({
			kioskNickname: event.target.value
		});

        if (this.state.kioskNickname && this.state.kioskNickname.length > 1) {
            this.setState({
                showProceedBtn: true
            });
        } else {
            this.setState({
                showProceedBtn: false
            });
        }

        console.log("Kiosk nickie: " + this.state.kioskNickname);
    }

    _updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

	render() {
        console.log("Order create is: " + JSON.stringify(this.props));
        const summary = this.props.ordercreate;
        
        // process.env.DEBUG === "true" && console.log('Order create working: ' + this.state.progress + ' ' + this.state.width );
        
        console.log("Render...")
		return (
			<div className="valign-wrapper  animate__animated animate__slideInLeft">
				<div className="row ">{/* Welcome screen  */}
                    <div>
                            <h5 style={{ marginLeft: "5%", marginTop: "15%" }} className="menu-select center-align">Contactless Order</h5>
                            <p style={{ marginLeft: "5%" }} className="menu-subselect center-align wrap">{`Please use your camera to scan the QR Code displayed on the machine or \nsimply type in the machine alias below.`}</p>
                            <Card className="col s12 l12 m12 receipt" style={{ borderRadius: "10px", marginRight: "5%", marginLeft: "5%", width: "90%" }}>
                                <Card.Body className="center-align">
                                    {/* <p>{ this.state.connectionId } </p> */}
                                    {/* <div className="col s6 drop-link">
                                    dropwater.co/create/
                                    </div> */}
                                    <div className="col s6 drop-link">
                                        dropwater-dashboard.herokuapp.com/create/
                                    </div>
                                    <div className="input-field col s6">
										<input
											id="kiosk-nickname"
											type="text"
											class="validate"
											// value={this.state.password}
											onChange={this._handleKioskChange}
                                            // className="col s6"
                                            placeholder="machine alias"
										/>
										{this.state.isValid === false && (
											<label for="password" style={{ color: 'red' }}>
												Machine not found. Please try again.
											</label>
										)}
										{/* {this.state.isValid === true && (
											<label for="password" style={{}}>
												Machine Alias
											</label>
										)} */}
									</div>
                                    { this.state.showProceedBtn && 
                                    <div className="col s12 right-align">
                                        <button
												style={{
													width: '150px',
													borderRadius: '3px',
													letterSpacing: '1px',
													marginTop: '1rem',
													marginBottom: '1rem',
													fontSize: '13px',
													// backgroundColor: '#ff5733'
												}}
												className="btn btn-small blue hoverable"
												onClick={() => this._loadKioskData()}
											>
												Proceed
											</button>
                                    </div>
                                    }
                                    {/* {(!this.props.auth.isAuthenticated && ( */}
                                        <div className="col s12" style={{ paddingLeft: "11.250px" }}>
                                            <button
                                            style={{
                                                width: "150px",
                                                borderRadius: "3px",
                                                letterSpacing: "1.5px",
                                                marginTop: "1rem",
                                            }}
                                            type="submit"
                                            className="btn btn-large waves-effect waves-light hoverable teal white-text"
                                            >
                                                <i className="material-icons right">lock_open</i>
                                            Login
                                            </button>
                                        </div>
                                    {/* ))} */}
                                    
                                </Card.Body>
                                {/* <Card.Footer className="text-muted center" style={{ fontSize: "15px", marginTop: "5%", marginBottom: "5%" }}>Expires in { this.state.minutes }:{ this.state.seconds }</Card.Footer> */}
                            </Card>
                        </div>
                    {/* End of welcome screen */}


					
				</div>
			</div>
		);
	}
}

Welcome.propTypes = {
    loadKioskDetails: PropTypes.func.isRequired,
    // inventory: PropTypes.object.isRequired,
    // drinkPrice: PropTypes.object.isRequired,
	errors: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
    kioskDetails: state.kioskDetails,
	errors: state.errors
});
export default connect(mapStateToProps, {
    loadKioskDetails,
})(withRouter(Welcome));
