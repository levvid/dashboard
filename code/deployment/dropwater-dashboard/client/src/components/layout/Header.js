import React from 'react';
import header_img from '../../images/wave-header@2x.png';
import logoMobile from '../../images/drop_logo_white_shadow_mobile.png';
import { Link } from 'react-router-dom';
import logo from '../../images/drop_logo_white_shadow.png';
import PropTypes from "prop-types";
import { connect } from "react-redux";

class Header extends React.Component {
	constructor() {
		super();
		this.state = {
			width: window.innerWidth,
			height: window.innerHeight
		};
		this._updateWindowDimensions = this._updateWindowDimensions.bind(this);
	}

	componentDidMount() {
		window.addEventListener('resize', this._updateWindowDimensions);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this._updateWindowDimensions);
	}

	_updateWindowDimensions() {
		this.setState({ width: window.innerWidth, height: window.innerHeight });
	}

	render() {
		// var path = new Path()
		// .M(0,249.98)
		// .C(149.99,150,271.49,-49.98,500,49.98)
		// .L(500,0)
		// .L(0,0)
		// .close();
        let logoImage = logo;
        if (this.state.width < 700) {
            logoImage = logoMobile;
            // console.log("Mobile detected");
        } else {
        // console.log("Web browser detected");
        }
		return (
			<div>
			{/* // 	{(!this.props.auth.isAuthenticated && ( */}
            {/* //         <div className="logo">
          
			// 		<Link to="/overview">
			// 			<div>
			// 				<img */}
            {/* //                 	className="logo-image"
			// 					src={logoImage}
			// 					width="250"
			// 					style={{ */}
			{/* // 						marginTop: '9px',
			// 						marginBottom: '0px',
            //                         marginLeft: '1%',
            //                         overflow: "hidden",
			// 						position: "absolute",
			// 					}}
			// 					alt="Logo"
			// 				/>
			// 			</div> */}
			{/* // 		</Link> */}
			{/* // 	</div> */}
                {/* // ))} */}
				<div style={{ position: 'absolute', zIndex: '-1' }} className="wave-header">
					<img className="wave-header-img" border="0" alt="DropWater Logo" src={header_img} width="100%" />
				</div>
			</div>
		);
	}
}


Header.propTypes = {
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors,
});
export default connect(mapStateToProps, { })(Header);
