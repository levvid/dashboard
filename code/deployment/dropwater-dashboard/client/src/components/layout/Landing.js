import React, { Component } from "react";
import { Link } from "react-router-dom";

class Landing extends Component {
  render() {
    return (
      <div style={{ height: "100vh" }} className="valign-wrapper">
        
        <div className="row">
        
          <div className="col s12 center-align">
            <h4>Welcome</h4>
            <br />
            <div className="col s12 l6 m6 mb2 center-align" style={{ marginBottom: "5%" }}>
              <Link
                to="/create"
                style={{
                  width: "225px",
                  borderRadius: "3px",
                  letterSpacing: "1.5px",
                  marginRight: "5px"
                }}
                className="btn btn-large waves-effect waves-light hoverable blue accent-3"
              >
                  <i className="material-icons right">add_business</i>
                Create Order
              </Link>
            </div>
            <div className="col s12 l6 m6 center-align" style={{ marginBottom: "5%" }}>
              <Link
                to="/login"
                style={{
                  width: "155px",
                  borderRadius: "3px",
                  letterSpacing: "1.5px",
                  //backgroundColor: "#41768b"
                }}
                className="btn btn-large waves-effect waves-light hoverable teal white-text"
              >
                <i className="material-icons right">lock_open</i>
                Log In
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Landing;
