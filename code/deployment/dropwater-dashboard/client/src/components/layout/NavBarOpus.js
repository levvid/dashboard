import React, { Component } from 'react';
import { NavLink, Link, withRouter } from 'react-router-dom';
import 'font-awesome/css/font-awesome.min.css';
import { logoutUser } from '../../actions/authActions';
import styled from 'styled-components';
import logo from '../../images/drop_logo_white_shadow.png';
// import logoMobile from '../../images/wave-header@3x.png';
// import logoMobile from '../../images/wave-header@3x.png';

import logoMobile from '../../images/logo.png';
// import logoMobile from '../../images/drop_logo_white_shadow_mobile.png';
import { connect } from 'react-redux';
// import Header from 'Header.js';
import Avatar from '@material-ui/core/Avatar';


const Navigation = styled.header`
  width: 100%;
  z-index: 1000;
  display: flex;
  justify-content: space-between;
  align-items: center;
  // background: #2471a3;
  background: transparent;
  // background: url('../images/wave-header@3x.png');

  .logo a {
    display: flex;
    flex-direction: column;
    clear: both;
  }
  a {
    color: #222;
    opacity: 1;
    transition: all 0.6s;
    color: #222;
    font-size: 1em;
  }
  
  .fa-bars {
    display: none;
    color: #2471a3;
    font-size: 3.5rem;
  }
  nav {
      margin-left: 0%;
      // background-color: #2471a3;
      background-color: transparent;
      box-shadow: none;
      webkit-box-shadow: none;
      line-height: 40px;
      margin-top: 15px;
    ul {
      display: flex;
      justify-content: flex-end;
      
    }
    li {
      margin: 0 15px;
      justify-content: flex-end;
      font-size: 1.1em;
    }
    a {
      font-size: 1em;
      text-decoration: none;
      .active {
        
      }
    }
    a.active {
      color: #fff;
      border-style: solid;
      border-width: thin;
      border-radius: 30px;
    }
    a:hover {
        opacity: 1;
        // background-color: #2F4F4F;
        background-color: #fff;
        border-radius: 30px;
        border-style: none;
        color: #2471a3;
      }
  }

  @media only screen and (max-width: 800px) {
    padding: 0px;
    ul {
      background-color: #00C4E9;
      border-radius: 25px;
    }
    border-style: none;
    .logo {
      padding-left: 15px;
      padding-top: 0px !important;
    }
    a.active {
        border-style: none;
    }
  }
  
  @media only screen and (max-width: 600px) {
    height: auto;
    
    img {
      // max-width: 200px;
    }
    ul {
      position: fixed;
      right: 0px;
      margin-right: 0px;
      align-text: center;
      max-width: 100%;
      /* margin-top: 0px; */
      /* padding-top: 0px; */
      top: 0px;
      background-color: #3c4b64;
    }
    min-height: 50px;
    display: block;
    position: relative;
    .logo {
      width: 100%;
      display: block;
      margin: 0px;
      margin-left: -5px;
    
    }
    nav {
        line-height: 35px;
        a.active {
            border-style: none;
            border-radius: 0px !important;
        }
        a {
          border-radius: 0px;
        }
        padding-right: 0px;
    }

    li, ul {
      border-radius: 0px;
    }

    .fa-bars {
      display: inline-block;
      position: absolute;
      top: 10px;
      right: 10px;
      cursor: pointer;
    }
    ul.collapsed {
      width: 100%;
      display: flex;
      flex-direction: column;
      justify-content: flex-start;

      overflow: hidden;
      max-height: 0;
      &.is-expanded {
        // overflow: hidden;
        max-height: 1000px; /* approximate max height */
        width: 50%;
        height: 1000px;
      li {
        // padding: 15px 10px;  
        margin: 0px 0px;
        // width: 100%;
      }
    }
  }
`;

class Nav extends Component {
	constructor(props) {
		super(props);
		this.state = {
      isExpanded: false,
      width: window.innerWidth,
      height: window.innerHeight,
      showIcons: false
    };
    
    this.handleToggle = this.handleToggle.bind(this);
    this.handleOutsideClick = this.handleOutsideClick.bind(this);
    this._getIcon = this._getIcon.bind(this);
	}
	handleToggle(e) {
    // e.preventDefault();
    process.env.DEBUG === "true" && console.log("Handling toggle");
		
    if (!this.state.isExpanded) {
      // attach/remove event handler
      document.addEventListener('click', this.handleOutsideClick, false);
    } else {
      document.removeEventListener('click', this.handleOutsideClick, false);
    }
    // this.setState({
		// 	isExpanded: !this.state.isExpanded
    // });

    this.setState(prevState => ({
      isExpanded: !this.state.isExpanded
   }));
  }
  

  _updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }
  handleOutsideClick(e) {
    process.env.DEBUG === "true" && console.log(e)
    // ignore clicks on the component itself
    // if (this.node.contains(e.target)) {
    //   return;
    // }
    if (document.getElementsByClassName("collapsed")[0] && document.getElementsByClassName("collapsed")[0].contains(e.target)) {
      return;
    }
    // document.getElementsByClassName("collapsed")[0].classList.remove(" animate__slideInRight");
    // document.getElementsByClassName("collapsed")[0].classList.add(" animate__slideOutRight");
    this.handleToggle(e);
  }

	onLogoutClick = (e) => {
		e.preventDefault();
		this.props.logoutUser();
  };

  
  componentDidMount() {
    this._updateWindowDimensions();
    if (this.state.width < 800) {
      this.setState({
        showIcons: true
      });
    } else {
      this.setState({
        showIcons: false
      });
    }
    window.addEventListener('resize', this.updateWindowDimensions);
  }
  
  _getIcon = (icon) => {
    if (this.state.showIcons){
      if (icon === 'dashboard'){
        return ( 
          <>
          <i className="material-icons left">dashboard</i>
          </ >)
      } else if (icon === 'home'){
        return ( 
          <>
          <i className="material-icons left">people_alt</i>
          </ >)
      }  else if (icon === 'business_center') {
        return ( 
          <>
          <i className="material-icons left">business_center</i>
          </ >)
        
      } else if (icon === 'chevron_left') {
        return ( 
          <>
          <i className="material-icons left">chevron_left</i>
          </ >)
        
      } else if (icon === 'arrow_back') {
        return ( 
          <>
          <i className="material-icons left">arrow_back</i>
          </ >)
        
      } else if (icon === 'lock_open') {
        return ( 
          <>
          <i className="material-icons left">lock_open</i>
          </ >)
        
      } else if (icon === 'library_books') {
        return ( 
          <>
          <i className="material-icons left">library_books</i>
          </ >)
        
      } else if (icon === 'library_add') {
        process.env.DEBUG === "true" && console.log('add')
        return ( 
          <>
          <i className="material-icons left">library_add</i>
          </ >)
        
      }
    }  else {
      process.env.DEBUG === "true" && console.log("Returning non show icons")
      return (
        <> </ >
      )
    }
  }
	render() {
    let logoImage = logo;
    // let path = "M0,96L80,117.3C160,139,320,181,480,186.7C640,192,800,160,960,138.7C1120,117,1280,107,1360,101.3L1440,96L1440,0L1360,0C1280,0,1120,0,960,0C800,0,640,0,480,0C320,0,160,0,80,0L0,0Z";
    // let path = "M0.00,100.0 C149.99,150.00 349.20,-49.98 657.44,97.20 L500.00,150.00 L0.00,150.00 Z";
    if (this.state.width < 700) {
      logoImage = logoMobile;
    } else {
      // console.log("Web browser detected");
    }
		const { isExpanded } = this.state;

		return (
			<Navigation className="">
        
        {(this.props.auth.isAuthenticated && (
				<div className="logo">
          
					<Link to="/overview">
						<div>
							<img
                className="logo-image"
								src={logoImage}
								width="250"
								style={{
									marginTop: '9px',
									marginBottom: '0px',
                  marginLeft: '10px',
                  overflow: "hidden"
								}}
								alt="Logo"
							/>
						</div>
					</Link>
				</div>
        ))}
        {(this.props.auth.isAuthenticated) && (
				<nav className="nav navbar-expand-lg static-top">
					<i className="fa fa-bars" aria-hidden="true" onClick={(e) => this.handleToggle(e)} />
					<ul className={`collapsed ${isExpanded ? 'is-expanded animate__animated animate__slideInRight' : '' + this.state.width < 800 ? ' is-expanded animate__animated animate__slideOutRight': ''}`}>
            {(this.props.auth.user.role === 'drop-super-admin' ||
									this.props.auth.user.role === 'drop-admin') && (
						<NavLink activeClassName="active" to="/overview" >
							<li>{ this._getIcon('dashboard')}Overview</li>
						</NavLink>
            )}
            {/* <NavLink activeClassName="active" to="/overview" >
							<li>
                <Avatar alt="Remy Sharp" src="/broken-image.jpg" className="">
                  B
                </Avatar>
              </li>
						</NavLink> */}
            {(this.props.auth.user.role === 'drop-super-admin' ||
									this.props.auth.user.role === 'drop-admin') && (
						<NavLink activeClassName="active" to="/all_orders" >
							<li>{ this._getIcon('business_center')}Orders</li>
						</NavLink>
            )}
            {(this.props.auth.user.role === 'drop-super-admin' ||
									this.props.auth.user.role === 'drop-admin') && (
						<NavLink activeClassName="active" to="/inventory" >
							<li>{ this._getIcon('library_books')}Inventory</li>
						</NavLink>
            )}
            <NavLink activeClassName="active" to="/create" >
							<li>{ this._getIcon('library_add')}Create Order</li>
						</NavLink> 
            {/* create*/}
            {/* <NavLink activeClassName="active" to="/interactions">
							<li>Interactions</li>
						</NavLink> */}
            <a activeClassName="active" href="https://dropwater.co" >
							<li>{ this._getIcon('home')}About Us</li>
						</a>
            {(this.props.auth.isAuthenticated) && (
						<NavLink to="/logout" onClick={this.onLogoutClick}>
							<li>{ this._getIcon('lock_open')}Logout</li>
						</NavLink>
            // <NavLink to="/logout" onClick={this.onLogoutClick}>
							// <Avatar alt="Remy Sharp" src="/broken-image.jpg" className="">
              //   B
              // </Avatar>
						// </NavLink>
            // <NavLink>
              // <Avatar alt="Remy Sharp" src="/broken-image.jpg" className="">
              //   B
              // </Avatar>
            // </NavLink>
            )}
					</ul>
				</nav>
        )}
			</Navigation>
		);
	}
}

const mapStateToProps = (state) => ({
	errors: state.errors,
	auth: state.auth
});

export default connect(mapStateToProps, {
	logoutUser
})(withRouter(Nav));
