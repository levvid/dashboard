import React, { Component } from "react";
import { ReactNavbar } from "react-responsive-animate-navbar";
import logo from "../../images/drop_logo_white_shadow.png";
import '../../css/date_picker.css'
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';

class NavigationBarNew extends Component {
  onLogoutClick = (e) => {
    e.preventDefault();
    this.props.logoutUser();
    // this.props.history.push("/login");
  };

  render() {
    return (
      <ReactNavbar
        color="rgb(36, 113, 163)"
        logo={logo}
        menu={[
          { name: "Dashboard", to: "/dashboard" },
          { name: "Orders", to: "/all_orders" },
          { name: "Inventory", to: "/inventory" },
          { name: "Logout", to:  'logout'}
          // { name: "CONTACT", to: "/contact" },
        ]}
        // social={[
        //   {
        //     name: "Linkedin",
        //     url: "https://www.linkedin.com/in/nazeh-taha/",
        //     icon: ["fab", "linkedin-in"],
        //   },
        //   {
        //     name: "Facebook",
        //     url: "https://www.facebook.com/nazeh200/",
        //     icon: ["fab", "facebook-f"],
        //   },
        //   {
        //     name: "Instagram",
        //     url: "https://www.instagram.com/nazeh_taha/",
        //     icon: ["fab", "instagram"],
        //   },
        //   {
        //     name: "Logout",
        //     url: "/logout",
        //     icon: ["fab", "twitter"],
        //   },
        // ]}
      />
    );
  }
}


const mapStateToProps = (state) => ({
  errors: state.errors,
  auth: state.auth,
});

export default connect(mapStateToProps, {
  
})(withRouter(NavigationBarNew));
// export default NavigationBar;