import React, { Component } from "react";
import { Nav, Navbar } from "react-bootstrap";
import styled from "styled-components";
import { logoutUser } from "../../actions/authActions";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import logo from "../../images/drop_logo_white_shadow.png";

const Styles = styled.div`
  .navbar {
    background-color: #2471a3;
  }
  a,
  .navbar-nav,
  .navbar-light .nav-link {
    color: #000000;

    // margin-left: 10px;
    width: 100px;
  }

  .nav-link {
    &:hover {
      color: #5499c7;
      transform: scale(1.1);
    }
  }

  .nav-item {
    max-width: 200px;
    // background-color: #85C1E9;

    height: 50px;
    // width: 100px;
    &:hover {
      color: #21618c;
    }
  }
  .nav-link {
    margin-left: 10px;
  }
  .navbar-brand {
    font-size: 1.4em;
    color: #9fffcb;
    &:hover {
      color: white;
    }
  }
`;

class NavigationBar extends Component {
  onLogoutClick = (e) => {
    e.preventDefault();
    this.props.logoutUser();
    // this.props.history.push("/login");
  };
  render() {
    return (
      <Styles>
        <Navbar
          expand="lg"
          style={{
            height: "80px",
          }}
        >
          <Navbar.Brand href="/dashboard">
            <img
              src={logo}
              width="250"
              style={{
                marginTop: "9px",
                marginBottom: "0px",
                marginLeft: "10px",
              }}
              alt="Logo"
            />
          </Navbar.Brand>
            <Nav.Link
              href="/logout"
              style={{
                marginRight: "20px",
                marginTop: "25px",
                letterSpacing: "1.5px",
                // backgroundColor: "#D0D3D4",
                color: "white",
                width: "150px",
                borderRadius: "3px",
                
              }}
              className="right btn waves-effect waves-light hoverable accent-3 "
              onClick={this.onLogoutClick}
            >
              <i className="material-icons right" style={{
                marginTop: "-10px", 
              }}>logout</i>
              Logout
            </Nav.Link>
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-10">
              <Nav.Item>
                <Nav.Link
                  href="/dashboard"
                  className="waves-effect waves-light"
                  style={{ fontSize: "large" }}
                >
                  Dashboard
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link
                  href="/all_orders"
                  className="waves-effect waves-light"
                  style={{ fontSize: "large" }}
                >
                  Orders
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link
                  href="/inventory"
                  className="waves-effect waves-light"
                  style={{ fontSize: "large" }}
                >
                  Inventory
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link
                  href="/all_users"
                  className="waves-effect waves-light"
                  style={{ fontSize: "large" }}
                >
                  Users
                </Nav.Link>
              </Nav.Item>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </Styles>
    );
  }
}

const mapStateToProps = (state) => ({
  errors: state.errors,
  auth: state.auth,
});

export default connect(mapStateToProps, {
  logoutUser,
})(withRouter(NavigationBar));

// export default NavigationBar;
