import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import 'font-awesome/css/font-awesome.min.css';
import { logoutUser } from '../../actions/authActions';
import M from 'materialize-css';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from "react-google-maps";

// import { BurgerIcon } from './'
import { connect } from 'react-redux';
import logo from '../../images/Drop-Logo-Black.png';

const MapWithAMarker = withScriptjs(withGoogleMap(props =>
  <GoogleMap
    defaultZoom={8}
    defaultCenter={{ lat: -34.397, lng: 150.644 }}
  >
    <Marker
      position={{ lat: -34.397, lng: 150.644 }}
    />
  </GoogleMap>
));

class KioskMap extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isExpanded: false
		};
	}

	componentDidMount() {
		
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.errors) {
			this.setState({
				errors: nextProps.errors
			});
		}
	}

	render() {
		return (
      <>
      <MapWithAMarker
        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyB6q3L0yA0JJM7VjJ9OkAHEwk1PHfq0HsM&v=3.exp&libraries=geometry,drawing,places"
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `400px` }} />}
        mapElement={<div style={{ height: `100%` }} />}
        />
      </>
		);
	}
}

const mapStateToProps = (state) => ({
	errors: state.errors,
	auth: state.auth
});

export default connect(mapStateToProps, {
	logoutUser
})(withRouter(KioskMap));
