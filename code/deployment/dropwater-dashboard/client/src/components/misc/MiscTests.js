import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { startAdditiveDispense,  getCounter } from '../../actions/miscTestsActions';
import { Card, Button } from 'react-bootstrap';
// import io from 'socket.io-client';
// import socketIOClient from "socket.io-client";
// import { connectWS } from './../../wsTest';

// const ENDPOINT = "http://127.0.0.1:3000";


// import openSocket from 'socket.io';
// const socket = openSocket('http://localhost:8000');

// import { w3cwebsocket as W3CWebSocket } from "websocket";
// const client = new W3CWebSocket('ws://127.0.0.1:8000');
// const URL = 'wss://i93po5s0lh.execute-api.us-west-1.amazonaws.com/Prod';
// const URL = "wss://i4vssnn9zl.execute-api.us-west-1.amazonaws.com/Additive";
// const WebSocket = require('ws');
// const socket = io();
// const socket = io("http://localhost:3000", {
//   cors: {
//     origin: "http://localhost:5000",
//     methods: ["GET", "POST"]
//   }
// });

class MiscTests extends Component {
	constructor() {
		super();
		this.state = {
			errors: {},
            counter: 0,
            // socket: socketIOClient('http://localhost:8000')
            // ws: undefined
		};

        
	}
    
    
    // ws = undefined;

	componentDidMount() {
        
		const filters = {
            command: "None"
		};
    //    this.props.getCounter();
       console.log("Mounted.")
        setInterval(() => {
            console.log("getting counter.")
            this._checkCounter();
        }, 1000);
       
	}

    _startAdditiveDispense = () => {
        console.log("Start add.");
        const filters = {
            command: "Start"
		};
        
		this.props.startAdditiveDispense(filters);
        // var interval = setInterval(() => {
        //     this.props.startAdditiveDispense(filters);
        //     this.setState({
        //         counter: this.state.counter + 1,
        //         interval: interval
        //     });
        // }, 10000);
        
    }


    _checkCounter = () => {
        fetch("/api/misc_tests/additive_counter/")
      .then(res => res.json())
      .then(
        (result) => {
          console.log("received: " + JSON.stringify(result));
          this.setState({
              counter: result.counter
          })
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
        //   this.setState({
        //     isLoaded: true,
        //     error
        //   });
        }
      )
    }
    _pauseAdditiveDispense = () => {
        console.log("Pause");
        const filters = {
            command: "Pause"
		};
        clearInterval(this.state.interval);
		this.props.startAdditiveDispense(filters);
        
        
    }


	componentWillReceiveProps(nextProps) {
		// console.log('Returned' + JSON.stringify(nextProps.auth.user));
		// console.log('Errors: ' + JSON.stringify(nextProps.errors));
        // console.log("Next props: " + JSON.stringify(nextProps)) 
		if (nextProps.auth.user.isValid) {
			// console.log('User valid. Resetting.');
			nextProps.auth.user.isValid = false;
		} else if (nextProps.errors) {
			this.setState({
				errors: nextProps.errors
			});
		} else if(nextProps.misc) {
            // console.log("Next props: " + JSON.stringify(nextProps.misc));
        }
        console.log("Received prop")
        console.log(JSON.stringify(nextProps));
	}

    componentWillUnmount() {
       clearInterval(this.state.interval);
    }

	render() {
        console.log("Render called");
		
		return (
			(
				<div className="valign-wrapper  animate__animated animate__slideInLeft">
					<div className="row">
                        <Card style={{  borderRadius: "15px", width: "100%" }} className="col s12 l12 m12 center-align">
							{/* <Card.Header> */}
                                {/* .&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;. */}
                            {/* </Card.Header> */}
							<Card.Body className="center-align" style={{ marginBottom: "10rem"}}>
							
								<Card.Title style={{ fontWeight: '400', marginTop: "5rem" }}>Additive Tests</Card.Title>
                                <Card.Text className="col s12 l12 m12" style={{ fontSize: "100px", }}>
                                    {this.state.counter}
                                </Card.Text>
								<Card.Text style={{ fontWeight: '400' }} className="col s6 l6 m6">
                                        <button
                                        style={{
                                            width: "150px",
                                            borderRadius: "3px",
                                            letterSpacing: "1.5px",
                                            marginTop: "1rem",
                                        }}
                                        onClick={() => this._startAdditiveDispense()}
                                        // type="submit"
                                        className="btn btn-large teal white-text"
                                        >
                                            <i className="material-icons right">play_arrow</i>
                                        Start
                                        </button>
								</Card.Text>
                                <Card.Text style={{ fontWeight: '400' }} className="col s6 l6 m6">
                                        <button
                                        style={{
                                            width: "150px",
                                            borderRadius: "3px",
                                            letterSpacing: "1.5px",
                                            marginTop: "1rem",
                                        }}
                                        onClick={() => this._pauseAdditiveDispense()}
                                        // type="submit"
                                        className="btn btn-large  grey white-text"
                                        >
                                            <i className="material-icons right">pause</i>
                                        Pause
                                        </button>
								</Card.Text>

								
							</Card.Body>
						</Card>
					</div>
				</div>
			)
		);
	}
}

MiscTests.propTypes = {
	startAdditiveDispense: PropTypes.func.isRequired,
    getCounter: PropTypes.func.isRequired,
	errors: PropTypes.object.isRequired,
	auth: PropTypes.object.isRequired,
	user: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
	errors: state.errors,
	auth: state.auth,
	user: state.user
});
export default connect(mapStateToProps, {
	startAdditiveDispense, getCounter
})(withRouter(MiscTests));
