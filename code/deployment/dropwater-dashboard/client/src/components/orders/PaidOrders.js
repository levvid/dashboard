import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { loadAllOrders, loadPaidOrders } from "../../actions/orderActions";
import DatePicker from "react-datepicker";
import { Card } from "react-bootstrap";
import "react-datepicker/dist/react-datepicker.css";
import M from "materialize-css";
import Dropdown from "react-dropdown";
import "react-dropdown/style.css";
import ReactExport from "react-export-excel";
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory, { PaginationProvider, PaginationTotalStandalone, PaginationListStandalone } from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import moment from 'moment-timezone';
import { getUTCDateTime, getTimezoneAbbreviation } from '../../js/orders_functions';
import ReactLoading from 'react-loading';


import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import AccordionActions from '@material-ui/core/AccordionActions';


import "../../css/date_picker.css";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;


const TimezoneCities = [
  'America/Los_Angeles',
  'America/New_York',
  'America/Chicago',
  'America/Denver',
  'America/Phoenix',
  'America/Anchorage',
  'Pacific/Honolulu'
]

const sortCaret = (order, column) => {
  // const asc = '&#9650;';
  // const desc = '&#9660;';
  if (!order) return (<span className="sort-caret">&nbsp;&#9660;&#9650;</span>);
  else if (order === 'asc') return (<span className="sort-caret">&nbsp;&#9650;</span>);
  else if (order === 'desc') return (<span className="sort-caret">&nbsp;&#9660;</span>);

  return null;
}

const columns = [{
  dataField: 'converted_date',
  text: 'Date',
  sort: true,
  sortCaret: sortCaret,
  sortFunc: (a, b, order, dataField) => {
    if (order === 'asc') {
      return new Date(a) - new Date(b);
    }
    return new Date(b) - new Date(a); // desc
  },
  // widthPx: 160,
  // style: { font: { sz: "24", bold: true } },
}, {
  dataField: 'converted_time',
  text: 'Time',
  sort: true,
  sortCaret: sortCaret,
  formatter: (cell, row) => {
    return cell;
  },  
  sortFunc: (a, b, order, dataField) => {
    if (order === 'asc') {
      return new Date('1970/01/01 ' + a) - new Date('1970/01/01 ' + b);
    }
    return new Date('1970/01/01 ' + b) - new Date('1970/01/01 ' + a); // desc
  }
}, {
  dataField: 'kiosk_ID',
  text: 'Kiosk',
  // sort: true,
  // sortCaret: sortCaret
}, {
  dataField: 'drink_price',
  text: 'Price',
  sort: true,
  sortCaret: sortCaret,
  sortFunc: (a, b, order, dataField) => {
    if (order === "asc") {
      return parseFloat(a) - parseFloat(b);
    }
    return parseFloat(b) - parseFloat(a); // desc
  }
  // headerAlign: (column, colIndex) => 'right'

}, {
  dataField: 'flavor',
  text: 'Flavor',
  sort: true,
  sortCaret: sortCaret
}, {
  dataField: 'caffeine_level',
  text: 'Caffeine',
  sort: true,
  sortCaret: sortCaret
}, {
  dataField: 'order_type',
  text: 'Type',
  // sort: true,
  // sortCaret: sortCaret
}, {
  dataField: 'refill_amount',
  text: 'Amount(oz)',
  sort: true,
  sortCaret: sortCaret,
  sortFunc: (a, b, order, dataField) => {
    if (order === "asc") {
      return parseFloat(a) - parseFloat(b);
    }
    return parseFloat(b) - parseFloat(a); // desc
  }
}
];

const rowStyle = (row, rowIndex) => {
  const style = {};
  if (rowIndex === 0 || rowIndex % 2 === 0) {
    style.backgroundColor = '#f2f2f2';
  } 

  return style;
};


class Orderlist extends Component {
  constructor() {
    super();
    this.state = {
        all_orders: [],
        startDate: new Date(Date.now() - 2 * 24 * 60 * 60 * 1000), // 30 day date range
        endDate: new Date(),
        timeZone: getTimezoneAbbreviation(),
        company: "Company",
        kiosk: "Kiosk",
        pageCount: "20",
        pageCountInt: 20,
        errors: {},
        companies: [],
	    kiosks: [],
		types: [],
		transactionTypes: [],
		showFilters: false

    };
  }

  componentDidMount() {
    let order_category = this.props.match.params.order_category;
    console.log("Order cat: " + JSON.stringify(order_category));
    const filters = {
        startDate: getUTCDateTime(this.state.startDate, this.state.timeZone, true),
        endDate: getUTCDateTime(this.state.endDate, this.state.timeZone, false),
        // startDate: getUTCDate(this.state.startDate),
        // endDate: getUTCDate(this.state.endDate),
        kiosk: this.state.kiosk,
        timeZone: this.state.timeZone,
        company: this.state.company,

        companies: this.state.companies,
	    types: this.state.types,
		kiosks: this.state.kiosks,
		transactionTypes: this.state.transactionTypes,
    limit: true
    };
    console.log('Start date ' + filters.startDate);
    console.log('Start date ' + filters.endDate);
    this.props.loadPaidOrders(filters);
    // this.state.all_orders = this.props.all_orders.all_orders;
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors,
      });
    }
  }

  setStartDate = (date) => {
    this.setState({
      startDate: date,
    });
  };

  setEndDate = (date) => {
    this.setState({
      endDate: date,
    });
  };

  _renderTags = (obj) => {
		return (
			obj &&
			obj.map((item,i) => <div className="chip chosen-tags">{item}</div>)
		);
	};

  loadOrdersWithFilterDateRange() {
    var start = getUTCDateTime(this.state.startDate, this.state.timeZone, true);
		var end = getUTCDateTime(this.state.endDate, this.state.timeZone, false);


		if (start > end) {
			M.toast({ html: 'Start date cannot be greater than the end date!', classes: 'rounded' });
			return;
		}

    this.setState({
			showFilters: false
		});

    const filters = {
        startDate: start,
        endDate: end,
        kiosk: this.state.kiosk,
        timeZone: this.state.timeZone,
        company: this.state.company,

        companies: this.state.companies,
	      types: this.state.types,
		    kiosks: this.state.kiosks,
		    transactionTypes: this.state.transactionTypes,
        limit: false
    };
    this.props.loadPaidOrders(filters);
  }

  _onSelect = (option) => {
    this.setState({
      kiosk: option.value,
    });
  };

  _getSummaryDateRange = () => {
    return "SummaryReport " + moment(this.state.startDate).format('MM/DD/YY') + '-' + moment(this.state.endDate).format('MM/DD/YY');
  }
  _setAllOrders = (all_orders) => {
    this.setState({
      all_orders: all_orders,
    });
  }
;
  _onSelectTimeZone = (option) => {
    this.setState({
      timeZone: option.value,
    });
  };

  _onSelectCompany = (option) => {
    this.setState({
      company: option.value,
    });
  };

  handleSizePerPage = ({
    page,
    onSizePerPageChange
  }, newSizePerPage) => {
    onSizePerPageChange(newSizePerPage, page);
  }

  _onSelectPageCount = (option, paginationProps, all_orders) => {
    var value = 0;
    paginationProps.page = 1;
    var label = option.value;
    if (option.value === 'All') {
      value = all_orders.length;
    } else {
      value = parseInt(option.value);
    }
    this.setState({
      pageCount: label,
      pageCountInt: value,
    });
    this.handleSizePerPage(paginationProps, value);
  };

  formatAMPM = (date) => {
    date = new Date(date);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? "pm" : "am";
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? "0" + minutes : minutes;
    var strTime = hours + ":" + minutes + " " + ampm;

    return strTime.toUpperCase();
  };

  formatDate = (date) => {
    return new Date(date).toLocaleDateString();
  };

  _onChangeCompany = (event, value, reason) => {
		this.setState({
				companies: value
			},
			() => {
				// this.loadDashboardDateRange();
			}
		);
	};

	_onChangeKiosk = (event, value, reason) => {
		this.setState({
			kiosks: value
		});
	};

	_onChangeBottleType = (event, value, reason) => {
		this.setState({
			types: value
		});
	};

	_onChangeTransactionType = (event, value, reason) => {
		this.setState({
			transactionTypes: value
		});
	};

  _onChangeTimezone = (event, value, reason) => {
		this.setState({
			timeZone: value
		});
	};

	_onExpandMore = () => {
		console.log('test');
		this.setState({
			showFilters: !this.state.showFilters
		});
	};

	_onClearFilters = () => {
		this.setState({
			companies: [],
			kiosks: [],
			transactionTypes: [],
			types: [],
      timeZone: getTimezoneAbbreviation(),
		});
	}

  downloadExcelSheet = (all_orders, kioskSummary) => {
    
    return (
      all_orders && (
        <ExcelFile element={
          <button
            style={{
              width: "160px",
              borderRadius: "3px",
              letterSpacing: "1px",
              marginLeft: "0.5rem",
              color: "white",
              fontSize: "13px",
              backgroundColor: "#00B9DC",
            }}
            className="btn btn-small  btn-flat"
            onClick={() =>
              this.loadOrdersWithFilterDateRange()
            }
            >
            <i className="material-icons left">cloud_download</i>
            Export CSV
          </button>} filename={"DropWater Orders"}>
          <ExcelSheet data={all_orders} name="Orders">
            <ExcelColumn label="Date" value="converted_date" />
            <ExcelColumn label="Time" value="converted_time" />
            <ExcelColumn label="Kiosk" value="kiosk_ID" />
            <ExcelColumn label="Price($)" value="drink_price" />
            <ExcelColumn label="Flavor" value="flavor" />
            <ExcelColumn label="Caffeine" value="caffeine_level" />
            <ExcelColumn label="Type" value="order_type" />
            <ExcelColumn label="Amount(oz)" value="refill_amount" />
          </ExcelSheet>
          <ExcelSheet data={kioskSummary} name={this._getSummaryDateRange()}>
            <ExcelColumn label="Location" value="location" />
            <ExcelColumn label="Paid Refills" value="num_paid_refills" />
            <ExcelColumn label="Refill Revenue($)" value="revenue_refill" />
            <ExcelColumn label="Refill Average($)" value="average_refill" />
            <ExcelColumn label="Compostable Bottles" value="num_compostable" />
            <ExcelColumn label="Compostable Bottle Revenue($)" value="revenue_compostable" />
            <ExcelColumn label="Compostable Average($)" value="average_compostable" />
            <ExcelColumn label="Total Revenue($)" value="total_revenue" />
          </ExcelSheet>
        </ExcelFile>
      )
    );
  };

  render() {
    const kiosks = this.props.all_orders.all_orders.kiosks;
    var timezone_dropdown = this.props.all_orders.all_orders.timezones;


    console.log("Timezones: " + JSON.stringify(timezone_dropdown));
    const page_count = ["10", "20", "50", "100", "All"];
    var all_orders = this.props.all_orders.all_orders.mapped_orders;
    var kioskSummary = this.props.all_orders.all_orders.kioskSummary;
    var companyLabel = this.state.company;


    const companies = this.props.all_orders.all_orders.companies;
		const types = [ 'Drop Bottle', 'Screen Refill' ];
		const transactionTypes = [ 'Free', 'Paid' ];


    console.log("Kiosks are: " + JSON.stringify(kiosks));
    if (companyLabel === 'Company') {
      companyLabel = 'Orders';
    }
    if (!all_orders || !kiosks || !companies || !timezone_dropdown || !types || !transactionTypes) {
      return  <div>
                <ReactLoading type={"spinningBubbles"} color={"#58cbe1"} height={ this.state.width < 800?'55%':'20%' } width={this.state.width < 800?'55%':'20%'} className="center-align dash-progress-spinner" />
              </div>
    }
    
    var loading = this.props.loading;
    if (loading) {
      return <span>Loading</span>;
    } else {
      // process.env.DEBUG === "true" && console.log('All orders list ' + all_orders.mapped_orders[0]);
    }
    
    const customTotal = (from, to, size) => (
      <span className="react-bootstrap-table-pagination-total"><strong>
        Showing { from } to { to } of { size } Orders</strong>
      </span>
    );
    
    const pageButtonRenderer = ({
      page,
      active,
      disable,
      title,
      onPageChange
    }) => {
      const handleClick = (e) => {
        e.preventDefault();
        onPageChange(page);
      };
      const activeStyle = {};
      if (active) {
        activeStyle.backgroundColor = '#2471a3';
        activeStyle.color = 'white';
        activeStyle.borderRadius = '5px';
      } else {
        activeStyle.backgroundColor = 'white';
        activeStyle.color = 'black';
      }
      if (typeof page === 'string') {
        activeStyle.backgroundColor = 'white';
        activeStyle.color = 'black';
      }
      return (
        <li className="page-item">
          <a href="#" onClick={ handleClick } style={ activeStyle }>{ page }</a>
        </li>
      );
    };
    
    const options = {
      sizePerPage: this.state.pageCountInt,
      custom: true,
      // paginationSize: parseInt(this.state.pageCount),
      totalSize: all_orders.length,
      // totalSize: 100,
      pageButtonRenderer,
      paginationTotalRenderer: customTotal,
      
      // sizePerPageOptionRenderer
    };

    return (
      <div className="valign-wrapper  animate__animated animate__slideInLeft">
        <div className="row">
          <Accordion expanded={this.state.showFilters} style={{ borderRadius: "10px" }} className="filter-div">
						<AccordionSummary
							expandIcon={<ExpandMoreIcon />}
							aria-controls="panel1c-content"
							id="panel1c-header"
							onClick={this._onExpandMore}
						>
							<Typography style={{ fontWeight: '400', fontSize: '24px' }} className="col s12 l2 m2 left">
								Paid Orders
							</Typography>
              <div className="col s6 l10 m10 left">
                { this._renderTags([this.state.timeZone]) }
								{ this._renderTags(this.state.companies) }
								{ this._renderTags(this.state.kiosks) }
								{ this._renderTags(this.state.types) }
								{ this._renderTags(this.state.transactionTypes) }
							</div>
							{/* <div className="col s6 l4 m4">
								<Typography >Location</Typography>
							</div>
							<div >
								<Typography >Select trip destination</Typography>
							</div> */}

							{/* <div  className="col s4 l4 m4">
							<Typography>Select filter(s)</Typography>
						</div> */}
						</AccordionSummary>
						<AccordionDetails className="col s12 l12 m12">
						  <div className="col l2 m2"></div>
							<div className="col s6 m1 l1 center-align" style={{ marginLeft: '0px' }}>
								{/* <span style={{ marginRight: "5px", fontSize: "initial", fontWeight: "bold"}}>From:</span> */}
								<DatePicker
									selected={this.state.startDate}
									onChange={this.setStartDate}
									startDate={this.state.startDate}
									endDate={ this.state.endDate }
									// className="blue"
									style={{ width: '100%' }}
									selectsStart
									// peekNextMonth
									showMonthDropdown
									showYearDropdown
									// selectsRange
									todayButton="Today"
									// minDate={this.state.startDate}
									maxDate={new Date()}
									className="date-dropdown" 
								/>
							</div>

							<div className="col s6 l1 m1 center-align">
								{/* <span style={{ marginRight: "5px", fontSize: "initial", fontWeight: "bold"}}>To:</span> */}
								<DatePicker
									selected={this.state.endDate}
									onChange={this.setEndDate}
									selectsEnd
									// peekNextMonth
									showMonthDropdown
									showYearDropdown
									// selectsRange
									todayButton="Today"
									// dropdownMode="select"
									startDate={this.state.startDate}
									endDate={this.state.endDate}
									minDate={this.state.startDate}
									maxDate={new Date()}
									className="date-dropdown" 
								/>
							</div>
              <div className="col s12 l2 m2 center-align center filter-item" style={{ borderWidth: '10px' }}>
								<Autocomplete
									// multiple
									id="tags-standard5"
									// className="col s6 l2 m2 center-align center" 
									options={timezone_dropdown}
									getOptionLabel={(option) => option}
									defaultValue={[]}
									limitTags={5}
									value={this.state.timeZone}
									size="small"
									autoComplete={true}
									fullWidth={true}
									disablePortal={true}
									className="center"
									
									renderInput={(params) => (
										<TextField
											{...params}
											variant="outlined"
											label="Timezone"
                      // className="center"
											// placeholder="Company"
											// style={{ width: '100%', height: '100%' }}
										/>
									)}
									onChange={this._onChangeTimezone}
								/>
							</div>
							{/* <div className="col s6 l0 m0 center-align center"></div> */}
							<div className="col s12 l2 m2 center-align center filter-item" style={{ borderWidth: '10px' }}>
								<Autocomplete
									multiple
									id="tags-standard1"
									// className="col s6 l2 m2 center-align center" 
									options={companies}
									getOptionLabel={(option) => option}
									defaultValue={[]}
									limitTags={5}
									value={this.state.companies}
									size="small"
									autoComplete={true}
									fullWidth={true}
									disablePortal={true}
									// className="center col s6 l2 m2"
									
									renderInput={(params) => (
										<TextField
											{...params}
											variant="outlined"
											label="Companies"
											// placeholder="Company"
											// style={{ width: '100%', height: '100%' }}
										/>
									)}
									onChange={this._onChangeCompany}
								/>
							</div>
							<div className="col s12 l2 m2 center-align filter-item" style={{ borderWidth: '10px' }}>
								<Autocomplete
									multiple
									id="tags-standard2"
									options={kiosks}
									getOptionLabel={(option) => option}
									defaultValue={[]}
									limitTags={5}
									value={this.state.kiosks}
									size="small"
									autoComplete={true}
									fullWidth={true}
									// className="col s6 m1 l1 center-align" 
									renderInput={(params) => (
										<TextField
											{...params}
											variant="outlined"
											label="Kiosks"
											// placeholder="Company"
											style={{ width: '100%', height: '100%' }}
										/>
									)}
									onChange={this._onChangeKiosk}
								/>
							</div>
							<div className="col s12 l2 m2 center-align filter-item" style={{ borderWidth: '10px' }}>
								<Autocomplete
									multiple
									id="tags-standard3"
									options={types}
									getOptionLabel={(option) => option}
									defaultValue={[]}
									limitTags={2}
									value={this.state.types}
									size="small"
									autoComplete={true}
									fullWidth={true}
									renderInput={(params) => (
										<TextField
											{...params}
											variant="outlined"
											label="Type"
											// placeholder="Company"
											style={{ width: '100%', height: '100%' }}
										/>
									)}
									onChange={this._onChangeBottleType}
								/>
							</div>
							{/* <div className="col s12 l2 m2 center-align filter-item" style={{ borderWidth: '10px' }}>
								<Autocomplete
									multiple
									id="tags-standard4"
									options={transactionTypes}
									getOptionLabel={(option) => option}
									defaultValue={[]}
									limitTags={2}
									value={this.state.transactionTypes}
									size="small"
									autoComplete={true}
									fullWidth={true}
									renderInput={(params) => (
										<TextField
											{...params}
											variant="outlined"
											label="Transaction Type"
											// placeholder="Company"
											style={{ width: '100%', height: '100%' }}
										/>
									)}
									onChange={this._onChangeTransactionType}
								/>
							</div> */}

						</AccordionDetails>
						{/* <Divider /> */}
						<AccordionActions>
							<Link
								variant="contained"
								// color="secondary"
								component="button"
  								// variant="body2"
								onClick={() => this._onClearFilters()}
								className="clear-filters"
								// startIcon={<DeleteIcon />}
								// disableRipple={true}
								// id="clear-filter"
							>
								Clear Filters
							</Link>
							
              <div className="" style={{  }}>
                  { this.downloadExcelSheet(all_orders, kioskSummary) }
              </div>
							
              <button
								style={{
									// width: '150px',
									borderRadius: '3px',
									letterSpacing: '1px',
									// marginTop: '0.9rem',
									// marginBottom: '0.3rem',
									fontSize: '13px',
									backgroundColor: "#17A589",
                  color: "white",
								}}
								className="btn btn-small hoverable accent-3 btn-flat"
								onClick={() => this.loadOrdersWithFilterDateRange()}
							>
              <i className="material-icons left">cloud_done</i>
								Done
							</button>
						</AccordionActions>
					</Accordion>
          <Card className="col s12 l12 m12 " style={{ borderRadius: "7px" }}>
          <div className="col s12 l12 m12"><PaginationProvider
              pagination={ paginationFactory(options) }
            >
              {
                ({
                  paginationProps,
                  paginationTableProps
                }) => (
                  <div className="col s12 l12 m12 table-wrapper">
                    <BootstrapTable
                      keyField="key"
                      style={{
                        maxWidth: "100%"
                      }}
                      data={ all_orders }
                      columns={ columns }
                      wrapperClasses="table-responsive" 
                      noDataIndication="No Available Orders"
                      rowStyle={ rowStyle }
                      { ...paginationTableProps }
                    />
                    
                    <PaginationTotalStandalone className="col l4"
                      { ...paginationProps }
                    />
                    <div className="orders-per-page-select">
                      <div className="col l3 right-align orders-per-page"><strong>Orders Per Page</strong></div>
                      <div className="col s2 l1 m2 select-page-count center-align">
                        <Dropdown
                          options={page_count}
                          onChange={(option) =>
                            this._onSelectPageCount(
                              option,
                              paginationProps,
                              all_orders
                            )
                          }
                          value={this.state.pageCount}
                          // className="select-page-count"
                          // placeholder="Select an option"
                        />
                      </div>
                    </div>
                    <PaginationListStandalone className="col l4"
                      { ...paginationProps }
                    />
                    
                  </div>
                )
              }
            </PaginationProvider>
          </div>
        
          </Card>

        </div>
      </div>
    );
  }
}

Orderlist.propTypes = {
  loadAllOrders: PropTypes.func.isRequired,
  loadPaidOrders: PropTypes.func.isRequired,
  all_orders: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  all_orders: state.all_orders,
  errors: state.errors,
});
export default connect(mapStateToProps, {
  loadAllOrders,
  loadPaidOrders,
})(withRouter(Orderlist));