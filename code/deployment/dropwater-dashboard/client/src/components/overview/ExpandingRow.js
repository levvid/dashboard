import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Collapse from "@material-ui/core/Collapse";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';

class ExpandingRow extends React.Component {
    state = { open: false };
  
    render() {
      const { row } = this.props;
      const { open } = this.state;
  
      return (
        <>
          <TableRow key={row.id}>
            <TableCell>
            <IconButton aria-label="delete" color="action" onClick={() => this.setState(({ open }) => ({ open: !open }))}>
				<Icon color="secondary">expand_more</Icon>
			</IconButton>
            </TableCell>
            <TableCell>{row.date_created}</TableCell>
            <TableCell align="right">{row.date_created}</TableCell>
            <TableCell align="right">{row.kiosk_ID}</TableCell>
            <TableCell align="right">{row.video}</TableCell>
          </TableRow>
          <Collapse in={open} component="tr" style={{ display: "block" }}>
            <td>
              <div>Expanded data.</div>
            </td>
          </Collapse>
        </>
      );
    }
  }