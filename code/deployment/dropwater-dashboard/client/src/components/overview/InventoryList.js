import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { loadInventoryList } from '../../actions/inventoryActions';
import { Card } from 'react-bootstrap';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-dropdown/style.css';
import '../../css/date_picker.css';
import { Line } from 'react-chartjs-2';
import ReactLoading from 'react-loading';
import KioskMap from '../maps/KioskLocations';
import { getTimezoneAbbreviation } from '../../js/orders_functions';




import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import AccordionActions from '@material-ui/core/AccordionActions';



import ReactExport from 'react-export-excel';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

class InventoryList extends Component {
	constructor() {
		super();
		this.state = {
			inventory_list: [],
			kiosk: 'Kiosk',
			company: 'Company',
			co: "red",
			errors: {},
			companies: [],
			kiosks: [],
			showFilters: false
		};
	}

	componentDidMount() {
		const filters = {
			kiosk: this.state.kiosk,
			company: this.state.company,

			companies: this.state.companies,
			kiosks: this.state.kiosks,
			timezone: getTimezoneAbbreviation()
		};
		this.props.loadInventoryList(filters);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.errors) {
			this.setState({
				errors: nextProps.errors
			});
		}
	}

	loadInventoryWithFilters() {
		this.setState({
			showFilters: false
		});

		const filters = {
			kiosk: this.state.kiosk,
			company: this.state.company,

			companies: this.state.companies,
			kiosks: this.state.kiosks,
			timezone: getTimezoneAbbreviation(),
		};
		this.props.loadInventoryList(filters);
	}

	_onSelect = (option) => {
		this.setState({
			kiosk: option.value
		});
	};

	_onSelectCompany = (option) => {
		this.setState({
			company: option.value
		});
	};

	formatAMPM = (date) => {
		date = new Date(date);
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? 'pm' : 'am';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0' + minutes : minutes;
		var strTime = hours + ':' + minutes + ' ' + ampm;

		// process.env.DEBUG === "true" && console.log("this is the date in AM PM" + date);
		return strTime.toUpperCase();
	};

	formatDate = (date) => {
		var ret = new Date(date);
		// process.env.DEBUG === "true" && console.log("this is the original date formatting" + date);
		return ret.toLocaleDateString('en-US');
	};

	_onExpandMore = () => {
		console.log('test');
		this.setState({
			showFilters: !this.state.showFilters
		});
	};

	_onClearFilters = () => {
		this.setState({
			companies: [],
			kiosks: [],
		});
	}

	_onChangeCompany = (event, value, reason) => {
		this.setState({
			companies: value,
		});
	};

	_onChangeKiosk = (event, value, reason) => {
		this.setState({
			kiosks: value
		});
	};

	downloadExcelSheet = (inventory_list) => {
		return (
			inventory_list && (
				<ExcelFile
					element={
						<button
							style={{
								width: "160px",
								borderRadius: "3px",
								letterSpacing: "1px",
								fontSize: "13px",
								backgroundColor: "#00B9DC",
								color: "white",
								marginLeft: "0.5rem"
							}}
							className="btn btn-small btn-flat"
							onClick={() =>
							this.loadInventoryWithFilters()
							}
							>
							<i className="material-icons left">cloud_download</i>
							Export CSV
						</button>
					}
					filename={'DropWater Kiosk Inventory'}
				>
					<ExcelSheet data={inventory_list} name="Kiosk Inventory">
						<ExcelColumn label="Kiosk" value="kiosk_ID" />
						<ExcelColumn label="Active Tower" value="towerSelected" />
						<ExcelColumn label="Left Tower" value="leftTowerRemaining" />
						<ExcelColumn label="Right Tower" value="rightTowerRemaining" />
						<ExcelColumn label="Caffeine (ml)" value="caffeine" />
						<ExcelColumn label="Guava (ml)" value="guava" />
						<ExcelColumn label="Cucumber (ml)" value="cucumber" />
						<ExcelColumn label="Lemonade (ml)" value="lemonade" />
						<ExcelColumn label="Data This Month (mb)" value="dataUsed" />
						<ExcelColumn label="Drain Tank" value="drainTankStatus" />
					</ExcelSheet>
				</ExcelFile>
			)
		);
	};


	_renderTags = (obj) => {
		return (
			obj &&
			obj.map((item,i) => <div className="chip chosen-tags">{item}</div>)
		);
	};
	
	renderRows = (inventory_list) => {
		return (
			inventory_list &&
			inventory_list.map(
				({
					key,
					kiosk_ID,
					kioskColor,
					losantId,
					towerSelected,
					leftTowerRemaining,
					leftTowerBgColor,
					rightTowerRemaining,
					rightTowerBgColor,
					caffeine,
					caffeineBgColor,
					guava,
					guavaBgColor,
					cucumber,
					cucumberBgColor,
					lemonade,
					lemonadeBgColor,
					dataUsed,
					drainTankStatus,
					drainTankStatusBgColor,
					lastInteraction,
				}) => (
					<tr key={key}>
						<td className="center-align" style={{ position: 'sticky', zIndex:
										'9999'}}>
							<span className="dot" style={{ backgroundColor: kioskColor }}></span>
							<Link to={`/kiosk/${ losantId }`} 
								onClick={() => {
									console.info("I'm a button.");
									this.props.history.push(`/kiosk/${ losantId }`);
								}}
							className="btn-flat kiosk-link">{ kiosk_ID }
							</Link>
						</td>
						<td className="center-align">{towerSelected}</td>
						<td className="center-align" style={{ color: leftTowerBgColor, fontWeight: leftTowerBgColor === '' ? '': 'bolder' }}>{leftTowerRemaining}</td>
						<td className="center-align" style={{ color: rightTowerBgColor, fontWeight: rightTowerBgColor === '' ? '': 'bolder' }}>{rightTowerRemaining}</td>
						<td className="center-align" style={{ color: caffeineBgColor, fontWeight: caffeineBgColor === '' ? '': 'bolder' }}>{caffeine}</td>
						<td className="center-align" style={{ color: guavaBgColor, fontWeight: guavaBgColor === '' ? '': 'bolder' }}>{guava}</td>
						<td className="center-align" style={{ color: cucumberBgColor, fontWeight: cucumberBgColor === '' ? '': 'bolder' }}>{cucumber}</td>
						<td className="center-align" style={{ color: lemonadeBgColor, fontWeight: lemonadeBgColor === '' ? '': 'bolder' }}>{lemonade}</td>
						<td className="center-align" >{ dataUsed?dataUsed:'0 MB' }</td>
						<td className="center-align" style={{ color: drainTankStatusBgColor }}>{ drainTankStatus }</td>
						<td className="center-align" style={{  }}>{ lastInteraction  }</td>
					</tr>
				)
			)
		);
	};


	renderRowsMini = (inventory_list_mini) => {
		return (
			inventory_list_mini &&
			inventory_list_mini.map(
				({
					key,
					kiosk_ID,
					kioskColor,
					losantId,
					towerSelected,
					towerOneRemaining,
					towerTwoRemaining,
					rightTowerRemaining,
					towerThreeRemaining,
					towerOneBgColor,
					towerTwoBgColor,
					towerThreeBgColor,
					caffeine,
					caffeineBgColor,
					guava,
					guavaBgColor,
					cucumber,
					cucumberBgColor,
					lemonade,
					lemonadeBgColor,
					lastInteraction,
				}) => (
					<tr key={key}>
						<td className="center-align">
							<span className="dot" style={{ backgroundColor: kioskColor, marginRight: '20px' }}></span>
							{kiosk_ID}
						</td>
						<td className="center-align">{towerSelected}</td>
						<td className="center-align" style={{ color: towerOneBgColor, fontWeight: towerOneBgColor === '' ? '': 'bolder'  }}>{towerOneRemaining}</td>
						<td className="center-align" style={{ color: towerTwoBgColor, fontWeight: towerTwoBgColor === '' ? '': 'bolder'  }}>{towerTwoRemaining}</td>
						<td className="center-align" style={{ color: towerThreeBgColor, fontWeight: towerThreeBgColor === '' ? '': 'bolder'  }}>{towerThreeRemaining}</td>
						<td className="center-align" style={{ color: caffeineBgColor, fontWeight: caffeineBgColor === '' ? '': 'bolder'  }}>{caffeine}</td>
						<td className="center-align" style={{ color: guavaBgColor, fontWeight: guavaBgColor === '' ? '': 'bolder'  }}>{guava}</td>
						<td className="center-align" style={{ color: cucumberBgColor, fontWeight: cucumberBgColor === '' ? '': 'bolder'  }}>{cucumber}</td>
						<td className="center-align" style={{ color: lemonadeBgColor, fontWeight: lemonadeBgColor === '' ? '': 'bolder'  }}>{lemonade}</td>
						<td className="center-align" style={{  }}>{ lastInteraction  }</td>
					</tr>
				)
			)
		);
	};


	render() {
		const kiosks = this.props.inventory_list.inventory_list.kiosks;
		var inventory_list = this.props.inventory_list.inventory_list.mapped_inventory;
		var inventory_list_mini = this.props.inventory_list.inventory_list.mapped_inventory_mini;
		console.log("Mapped inventory: " + JSON.stringify(inventory_list));
		var companies = this.props.inventory_list.inventory_list.companies;
		var companyLabel = this.state.company;
		var dailyLineGraph = this.props.inventory_list.inventory_list.dailyLineGraph;
		if (companyLabel === 'Company') {
			companyLabel = 'Inventory';
		}

		if (!inventory_list || !companies || !kiosks ) {
			return  <div>
                		<ReactLoading type={"spinningBubbles"} color={"#58cbe1"} height={ this.state.width < 800?'55%':'20%' } width={this.state.width < 800?'55%':'20%'} className="center-align dash-progress-spinner" />
					</div>
		}
		return (
			<div className="valign-wrapper animate__animated animate__slideInLeft">
				<div className="row">
					<Accordion expanded={this.state.showFilters} style={{ borderRadius: "10px" }} className="filter-div">
						<AccordionSummary
							expandIcon={<ExpandMoreIcon />}
							aria-controls="panel1c-content"
							id="panel1c-header"
							onClick={this._onExpandMore}
						>
							<Typography style={{ fontWeight: '400', fontSize: '24px' }} className="col s12 m2 m2">
								Inventory
							</Typography>
							<div className="col s6 l10 m10 left">
								{ this._renderTags(this.state.companies) }
								{ this._renderTags(this.state.kiosks) }
							</div>
							{/* <div className="col s6 l4 m4">
								<Typography >Location</Typography>
							</div>
							<div >
								<Typography >Select trip destination</Typography>
							</div> */}

							{/* <div  className="col s4 l4 m4">
							<Typography>Select filter(s)</Typography>
						</div> */}
						</AccordionSummary>
						<AccordionDetails className="col s12 l12 m12">

							<div className="col s0 l6 m6 center-align center"></div>
							<div className="col s12 l3 m3 center-align center filter-item" style={{ borderWidth: '10px' }}>
								<Autocomplete
									multiple
									id="tags-standard"
									// className="col s6 l2 m2 center-align center"
									options={companies}
									getOptionLabel={(option) => option}
									defaultValue={[]}
									limitTags={5}
									value={this.state.companies}
									size="small"
									autoComplete={true}
									fullWidth={true}
									disablePortal={true}
									// className="center col s6 l2 m2"

									renderInput={(params) => (
										<TextField
											{...params}
											variant="outlined"
											label="Companies"
											// placeholder="Company"
											// style={{ width: '100%', height: '100%' }}
										/>
									)}
									onChange={this._onChangeCompany}
								/>
							</div>
							<div className="col s12 l3 m3 center-align filter-item" style={{ borderWidth: '10px' }}>
								<Autocomplete
									multiple
									id="tags-standard"
									options={kiosks}
									getOptionLabel={(option) => option}
									defaultValue={[]}
									limitTags={5}
									value={this.state.kiosks}
									size="small"
									autoComplete={true}
									fullWidth={true}
									// className="col s6 m1 l1 center-align"
									renderInput={(params) => (
										<TextField
											{...params}
											variant="outlined"
											label="Kiosks"
											// placeholder="Company"
											style={{ width: '100%', height: '100%' }}
										/>
									)}
									onChange={this._onChangeKiosk}
								/>
							</div>
						</AccordionDetails>
						{/* <Divider /> */}
						<AccordionActions>
							<Link
								variant="contained"
								// color="secondary"
								component="button"
  								// variant="body2"
								onClick={() => this._onClearFilters()}
								className="clear-filters"
								// startIcon={<DeleteIcon />}
								// disableRipple={true}
								// id="clear-filter"
							>
								Clear Filters
							</Link>

							<div className="" style={{  }}>
								{ this.downloadExcelSheet(inventory_list) }
							</div>
							<button
								style={{
									width: '115px',
									borderRadius: '3px',
									letterSpacing: '1px',
									// marginTop: '0.9rem',
									// marginBottom: '0.3rem',
									fontSize: '13px',
									backgroundColor: "#17A589",
                  					color: "white",
								}}
								className="btn btn-small hoverable accent-3 btn-flat"
								onClick={() => this.loadInventoryWithFilters()}
							>
							<i className="material-icons left">cloud_done</i>
								Done
							</button>
						</AccordionActions>
					</Accordion>

					{/* Main inventory */}
					{ inventory_list.length > 0 && (
					<Card style={{ borderRadius: "20px" }} className="col s12 l12 m12
					filter-card">
						<Card.Body >
							<table style={{ }}>
								<thead style={{ overflowX: 'auto'}}>
									<tr>
										<th className="center-align" style={{ position: 'sticky', zIndex:
										'9999'}}>Kiosk</th>
										<th className="center-align">Active Tower</th>
										<th className="center-align">Left Tower</th>
										<th className="center-align">Right Tower</th>
										<th className="center-align">Caffeine (ml)</th>
										<th className="center-align">Guava (ml)</th>
										<th className="center-align">Cucumber (ml)</th>
										<th className="center-align">Lemonade (ml)</th>
										<th className="center-align">Data This Month (mb)</th>
										<th className="center-align">Drain Tank</th>
										<th className="center-align">Last Interaction</th>
									</tr>
								</thead>
								<tbody>{this.renderRows(inventory_list)}</tbody>
							</table>
						</Card.Body>
					</Card>
					)}

					{/* END orf main inventory */}

					{/* Mini Inventory */}
					{ inventory_list_mini.length > 0 && (
						<Card style={{ width: '100%', borderRadius: "20px", paddingBottom: '25px' }} className="col s12 l12 m12 filter-card">
						<Card.Body>
							<table style={{ tableLayout: "fixed" }}>
								<thead>
									<tr>
										<th className="center-align">Kiosk</th>
										<th className="center-align">Active Tower</th>
										<th className="center-align">Tower One</th>
										<th className="center-align">Tower Two</th>
										<th className="center-align">Tower Three</th>
										<th className="center-align">Caffeine (ml)</th>
										<th className="center-align">Guava (ml)</th>
										<th className="center-align">Cucumber (ml)</th>
										<th className="center-align">Lemonade (ml)</th>
										<th className="center-align">Last Interaction</th>
									</tr>
								</thead>
								<tbody>{this.renderRowsMini(inventory_list_mini)}</tbody>
							</table>
						</Card.Body>
					</Card>
					)}					

					{/* End of mini inventory */}

					{/* Kiosk data usage  */}
					{ inventory_list.length > 0 && (
						<Card style={{ width: '100%', borderRadius: "15px" }} className="col s12 l12 m12 center-align">
						<Card.Body>
							<Card.Title style={{ fontWeight: "400" }}>Daily Data Usage</Card.Title>
							<Card.Text style={{ fontWeight: '400' }} className="">
							<Line
								data={ dailyLineGraph }
								height={ this.state.width < 800?170:80 }
								// width={100}
								options={{
									maintainAspectRatio: true,
									// bezierCurve: true,
									scales: {
										yAxes: [{
											ticks: {
												beginAtZero:false
											},
											scaleLabel: {
												display: true,
												labelString: 'Data Used (MB)',
												fontSize: 13 
											}
										}]            
									}
								}}
							/>
							</Card.Text>
						</Card.Body>
					</Card>

					)}
					{/* End of Kiosk data usage  */}
					{/* <Card style={{ width: '100%', borderRadius: "15px" }} className="col s12 l12 m12 center-align">
						<Card.Body>
							<Card.Title style={{ fontWeight: "400" }}>Kiosk Locations</Card.Title>
							<Card.Text style={{ fontWeight: '400' }} className="">
								<KioskMap />
							</Card.Text>
						</Card.Body>
					</Card> */}
				</div>
			</div>
		);
	}
}

InventoryList.propTypes = {
	loadInventoryList: PropTypes.func.isRequired,
	inventory_list: PropTypes.object.isRequired,
	errors: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
	inventory_list: state.inventory_list,
	errors: state.errors
});
export default connect(mapStateToProps, {
	loadInventoryList,
})(withRouter(InventoryList));
