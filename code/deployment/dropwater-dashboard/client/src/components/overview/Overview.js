import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loadDashboardMetrics } from '../../actions/overviewActions';
import { Card } from 'react-bootstrap';
import { Pie, Line, Doughnut } from 'react-chartjs-2';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import M from 'materialize-css';
import '../../css/date_picker.css';
import { getUTCDateTime, getTimezoneAbbreviation } from '../../js/orders_functions';
import ReactLoading from 'react-loading';

import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import AccordionActions from '@material-ui/core/AccordionActions';
import refill from '../../images/refill_square.png';
import paid from '../../images/paid.png';
import plastic_bottle from '../../images/plastic_bottle.png';
import gross from '../../images/gross.png';
import drop_bottle from '../../images/drop_bottle.png';
//import AnimatedNumber from "react-animated-numbers"


class Dashboard extends Component {
	constructor() {
		super();
		this.state = {
			dashboard_metrics: [],
			startDate: new Date(Date.now() - 5 * 24 * 60 * 60 * 1000), // 5 day date range
			endDate: new Date(),
			errors: {},
			timezone: getTimezoneAbbreviation(),
			company: 'Company',
			bottleType: 'Bottle Type',
			kiosk: 'Kiosk',
			transactionType: 'Transaction Type',
			width: window.innerWidth,
			height: window.innerHeight,
			companies: [],
			kiosks: [],
			orderTypes: [],
			transactionTypes: [],
			showFilters: false
		};
		this._updateWindowDimensions = this._updateWindowDimensions.bind(this);
	}

	componentDidMount() {
		window.addEventListener('resize', this._updateWindowDimensions);
		var utc_start = getUTCDateTime(this.state.startDate, this.state.timezone, true);
		
		const dateRange = {
			startDate: utc_start,
			endDate: getUTCDateTime(this.state.endDate, this.state.timezone, false),
			company: this.state.company,
			timezone: this.state.timezone,
			bottleType: this.state.bottleType,
			companies: this.state.companies,
			orderTypes: this.state.orderTypes,
			kiosks: this.state.kiosks,
			transactionTypes: this.state.transactionTypes
		};
		
		setTimeout(() => {
			
			console.log("Loading rest of data....");
			
			
			this.setState({
				startDate: new Date(Date.now() - 15 * 24 * 60 * 60 * 1000)
			});
			this.setState({
				startDate: new Date(Date.now() - 15 * 24 * 60 * 60 * 1000)
			}, () => {
				utc_start = getUTCDateTime(this.state.startDate, this.state.timezone, true);
				dateRange['startDate'] = utc_start;
				this.props.loadDashboardMetrics(dateRange);
								
			});
			
			
		}, 1000);
		this.props.loadDashboardMetrics(dateRange);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this._updateWindowDimensions);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.errors) {
			this.setState({
				errors: nextProps.errors
			});
		}
	}

	setStartDate = (date) => {
		this.setState({
			startDate: date
		});
	};

	_updateWindowDimensions() {
		this.setState({ width: window.innerWidth, height: window.innerHeight });
	}

	setEndDate = (date) => {
		this.setState({
			endDate: date
		});
	};

	_onSelectCompany = (option) => {
		this.setState({
			company: option.value
		});
	};

	_onSelectKiosk = (option) => {
		this.setState({
			kiosk: option.value
		});
	};

	_onSelectTransactionType = (option) => {
		this.setState({
			transactionType: option.value
		});
	};

	_onSelectBottleType = (option) => {
		this.setState({
			bottleType: option.value
		});
	};

	_getPieChart = (chart_data) => {
		return (
			<Doughnut
				data={chart_data}
				height={this.state.width < 800 ? 180 : 80}
				options={{
					maintainAspectRatio: true,
					responsive: true,
                    legend: {
                        position: 'bottom',
                        align: 'center',
                        fullSize: false,
                    },
					tooltips: {
						callbacks: {
							// https://stackoverflow.com/questions/43604597/how-to-customize-the-tooltip-of-a-chart-js-2-0-doughnut-chart
							// https://jsfiddle.net/m7s43hrs/
							//   title: function(tooltipItem, data) {
							// 	return data['labels'][tooltipItem[0]['index']];
							//   },
							//   label: function(tooltipItem, data) {
							// 	return data['datasets'][0]['data'][tooltipItem['index']];
							//   },
							afterLabel: function(tooltipItem, data) {
								var dataset = data['datasets'][0];
								var dataTmp = dataset['data'];

								var total = dataTmp.reduce((a, b) => a + b, 0); // get sum of data array
								var percent = Math.round(dataset['data'][tooltipItem['index']] / total * 100);
								return '(' + percent + '%)';
							}
						},
						backgroundColor: '#000',
						titleFontSize: 15,
						titleFontColor: '#fff',
						bodyFontColor: '#fff',
						bodyFontSize: 15,
						displayColors: true
					}
				}}
			/>
		);
	};

	_getLineGraph = (chart_data, label, sale=false) => {
		return (
			<Line
				data={chart_data}
				height={this.state.width < 800 ? 170 : 80}
				// width={100}
				options={{
					maintainAspectRatio: true,
					legend: {
                        display: false
                    },
					// bezierCurve: true,
					scales: {
						yAxes: [
							{
								ticks: {
									beginAtZero: false,
									callback: function(value, index, values) {
                                        return sale ?'$' + value : value;
                                    }
								},
								scaleLabel: {
									display: false,
									labelString: label,
									fontSize: 13
								}
							}
						]
					}
				}}
			/>
		);
	};

	loadDashboardDateRange() {
		var start = getUTCDateTime(`${this.state.startDate}`, this.state.timezone, true);
		var end = getUTCDateTime(`${this.state.endDate}`, this.state.timezone, false);
		console.log("Start date is: " + this.state.startDate);

		if (start > end) {
			M.toast({ html: 'Start date cannot be greater than the end date!', classes: 'rounded' });
			return;
		}

		this.setState({
			showFilters: false
		});

		const dateRange = {
			startDate: start,
			endDate: end,
			company: this.state.company,
			timezone: this.state.timezone,
			bottleType: this.state.bottleType,

			companies: this.state.companies,
			orderTypes: this.state.orderTypes,
			kiosks: this.state.kiosks,
			transactionTypes: this.state.transactionTypes
		};
		this.props.loadDashboardMetrics(dateRange);
	}

	_onChangeCompany = (event, value, reason) => {
		this.setState({
			companies: value
		});
	};

	_onChangeKiosk = (event, value, reason) => {
		this.setState({
			kiosks: value
		});
	};

	_onChangeBottleType = (event, value, reason) => {
		this.setState({
			orderTypes: value
		});
	};

	_onChangeTransactionType = (event, value, reason) => {
		this.setState({
			transactionTypes: value
		});
	};

	_onExpandMore = () => {
		// // console.log('test');
		this.setState({
			showFilters: !this.state.showFilters
		});
	};

	_onClearFilters = () => {
		this.setState({
			companies: [],
			kiosks: [],
			transactionTypes: [],
			orderTypes: [],
			startDate: new Date(Date.now() - 30 * 24 * 60 * 60 * 1000), // 30 day date range
			endDate: new Date(),
		});
	}

	range = (start, end) => {
		var ans = [];
		for (let i = start; i <= end; i++) {
			ans.push(i);
		}
		return ans;
	};


	_renderTags = (obj) => {
		return (
			obj &&
			obj.map((item,i) => <div className="chip chosen-tags">{item}</div>)
		);
	};

	

	render() {
		const num_orders = this.props.dashboard_metrics.dashboard_metrics.num_orders;
		const num_paid_orders = this.props.dashboard_metrics.dashboard_metrics.num_paid_orders;
		const tot_earnings = this.props.dashboard_metrics.dashboard_metrics.tot_earnings;
		const num_refills = this.props.dashboard_metrics.dashboard_metrics.num_refills;
		const num_compostable = this.props.dashboard_metrics.dashboard_metrics.num_compostable;
		const flavor_graph = this.props.dashboard_metrics.dashboard_metrics.flavor_graph;
		// console.log("Flavor graph: " + JSON.stringify((flavor_graph.labels)));
		const caffeine_flavor_graph = this.props.dashboard_metrics.dashboard_metrics.caffeine_flavor_graph;
		const daily_sales_graph = this.props.dashboard_metrics.dashboard_metrics.daily_sales_graph;
		const daily_transactions_graph = this.props.dashboard_metrics.dashboard_metrics.daily_transactions_graph;
		const companies = this.props.dashboard_metrics.dashboard_metrics.companies;
		const orderTypes = [ 'Drop Bottle', 'Button Refill', 'Screen Refill' ];
		const transactionTypes = [ 'Free', 'Paid' ];
		const kiosks = this.props.dashboard_metrics.dashboard_metrics.kiosks;

		console.log('Kiosks are: ' + JSON.stringify(companies));

		var companyLabel = this.state.company;
		if (companyLabel === 'Company') {
			companyLabel = 'Dashboard';
		}

		if (!companies) {
			return (
				<div>
					<ReactLoading
						type={'spinningBubbles'}
						color={'#58cbe1'}
						height={this.state.width < 800 ? '55%' : '20%'}
						width={this.state.width < 800 ? '55%' : '20%'}
						className="center-align dash-progress-spinner"
					/>
				</div>
			);
		}

//		var ctx = document.getElementById("chart").getContext("2d");
//
//        /*** Gradient ***/
//        var gradient = ctx.createLinearGradient(0, 0, 0, 400);
//            gradient.addColorStop(0, 'rgba(250,174,50,1)');
//            gradient.addColorStop(1, 'rgba(250,174,50,0)');
        /***************/

		console.log('Test ' + Object.keys(flavor_graph.labels).length )
		
		return (
			<div className="valign-wrapper animate__animated animate__slideInLeft"  >
				<div className="row">
					<Accordion expanded={this.state.showFilters} style={{ borderRadius: "10px" }} className="filter-div left">
						<AccordionSummary
							expandIcon={<ExpandMoreIcon />}
							aria-controls="panel1c-content"
							id="panel1c-header"
							onClick={this._onExpandMore}
						>
							<Typography style={{ fontWeight: '400', fontSize: '24px' }} className="col s6 l2 m2 left">
								Overview
							</Typography>
							<div className="col s6 l10 m10 left">
								{ this._renderTags(this.state.companies) }
								{ this._renderTags(this.state.kiosks) }
								{ this._renderTags(this.state.orderTypes) }
								{ this._renderTags(this.state.transactionTypes) }
							</div>
						</AccordionSummary>
						<AccordionDetails className="col s12 l12 m12">
						<div className="col s12 "></div>
							<div className="col s6 m2 l2 center-align" style={{ marginLeft: '0px' }}>
								<DatePicker
									selected={this.state.startDate}
									onChange={this.setStartDate}
									startDate={this.state.startDate}
									endDate={ this.state.endDate }
									// className="blue"
									style={{ width: '100%' }}
									selectsStart
									// peekNextMonth
									showMonthDropdown
									showYearDropdown
									// selectsRange
									todayButton="Today"
									// minDate={this.state.startDate}
									maxDate={new Date()}
									className="date-dropdown" 
								/>
							</div>

							<div className="col s6 l2 m2 center-align">
								{/* <span style={{ marginRight: "5px", fontSize: "initial", fontWeight: "bold"}}>To:</span> */}
								<DatePicker
									selected={this.state.endDate}
									onChange={this.setEndDate}
									selectsEnd
									// peekNextMonth
									showMonthDropdown
									showYearDropdown
									// selectsRange
									todayButton="Today"
									// dropdownMode="select"
									startDate={this.state.startDate}
									endDate={this.state.endDate}
									minDate={this.state.startDate}
									maxDate={new Date()}
									className="date-dropdown" 
								/>
							</div>
							{/* <div className="col s6 l0 m0 center-align center"></div> */}
							<div className="col s12 l2 m2 center-align center filter-item" style={{ borderWidth: '10px' }}>
								<Autocomplete
									multiple
									id="tags-standard"
									// className="col s6 l2 m2 center-align center" 
									options={companies}
									getOptionLabel={(option) => option}
									defaultValue={[]}
									limitTags={5}
									value={this.state.companies}
									size="small"
									autoComplete={true}
									fullWidth={true}
									disablePortal={true}
									// className="center col s6 l2 m2"
									
									renderInput={(params) => (
										<TextField
											{...params}
											variant="outlined"
											label="Companies"
											// placeholder="Company"
											// style={{ width: '100%', height: '100%' }}
										/>
									)}
									onChange={this._onChangeCompany}
								/>
							</div>
							<div className="col s12 l2 m2 center-align filter-item" style={{ borderWidth: '10px' }}>
								<Autocomplete
									multiple
									id="tags-standard"
									options={kiosks}
									getOptionLabel={(option) => option}
									defaultValue={[]}
									limitTags={5}
									value={this.state.kiosks}
									size="small"
									autoComplete={true}
									fullWidth={true}
									// className="col s6 m1 l1 center-align" 
									renderInput={(params) => (
										<TextField
											{...params}
											variant="outlined"
											label="Kiosks"
											// placeholder="Company"
											style={{ width: '100%', height: '100%' }}
										/>
									)}
									onChange={this._onChangeKiosk}
								/>
							</div>
							<div className="col s12 l2 m2 center-align filter-item" style={{ borderWidth: '10px' }}>
								<Autocomplete
									multiple
									id="tags-standard"
									options={orderTypes}
									getOptionLabel={(option) => option}
									defaultValue={[]}
									limitTags={2}
									value={this.state.orderTypes}
									size="small"
									autoComplete={true}
									fullWidth={true}
									renderInput={(params) => (
										<TextField
											{...params}
											variant="outlined"
											label="Order Type"
											// placeholder="Company"
											style={{ width: '100%', height: '100%' }}
										/>
									)}
									onChange={this._onChangeBottleType}
								/>
							</div>
							<div className="col s12 l2 m2 center-align filter-item" style={{ borderWidth: '10px' }}>
								<Autocomplete
									multiple
									id="tags-standard"
									options={transactionTypes}
									getOptionLabel={(option) => option}
									defaultValue={[]}
									limitTags={2}
									value={this.state.transactionTypes}
									size="small"
									autoComplete={true}
									fullWidth={true}
									renderInput={(params) => (
										<TextField
											{...params}
											variant="outlined"
											label="Transaction Type"
											// placeholder="Company"
											style={{ width: '100%', height: '100%' }}
										/>
									)}
									onChange={this._onChangeTransactionType}
								/>
							</div>

						</AccordionDetails>
						{/* <Divider /> */}
						<AccordionActions>
							<Link
								variant="contained"
								// color="secondary"
								component="button"
  								// variant="body2"
								onClick={() => this._onClearFilters()}
								className="clear-filters"
								// startIcon={<DeleteIcon />}
								// disableRipple={true}
								// id="clear-filter"
							>
								Clear Filters
							</Link>
							
							<button
								style={{
									width: '110px',
									borderRadius: '3px',
									letterSpacing: '1px',
									marginLeft: '0.5rem',
									// marginBottom: '0.3rem',
									fontSize: '13px',
									backgroundColor: "#17A589",
                  					color: "white",
									
								}}
								className="btn btn-small hoverable accent-3 btn-flat"
								onClick={() => this.loadDashboardDateRange()}
							>
							<i className="material-icons left">cloud_done</i>
								Done
							</button>
						</AccordionActions>
					</Accordion>

					<div style={{ width: '100%' }}>
					    <Card
                                style={{ width: '19%', marginRight: '1.25%', borderRadius: '10px', textAlign: 'left', }}
                                className="animate__animated animate__zoomIn col l2 center-align summary-card
                                summary-card-three"
                            >
                                <Card.Body style={{ backgroundColor: '#FFFFFF',
                                    float: 'left', margin: '0px', borderRadius: '50%', width: '25%', height: '22%', marginRight:
                                    '5%', paddingRight: '10%', marginTop: '7%', paddingTop: '1%', marginBottom: '5%',
                                    boxShadow: "inset 0px 0px 11px rgba(154, 162, 184, 0.1)",
                                    marginLeft: '3%',
                                      }}>
                                        <Card.Img style={{ marginLeft: '35%', marginBottom: '10px', marginTop: '10px', width:
                                        '100%', marginRight: '0px',
                                         }}
                                        src={refill}/>
                                    </Card.Body>
                                    <Card.Body style={{ marginTop: '7%'}}>
                                        <Card.Title style={{ color: '#636876', fontSize: 'large', fontWeight: '400', lineHeight:
                                        '24px'
                                        }}>refill
                                    servings</Card
                                    .Title>
                                    <Card.Text
                                        style={{ fontWeight: '800', fontSize: 'x-large', color: '#2E2E35', marginTop: '10px',
                                        marginBottom: '10px', lineHeight:
                                    '24px'
                                         }}
                                    >
                                        {num_refills}
                                    </Card.Text>
                                </Card.Body>
                            </Card>


                    <Card
                                style={{ width: '19%', marginRight: '1.25%', borderRadius: '10px', textAlign: 'left', }}
                                className="animate__animated animate__zoomIn col s8 m6 l2 center-align summary-card
                                summary-card-three"
                            >
                                <Card.Body style={{ backgroundColor: '#FFFFFF',
                                    float: 'left', margin: '0px', borderRadius: '50%', width: '25%', height: '22%', marginRight:
                                    '5%', paddingRight: '10%', marginTop: '7%', paddingTop: '1%', marginBottom: '5%',
                                    boxShadow: "inset 0px 0px 11px rgba(154, 162, 184, 0.1)",
                                    marginLeft: '3%',
                                      }}>
                                        <Card.Img style={{ marginLeft: '35%', marginBottom: '10px', marginTop: '10px', width:
                                        '100%', marginRight: '0px',
                                         }}
                                        src={drop_bottle}/>
                                    </Card.Body>
                                    <Card.Body style={{ marginTop: '7%'}}>
                                        <Card.Title style={{ color: '#636876', fontSize: 'large', fontWeight: '400', lineHeight:
                                        '24px'
                                        }}>drop
                                    bottles</Card
                                    .Title>
                                    <Card.Text
                                        style={{ fontWeight: '800', fontSize: 'x-large', color: '#2E2E35', marginTop: '10px',
                                        marginBottom: '10px', lineHeight:
                                    '24px'
                                         }}
                                    >
                                        {num_compostable}
                                    </Card.Text>
                                </Card.Body>
                            </Card>


					<Card
						style={{ width: '19%', marginRight: '1.25%', borderRadius: '10px ', textAlign: 'left', }}
						className="animate__animated animate__zoomIn col s12 m6 l2 center-align summary-card summary-card-four"
					>
						<Card.Body style={{ backgroundColor: '#FFFFFF',
                            float: 'left', margin: '0px', borderRadius: '50%', width: '25%', height: '22%', marginRight:
                            '5%', paddingRight: '10%', marginTop: '7%', paddingTop: '1%', marginBottom: '5%',
                            boxShadow: "inset 0px 0px 11px rgba(154, 162, 184, 0.1)",
                            marginLeft: '3%'
                              }}>
                                <Card.Img style={{ marginLeft: '35%', marginBottom: '10px', marginTop: '10px', width:
                                '100%', marginRight: '30px'
                                 }}
                                src={paid}/>
                            </Card.Body>
                            <Card.Body style={{ marginTop: '7%'}}>
                                <Card.Title style={{ color: '#636876', fontSize: 'large', fontWeight: '400', lineHeight:
                                '24px'
                                }}>paid transactions</Card.Title>
							<Card.Text
								style={{ fontWeight: '800', fontSize: 'x-large', color: '#2E2E35', margin: '10px' }}
							>
								{num_paid_orders}
							</Card.Text>
						</Card.Body>
					</Card>
					<Card
						style={{ width: '19%', marginRight: '1.25%', borderRadius: '10px', textAlign: 'left', }}
						className="animate__animated animate__zoomIn col s12 m6 l2 center-align summary-card summary-card-five"
					>
						<Card.Body style={{ backgroundColor: '#FFFFFF',
                            float: 'left', margin: '0px', borderRadius: '50%', width: '25%', height: '22%', marginRight:
                            '5%', paddingRight: '10%', marginTop: '7%', paddingTop: '1%', marginBottom: '5%',
                            boxShadow: "inset 0px 0px 11px rgba(154, 162, 184, 0.1)",
                            marginLeft: '3%'
                              }}>
                                <Card.Img style={{ marginLeft: '35%', marginBottom: '10px', marginTop: '10px', width:
                                '100%', marginRight: '0px'
                                 }}
                                src={gross}/>
                            </Card.Body>
                            <Card.Body style={{ marginTop: '7%'}}>
                                <Card.Title style={{ color: '#636876', fontSize: 'large', fontWeight: '400', lineHeight:
                                '24px'
                                }}>gross sales</Card.Title>
							<Card.Text
								style={{ fontWeight: '800', fontSize: 'x-large', color: '#2E2E35', marginTop: '10px',
								marginBottom: '10px'
								 }}
							>
								$ {tot_earnings}
							</Card.Text>
						</Card.Body>
					</Card>


					<Card
						style={{ width: '19%', marginRight: '0px', borderRadius: '10px', backgroundColor: '#FFFFFF',
						textAlign: 'left', }}
						className="col s12 m6 l2 center-align summary-card summary-card-one animate__animated animate__zoomIn"
					>
						<Card.Body style={{ backgroundColor: '#FFFFFF',
                            float: 'left', margin: '0px', borderRadius: '50%', width: '25%', height: '22%', marginRight:
                            '5%', paddingRight: '10%', marginTop: '7%', paddingTop: '1%', marginBottom: '5%',
                            boxShadow: "inset 0px 0px 11px rgba(154, 162, 184, 0.1)",
                            marginLeft: '3%'
                              }}>
                                <Card.Img style={{ marginLeft: '35%', marginBottom: '10px', marginTop: '10px', width:
                                '100%', marginRight: '0px'
                                 }}
                                src={plastic_bottle}/>
                            </Card.Body>
                            <Card.Body style={{ marginTop: '7%'}}>
                                <Card.Title style={{ color: '#636876', fontSize: 'large', fontWeight: '400', lineHeight:
                                '24px'
                                }}>plastic bottles
							saved</Card.Title>
							<Card.Text
								style={{ color: '#2E2E35', fontWeight: '800', fontSize: 'x-large', margin: '10px'
								 }}
							>
								{ num_orders }
							</Card.Text>
						</Card.Body>
					</Card>

					</div>


					{ Object.keys(flavor_graph.labels).length > 0 && (
						<Card
							style={{ borderRadius: '15px', width: '49.5%' }}
							className="col s12 l6 m6 center-align mr-2 pie-chart"
						>
							{/* <Card.Header>Transactions by Flavor</Card.Header> */}
							<Card.Body>
								<Card.Title style={{ fontWeight: '800', fontSize: '16px', marginTop: '3%' }}>Servings by
								Flavor</Card
								.Title>
								<Card.Text style={{}} className="">
									{this._getPieChart(flavor_graph)}
								</Card.Text>
							</Card.Body>
						</Card>
					)}
					
					{ caffeine_flavor_graph.labels.length > 0 && (
						<Card
							style={{ borderRadius: '15px', width: '49.5%', marginLeft: '1%' }}
							className="col s12 l6 m6 center-align ml-5 pie-chart"
						>
							{/* <Card.Header>
	Summary
	</Card.Header> */}
							<Card.Body className="center-align">
								<Card.Title style={{ fontWeight: '800', fontSize: '16px', marginTop: '3%' }}>Caffeine Servings</Card.Title>
								<Card.Text style={{ fontWeight: '400' }} className="">
									{this._getPieChart(caffeine_flavor_graph)}
								</Card.Text>
							</Card.Body>
						</Card>
					)}
					
					{ daily_sales_graph.labels.length > 0 && (
						<Card style={{ width: '100%', borderRadius: '15px' }} className="col s12 l12 m12
						center-align">
							<Card.Body>
								<Card.Title style={{ fontWeight: '800', fontSize: 'large', marginTop: '3%' }}>Daily
								Sales (USD)</Card.Title>
								<Card.Text style={{ fontWeight: '400' }} className="">
									{this._getLineGraph(daily_sales_graph, 'Amount ($)', true)}
								</Card.Text>
							</Card.Body>
						</Card>
					)}
					
					<Card style={{ width: '100%', borderRadius: '15px' }} className="col s12 l12 m12 center-align">
						<Card.Body>
							<Card.Title style={{ fontWeight: '800', fontSize: 'large', marginTop: '3%' }}>Daily
							Servings</Card.Title>

							<Card.Text style={{ fontWeight: '400' }} className="">
								{this._getLineGraph(daily_transactions_graph, 'Number of Servings')}
							</Card.Text>
						</Card.Body>
					</Card>
				</div>
			</div>
		);
	}
}
Dashboard.propTypes = {
	loadDashboardMetrics: PropTypes.func.isRequired,
	dashboard_metrics: PropTypes.object.isRequired
};
const mapStateToProps = (state) => ({
	dashboard_metrics: state.dashboard_metrics
});
export default connect(mapStateToProps, { loadDashboardMetrics })(Dashboard);
