import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { loadUserInteractionsList, loadUserInteractionVideo } from '../../actions/userInteractionActions';
import { Card, Modal, Button } from 'react-bootstrap';
import 'react-datepicker/dist/react-datepicker.css';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import '../../css/date_picker.css';
import M from 'materialize-css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';

// const rrweb = require('rrweb');
const rrwebPlayer = require('rrweb-player').default;
// const event_test = require('./test_event');
// let events = [];
// let stopRecording = null;

// this function will send events to the backend and reset the events array
// function save() {
// 	const body = JSON.stringify({ events });
// 	const events_tmp = events;
// 	events = [];

// 	var filters = {
// 		kiosk_ID: 'Dash',
// 		events: events_tmp
// 	};
// 	this.props.loadUserInteractionsList(filters);
// }
// ReactModal.setAppElement(document.body);


function rafAsync() {
	return new Promise((resolve) => {
		requestAnimationFrame(resolve); //faster than set time out
	});
}

function checkElement(selector) {
	if (document.querySelector(selector) === null) {
		return rafAsync().then(() => checkElement(selector));
	} else {
		return Promise.resolve(true);
	}
}


class UserInteractionsList extends Component {
	constructor() {
		super();
		this.videoModal = React.createRef();
		this.state = {
			user_interactions_list: [],
			kiosk: 'All',
			errors: {},
			showModal: false,
			pageCount: '20',
			pageCountInt: 20,
			open: {},
		};
		this.handleOpenModal = this.handleOpenModal.bind(this);
		this.handleCloseModal = this.handleCloseModal.bind(this);
		this.expandRow = this.expandRow.bind(this);
		this.videoLinkFormatter = this.videoLinkFormatter.bind(this);
		this.formatDate = this.formatDate.bind(this);
		this.playVideo = this.playVideo.bind(this);
	}

	componentDidMount() {
		const filters = {
			kiosk: this.state.kiosk
		};

		this.props.loadUserInteractionsList(filters);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.errors) {
			this.setState({
				errors: nextProps.errors
			});
		}
	}

	handleOpenModal() {
		this.setState({ showModal: true });
	}

	handleCloseModal() {
		this.setState({ showModal: false });
	}

	loadUserInteractionsListWithFilters(kiosk) {
		const filters = {
			kiosk: `${kiosk}`
		};
		this.props.loadUserInteractionsList(filters);
	}

	_onSelect = (option) => {
		this.setState({
			kiosk: option.value
		});
	};

	handleSizePerPage = ({ page, onSizePerPageChange }, newSizePerPage) => {
		onSizePerPageChange(newSizePerPage, page);
	};

	_onSelectPageCount = (option, paginationProps, all_orders) => {
		var value = 0;
		paginationProps.page = 1;
		var label = option.value;
		if (option.value === 'All') {
			value = all_orders.length;
		} else {
			value = parseInt(option.value);
		}
		this.setState({
			pageCount: label,
			pageCountInt: value
		});
		this.handleSizePerPage(paginationProps, value);
	};

	formatAMPM = (date) => {
		date = new Date(date);
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? 'pm' : 'am';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0' + minutes : minutes;
		var strTime = hours + ':' + minutes + ' ' + ampm;

		return strTime.toUpperCase();
	};

	formatDate = (date) => {
		var ret = new Date(date);
		// process.env.DEBUG === "true" && console.log("this is the original date formatting" + date);
		return ret.toLocaleDateString('en-US');
	};
	/// Video Code
	// var tmp = this;
	videoLinkFormatter(cell, row) {
		process.env.DEBUG === "true" && console.log('This is the format: ' + JSON.stringify(row.order_ID));
		return (
			<button
				style={{
					all: 'unset',
					width: '150px',
					// borderRadius: '3px',
					letterSpacing: '1px',
					marginTop: '0.5rem',
					fontSize: '13px',
					// backgroundColor: '#2471a3'
					background: 'none!important',
					border: 'none',
					padding: '0!important',
					/*optional*/
					// fontFamily: "arial, sans-serif",
					/*input has OS specific font-family*/
					color: '#069',
					textDecoration: 'underline',
					cursor: 'pointer'
				}}
				className="link"
				onClick={() => this.playVideo(row.events)}
			>
				Video
			</button>
		);
	}

	expandRow = (order_ID) => {
		// open[order_ID] = !open[order_ID];
		process.env.DEBUG === "true" && console.log('This is open ' + JSON.stringify(this.state));
		if (order_ID in this.state.open) {
			const flipOpenState = !this.state.open[order_ID];
			const newOpen = { ...this.state.open, order_ID: flipOpenState };
			this.setState({
				open: newOpen
			});
		} else {
			const flipOpenState = !this.state.open[order_ID];
			const newOpen = { ...this.state.open, order_ID: flipOpenState };
			this.setState({
				open: newOpen
			});
			process.env.DEBUG === "true" && console.log("Setting open to " + JSON.stringify(this.state.open));
		}
		process.env.DEBUG === "true" && console.log('This is open 2 ' + JSON.stringify(this.state));
		process.env.DEBUG === "true" && console.log("Expand row called");
	}


	fetchVideo = (s3URL_link) => {
		fetch(s3URL_link)
		.then(res => res.json())
		.then((out) => {
		process.env.DEBUG === "true" && console.log('Checkout this JSON! ', out);
		return out;
		})
		.catch(err => { throw err });
	}

	getJSON = (url, callback) => {
		var xhr = new XMLHttpRequest();
		process.env.DEBUG === "true" && console.log("This is thje URL" + JSON.stringify(url));
		xhr.open('GET', url, true);
		xhr.responseType = 'json';
		xhr.onload = () => {
		  var status = xhr.status;
		  if (status === 200) {
			callback(null, xhr.response);
		  } else {
			callback(status, xhr.response);
		  }
		};
		xhr.send();
	};
	playVideo = (play_events) => {
		process.env.DEBUG === "true" && console.log('Playing video ' + JSON.stringify(play_events));
		this.handleOpenModal();
		// var video = this.fetchVideo(play_events.events);
		// process.env.DEBUG === "true" && console.log("Events: " + JSON.stringify(video));
		checkElement('#video-modal') //use whichever selector you want
			.then((element) => {
				try {
					// const replayer = new rrweb.Replayer(play_events.events, {
					// 	root: document.getElementById('video-modal'),
					// 	showDebug: true
					// 	// root: document.getElementsByClassName('container')[0]
					// });
					// // replayer.enableInteract();
					// replayer.startLive();
					// var playPromise = replayer.play();
					// alternate with ctrls
					this.getJSON(play_events.events,
						function(err, data) {
						if (err !== null) {
							process.env.DEBUG === "true" && console.log('Something went wrong: ' + err);
						} else {
							// alert('Your query count: ' + data.query.count);
							process.env.DEBUG === "true" && console.log("Data" + JSON.stringify(data));
							new rrwebPlayer({
								target: document.getElementById('video-modal'), // customizable root element
								props: {
									events: data,
									autoPlay: true
									// width: 100,
									// height: 100,
								}
							});
						}
					});
					
					// player.play();

					// if (playPromise !== undefined) {
					// 	playPromise
					// 		.then((_) => {
					// 			// Automatic playback started!
					// 			// Show playing UI.
					// 			process.env.DEBUG === "true" && console.log('Done playing UIn vid');
					// 		})
					// 		.catch((error) => {
					// 			// Auto-play was prevented
					// 			// Show paused UI.
					// 			process.env.DEBUG === "true" && console.log('Error playing UIn vid');
					// 		});
					// }
				} catch (err) {
					M.toast({
						html: 'Video playback error',
						classes: 'rounded'
					});
					process.env.DEBUG === "true" && console.log('Error is: ' + JSON.stringify(err));
					process.env.DEBUG === "true" && console.log(err);
					this.handleCloseModal();
				}
			});
	}
	renderRows = (user_interactions_list) => {
		// const { open } = this.state;
		
		return (
			user_interactions_list &&
			user_interactions_list.map(({ key, kiosk_ID, date_created, order_ID, events }) => (
				<>
				<tr key={key}>
					<td className="center-align">{order_ID}</td>
					{/* <td className="center-align"> 
						<IconButton aria-label="delete" color="action" onClick={() => this.expandRow(order_ID)}>
							<Icon color="secondary">expand_more</Icon>
						</IconButton>
					</td> */}
					<td className="center-align">{this.formatDate(date_created)}</td>
					<td className="center-align">{this.formatAMPM(date_created)}</td>
					<td className="center-align">{kiosk_ID}</td>
					{/* <td className="center-align">{order_ID}</td> */}
					<td className="center-align">
						<button
							style={{
								all: 'unset',
								width: '150px',
								// borderRadius: '3px',
								letterSpacing: '1px',
								marginTop: '0.5rem',
								fontSize: '13px',
								// backgroundColor: '#2471a3'
								background: 'none!important',
								border: 'none',
								padding: '0!important',
								/*optional*/
								// fontFamily: "arial, sans-serif",
								/*input has OS specific font-family*/
								color: '#069',
								textDecoration: 'underline',
								cursor: 'pointer'
							}}
							className="link"
							onClick={() => this.playVideo({ events })}
						>
							Video
						</button>
					</td>
				</tr>
				{/* <Collapse in={this.state.open[order_ID]} component="tr" style={{ display: "block" }}>
					<td>
					<div>Expanded data.</div>
					</td>
				</Collapse> */}
				</ >
			))
		);
	};
	render() {
		const kiosks = [ 'All', 'P3-1', 'P3-2' ];
		var user_interactions_list = this.props.user_interactions_list.user_interactions_list;
		
		
		// const columns = [
		// 	{
		// 		dataField: 'kiosk_ID',
		// 		text: 'Kiosk',
		// 		sort: true,
		// 		sortCaret: sortCaret
		// 	},
		// 	{
		// 		dataField: 'date_created',
		// 		text: 'Date',
		// 		sort: true,
		// 		sortCaret: sortCaret,
		// 		sortFunc: (a, b, order, dataField) => {
		// 			if (order === 'asc') {
		// 				return new Date('1970/01/01 ' + a) - new Date('1970/01/01 ' + b);
		// 			}
		// 			return new Date('1970/01/01 ' + b) - new Date('1970/01/01 ' + a); // desc
		// 		}
		// 	},
		// 	{
		// 		dataField: 'order_ID',
		// 		text: 'Order ID'
		// 		// sort: true,
		// 		// sortCaret: sortCaret
		// 	},
		// 	{
		// 		dataField: 'events',
		// 		text: 'Video',
		// 		sort: false,
		// 		formatter: this.videoLinkFormatter
		// 		// sortCaret: sortCaret,
		// 	}
		// ];
		// const page_count = [ '10', '20', '50', '100', 'All' ];

		if (!user_interactions_list || !kiosks) {
			return (
				<div className="progress progress-blue-only">
					<div className="indeterminate" />
				</div>
			);
		}
		user_interactions_list = user_interactions_list.mapped_user_interactions;
		
		user_interactions_list = Object.values(user_interactions_list);
		return (
			<div className="valign-wrapper">
				<div className="row">
					<Card style={{ width: '100%' }} className="col s12 l12 m12">
						<Card.Body>
							<Card.Title style={{ fontWeight: '400' }}>
								<div
									className="col s4 l4 m2 left ml-0"
									style={{ marginTop: '0.5rem', fontWeight: '400' }}
								>
									User Interactions
								</div>
								<div className="col s2 l4 m4" />
								<div className="col s2 l2 m2">
									<Dropdown
										options={kiosks}
										onChange={this._onSelect}
										value={this.state.kiosk}
										// placeholder="Select an option"
									/>
								</div>

								<div className="col s2 l2 m2">
									<button
										style={{
											width: '150px',
											borderRadius: '3px',
											letterSpacing: '1px',
											marginTop: '0.5rem',
											fontSize: '13px'
											// backgroundColor: '#2471a3'
										}}
										className="btn btn-small waves-effect hoverable accent-3"
										onClick={() => this.loadUserInteractionsListWithFilters(this.state.kiosk)}
									>
										Reload Data
									</button>
								</div>
								{/* </div> */}
							</Card.Title>
						</Card.Body>
					</Card>

					{/* <div>
						<button onClick={this.handleOpenModal}>Trigger Modal</button>
						<ReactModal
							isOpen={this.state.showModal}
							contentLabel="onRequestClose Example"
							onRequestClose={this.handleCloseModal}
						>
							<p>Modal text!</p>
							<button onClick={this.handleCloseModal}>Close Modal</button>
						</ReactModal>
					</div> */}
					<Modal
						size="sm"
						aria-labelledby="contained-modal-title-vcenter"
						centered
						show={this.state.showModal}
						onHide={this.handleCloseModal}
					>
						{/* <Modal.Header closeButton>
							<Modal.Title>User Interaction Video</Modal.Title>
						</Modal.Header> */}
						<Modal.Body id="video-modal" />
						<Modal.Footer>
							<Button variant="secondary" onClick={this.handleCloseModal}>
								Close
							</Button>
							{/* <Button variant="primary" onClick={this.handleCloseModal}>
                            Save Changes
                        </Button> */}
						</Modal.Footer>
					</Modal>
					<div className="col s12 l12 m12">
						<table>
							<thead>
								<tr>
									<th className="center-align">Order ID</th>
									<th className="center-align">Date</th>
									<th className="center-align">Time</th>
									<th className="center-align">Kiosk</th>
									<th className="center-align">Video</th>
								</tr>
							</thead>
							<tbody>{this.renderRows(user_interactions_list)}</tbody>
						</table>
					</div>

					{/* <div className="col s12 l12 m12">
						<PaginationProvider pagination={paginationFactory(options)}>
							{({ paginationProps, paginationTableProps }) => (
								<div className="col s12 l12 m12">
									<BootstrapTable
										keyField="key"
										data={user_interactions_list}
										columns={columns}
										wrapperClasses="table-responsive"
										noDataIndication="No Available Interactions"
										rowStyle={rowStyle}
										{...paginationTableProps}
									/>

									<PaginationTotalStandalone className="col l4" {...paginationProps} />
									<div className="orders-per-page-select">
										<div className="col l3 right-align orders-per-page">
											<strong>Interactions Per Page</strong>
										</div>
										<div className="col s2 l1 m2 select-page-count center-align">
											<Dropdown
												options={page_count}
												onChange={(option) =>
													this._onSelectPageCount(
														option,
														paginationProps,
														user_interactions_list
													)}
												value={this.state.pageCount}
												// className="select-page-count"
												// placeholder="Select an option"
											/>
										</div>
									</div>
									<PaginationListStandalone className="col l4" {...paginationProps} />
								</div>
							)}
						</PaginationProvider>
					</div> */}
				</div>
			</div>
		);
	}
}

UserInteractionsList.propTypes = {
	loadUserInteractionsList: PropTypes.func.isRequired,
	loadUserInteractionVideo: PropTypes.func.isRequired,
	user_interactions_list: PropTypes.object.isRequired,
	errors: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
	user_interactions_list: state.user_interactions_list,
	errors: state.errors,
	open: state.open
});
export default connect(mapStateToProps, {
	loadUserInteractionsList, loadUserInteractionVideo
})(withRouter(UserInteractionsList));
