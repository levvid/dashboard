import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { loadAllUsers, emailQRCode } from "../../actions/userTableActions";
import { logoutUser } from "../../actions/authActions";
import M from 'materialize-css';

class Userlist extends Component {
  constructor() {
    super();
    this.state = {
      all_users: [],
      errors: {},
    };
  }

  componentDidMount() {

    console.log("Auth is: " + JSON.stringify(this.props.auth))
    this.props.loadAllUsers();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors,
      });
    }
    process.env.DEBUG === "true" && console.log("These are the next props Userlist" + JSON.stringify(nextProps.history));
  }

  onLogoutClick = (e) => {
    e.preventDefault();
    this.props.logoutUser();
  };

  onEmailQRCode(uuid) {
    const userUuid = {
      uuid: `${uuid}`,
    };
    this.props.emailQRCode(userUuid);
    M.toast({html: 'Email with QRCode sent!', classes: 'rounded'});
  }
  renderRows = () => {
    const all_users = Object.values(this.props.all_users.all_users);
    return (
      all_users &&
      all_users.map(({ key, name, uuid, email }) => (
        <tr key={uuid}>
          <td>{name}</td>
          <td>{email}</td>
          <td>{uuid}</td>
          <td>
            <button
              style={{
                width: "150px",
                borderRadius: "3px",
                letterSpacing: "1px",
                marginTop: "1rem",
                fontSize: "10px",
              }}
              className="btn btn-medium waves-effect waves-light hoverable blue accent-3"
              onClick={() => this.onEmailQRCode(uuid)}
            >
              Email
            </button>
          </td>
        </tr>
      ))
    );
  };
  render() {
    return (
      <div className="">
        <div className="row">
        </div>
        <div style={{ height: "75vh" }} className="valign-wrapper">
          <div className="row">
            <div className="col s12" style={{ width: "100%" }}>
              <div>
                <table>
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Email</th>
                      <th>UUID</th>
                      <th>Email QRCode</th>
                    </tr>
                  </thead>
                  <tbody>{this.renderRows()}</tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Userlist.propTypes = {
  loadAllUsers: PropTypes.func.isRequired,
  logoutUser: PropTypes.func.isRequired,
  all_users: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  emailQRCode: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  all_users: state.all_users,
  errors: state.errors,
  auth: state.auth,
});
export default connect(mapStateToProps, {
  loadAllUsers,
  emailQRCode,
  logoutUser,
})(withRouter(Userlist));
