import React, { Component } from "react";
import PropTypes from "prop-types";
import { Card } from "react-bootstrap";
import { connect } from "react-redux";
import { loadUserProfile, saveUserProfile } from "../../actions/UserProfileActions";
import { USER_LOADING } from "../../actions/types";
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Tab from '@material-ui/core/Tab';
import TabContext from '@material-ui/lab/TabContext';
import TabList from '@material-ui/lab/TabList';
import TabPanel from '@material-ui/lab/TabPanel';
import TextField from '@material-ui/core/TextField';
import ReactLoading from 'react-loading';


var QRCode = require('qrcode');

class UserProfile extends Component {
  constructor(props) {
		super(props);
		this.state = {
			uuid: '',
            password: '',
            passwordConfirm: '',

            errors: {},
            currentTab: '1',
            editGeneralDisabled: true,
            editCompanyDisabled: true,
            company: {},
            user: {},
		};
	}

  componentDidMount() {
        console.log("In component didmount")
		var uuid = this.props.match.params.uuid;
		this.setState({
			uuid: uuid,
            user: this.props.auth.user,
            company: this.props.user_profile.user_profile.company,
		});

		const filters = {
			uuid: uuid,
            type: 'Load' 
		};


        console.log("Calling load user.");
		this.props.loadUserProfile(filters);
	}

    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors,
            });
        }

        if (nextProps.user_profile && nextProps.user_profile.user_profile) {
            console.log("Setting state to user && company")
            this.setState({
                user: nextProps.user_profile.user_profile.user,
                company: nextProps.user_profile.user_profile.company,
            })
        }

        console.log("Next props:" + JSON.stringify(nextProps.user_profile.user_profile.user));

    }

    _onChangePassword = (event) => {
        this.setState({
            password: event.target.value
        });
    }


    _onChangePasswordConfirm = (event) => {
        this.setState({
            passwordConfirm: event.target.value
        });
    }

    _updateUserPassword = () => {

        if(this.state.password.trim() === '' || this.state.password !== this.state.passwordConfirm) {
            console.log("Passwords don't match");
        }

        let update = {
            password: this.state.password,
            type: 'Password',
            user: this.state.user,
        }

        this.setState({
            password: ''
        })

        this.props.loadUserProfile(update);
    }
    


    _handleChange = (event, newValue) => {
        console.log(newValue);

        this.setState({
            currentTab: newValue
        })
    }

    _onChangeName = (event) => {
        this.setState({
            user: { ...this.state.user, name: event.target.value }
        });
    }
    
    _onChangeEmail = (event) => {
        this.setState({
            user: { ...this.state.user, email: event.target.value }
        });
    }

    _onChangePhoneNumber = (event) => {
        this.setState({
            user: { ...this.state.user, phone_number: event.target.value }
        });
    }

    _onChangeCountry = (event) => {
        this.setState({
            user: { ...this.state.user, country: event.target.value }
        });
    }

    _onChangeState = (event) => {
        this.setState({
            user: { ...this.state.user, state: event.target.value }
        });
    }

    _onChangeCity = (event) => {
        this.setState({
            user: { ...this.state.user, city: event.target.value }
        });
    }

    _editGeneralUserInfo = () => {
        this.setState({
            editGeneralDisabled: false
        });
    }


    _saveGeneralUserInfo = (e) => {
        e.preventDefault();
        this.setState({
            editGeneralDisabled: true
        });

        let user = {
            user: this.state.user,
            type: 'User'
        }

        this.props.loadUserProfile(user);
    }


    // Company

    _onChangeCompanyName = (event) => {
        this.setState({
            company: { ...this.state.company, name: event.target.value }
        });
    }
    
    _onChangeCompanyEmail = (event) => {
        this.setState({
            company: { ...this.state.company, email: event.target.value }
        });
    }

    _onChangeCompanyPhoneNumber = (event) => {
        this.setState({
            company: { ...this.state.company, phone_number: event.target.value }
        });
    }

    _onChangeCompanyAddress = (event) => {
        this.setState({
            company: { ...this.state.company, address: event.target.value }
        });
    }

    _onChangeCompanyState = (event) => {
        this.setState({
            company: { ...this.state.company, state: event.target.value }
        });
    }

    _onChangeCompanyCity = (event) => {
        this.setState({
            company: { ...this.state.company, city: event.target.value }
        });
    }

    _editCompanyInfo = () => {
        this.setState({
            editCompanyDisabled: false
        });
    }


    _saveCompanyInfo = (e) => {
        e.preventDefault();
        this.setState({
            editCompanyDisabled: true
        });

        let update = {
            company: this.state.company,
            user: this.state.user,
            type: 'Company'
        }

        this.props.loadUserProfile(update);
    }


  render() {
    let company = this.props.user_profile.user_profile.company;
    let user = this.props.user_profile.user_profile.user;
    console.log("Props: " + JSON.stringify(this.props.user_profile.user_profile));
    console.log("Company: " + JSON.stringify(company) + ' ' + JSON.stringify(this.state.company));
    console.log("User: " + JSON.stringify(this.state.user) + ' ' + JSON.stringify(user));

    if ((!company || !user) && (!this.state.company || !this.state.user)) {
			return (
				<div>
					<ReactLoading
						type={'spinningBubbles'}
						color={'#58cbe1'}
						height={this.state.width < 800 ? '55%' : '20%'}
						width={this.state.width < 800 ? '55%' : '20%'}
						className="center-align dash-progress-spinner"
					/>
				</div>
			);
		}
    return (
      <div style={{ marginTop: "1%", width: "100%" }} className="valign-wrapper">
        <div className="row" style={{ marginTop: "1%", width: "100%" }}>
          <div className="">
            {/* <h4>
              <b>Hey there,</b> {user.name.split(" ")[0]} {user.name.split(" ")[1]}
            </h4>
            <p><b>UUID: </b>{user.uuid}</p>
             */}
             {/* <Paper square className="animate__animated animate__zoomIn"> */}
                <TabContext value={this.state.currentTab}>
                    <AppBar position="static" style={{ backgroundColor: "transparent", color: "black" }}>
                    <TabList onChange={ this._handleChange } aria-label="" variant="">
                        <Tab label="General" value="1"/>
                        <Tab label="Company" value="2" />
                        <Tab label="Security" value="3" />
                    </TabList>
                    </AppBar>
                    <TabPanel value="1">
                        <Card
						style={{ padding: "2%" }}
						className="animate__animated animate__zoomIn"
					>
						<Card.Body>
							{/* <Card.Title style={{  }}>User Profile</Card.Title> */}
							<Card.Text style={{  }}>
								<p className="col l4 m4 s6"><TextField size="small" variant="outlined" disabled={ this.state.editGeneralDisabled } fullWidth={true} required={ !this.state.editGeneralDisabled } id="name" label="Name" defaultValue="" value={ this.state.user ? this.state.user.name : user.name } onChange={this._onChangeName }/></p>
								<p className="col l4 m4 s6"><TextField size="small" variant="outlined" disabled={ this.state.editGeneralDisabled } fullWidth={true} required={ !this.state.editGeneralDisabled } id="user-email" label="Email" defaultValue="" value={ this.state.user ? this.state.user.email : user.email } onChange={this._onChangeEmail } placeholder="Email" /></p>
								<p className="col l4 m4 s6"><TextField size="small" variant="outlined" disabled={ this.state.editGeneralDisabled } fullWidth={true} id="phone-number" label="Phone Number" defaultValue="" value={ this.state.user ? this.state.user.phone_number : user.phone_number } onChange={this._onChangePhoneNumber }/></p>
								<p className="col l4 m4 s6"><TextField size="small" variant="outlined" disabled={ this.state.editGeneralDisabled } fullWidth={true} id="country" label="Country" defaultValue="" value={ this.state.user ? this.state.user.country : user.country } onChange={this._onChangeCountry }/></p>
								<p className="col l4 m4 s6"><TextField size="small" variant="outlined" disabled={ this.state.editGeneralDisabled } fullWidth={true} id="state" label="State" defaultValue="" value={ this.state.user ? this.state.user.state : user.state } onChange={this._onChangeState }/></p>
								<p className="col l4 m4 s6"><TextField size="small" variant="outlined" disabled={ this.state.editGeneralDisabled } fullWidth={true} id="city" label="City" defaultValue="" value={ this.state.user ? this.state.user.city : user.city } onChange={this._onChangeCity}/></p>
							</Card.Text>
                            { this.state.editGeneralDisabled && (
                                <button
                                    style={{
                                        width: "110px",
                                        borderRadius: "3px",
                                        letterSpacing: "1.5px",
                                        marginTop: "1rem",
                                        left: "87%"
                                    }}
                                    type="submit"
                                    className="btn btn-medium waves-effect waves-light hoverable white-text"
                                    onClick={this._editGeneralUserInfo} 
                                    >
                                        <i className="material-icons right">edit</i>
                                    Edit
                                </button>
                            )}
                            { !this.state.editGeneralDisabled && (
                                <button
                                    style={{
                                        width: "110px",
                                        borderRadius: "3px",
                                        letterSpacing: "1.5px",
                                        marginTop: "1rem",
                                        left: "87%"
                                    }}
                                    type="submit"
                                    className="btn btn-medium waves-effect waves-light hoverable white-text"
                                    onClick={this._saveGeneralUserInfo} 
                                    >
                                        <i className="material-icons right">save</i>
                                    Save
                                </button>
                            )}
						</Card.Body>
					</Card>
                    </TabPanel>

                    {/* Company  */}
                    <TabPanel value="2">
                        <Card
                            style={{ padding: "2%" }}
                            className="animate__animated animate__zoomIn"
                        >
                            <Card.Body>
                                {/* <Card.Title style={{  }}>User Profile</Card.Title> */}
                                <Card.Text
                                    style={{  }}
                                    
                                >
                                    <p className="col l8 m8 s6"><TextField size="small" variant="outlined" disabled={ this.state.editCompanyDisabled } fullWidth={true} required={ !this.state.editCompanyDisabled } id="company-name" label="Name" defaultValue="" value={ this.state.company ? this.state.company.name : company.name } onChange={this._onChangeCompanyName }/></p>
                                    <p className="col l4 m4 s6"><TextField size="small" variant="outlined" disabled={ this.state.editCompanyDisabled } fullWidth={true} required={ !this.state.editCompanyDisabled } id="company-alias" label="Alias" defaultValue="" value={ this.state.company ? this.state.company.alias : company.alias } onChange={this._onChangeCompanyAlias } placeholder="Alias" /></p>                                    
                                    <p className="col l4 m4 s6"><TextField size="small" variant="outlined" disabled={ this.state.editCompanyDisabled } fullWidth={true} required={ !this.state.editCompanyDisabled } id="company-poc" label="Point of Contact" defaultValue="" value={ this.state.company ? this.state.company.poc : company.poc } onChange={this._onChangeCompanyPOC } placeholder="POC" /></p>
                                    <p className="col l4 m4 s6"><TextField size="small" variant="outlined" disabled={ this.state.editCompanyDisabled } fullWidth={true} required={ !this.state.editCompanyDisabled } id="company-email" label="Email" defaultValue="" value={ this.state.company ? this.state.company.email : company.email } onChange={this._onChangeCompanyEmail } placeholder="Email" /></p>
                                    <p className="col l4 m4 s6"><TextField size="small" variant="outlined" disabled={ this.state.editCompanyDisabled } fullWidth={true} id="company-phone-number" label="Phone Number" value={ this.state.company ? this.state.company.phone_number : company.phone_number } onChange={this._onChangeCompanyPhoneNumber }/></p>
                                    <p className="col l4 m4 s6"><TextField size="small" variant="outlined" disabled={ this.state.editCompanyDisabled } fullWidth={true} id="company-address" label="Address" defaultValue="" value={ this.state.company ? this.state.company.address : company.address } onChange={this._onChangeCompanyAddress }/></p>
                                    <p className="col l4 m4 s6"><TextField size="small" variant="outlined" disabled={ this.state.editCompanyDisabled } fullWidth={true} id="company-city" label="City" defaultValue="" value={ this.state.company ? this.state.company.city : company.city } onChange={this._onChangeCompanyCity}/></p>
                                    <p className="col l4 m4 s6"><TextField size="small" variant="outlined" disabled={ this.state.editCompanyDisabled } fullWidth={true} id="company-state" label="State" defaultValue="" value={ this.state.company ? this.state.company.state : company.state } onChange={this._onChangeCompanyState }/></p>
                                </Card.Text>
                                { this.state.editCompanyDisabled && (
                                    <button
                                        
                                        style={{
                                            width: "110px",
                                            borderRadius: "3px",
                                            letterSpacing: "1.5px",
                                            marginTop: "1rem",
                                            left: "87%"
                                        }}
                                        type="submit"
                                        className="btn btn-medium waves-effect waves-light hoverable white-text"
                                        onClick={this._editCompanyInfo} 
                                        >
                                            <i className="material-icons right">edit</i>
                                        Edit
                                    </button>
                                )}
                                { !this.state.editCompanyDisabled && (
                                    <button
                                        style={{
                                            width: "110px",
                                            borderRadius: "3px",
                                            letterSpacing: "1.5px",
                                            marginTop: "1rem",
                                            left: "87%"
                                        }}
                                        type="submit"
                                        className="btn btn-medium waves-effect waves-light hoverable white-text"
                                        onClick={this._saveCompanyInfo} 
                                        >
                                            <i className="material-icons right">save</i>
                                        Save
                                    </button>
                                )}
                            </Card.Body>
                        </Card>
                    </TabPanel>
                    <TabPanel value="3">
                        <Card
                            style={{ padding: "2%" }}
                            className="animate__animated animate__zoomIn"
                        >
                            <Card.Body>
                                {/* <Card.Title style={{  }}>User Profile</Card.Title> */}
                                <Card.Text
                                    style={{  }}
                                    
                                >
                                    <p className="col l6 m6 s12"><TextField type="password" size="small" variant="outlined" fullWidth={true} required={ true } id="password" label="Password" onChange={this._onChangePassword } placeholder="Password" value={ this.state.password }/></p>
                                    {/* <p className="col l6 m6">&nbsp;</p> */}
                                    <p className="col l6 m6 s12"><TextField type="password" size="small" variant="outlined" fullWidth={true} required={ true } id="password-confirm" label="Password Confirmation" onChange={this._onChangePasswordConfirm } value={ this.state.passwordConfirm } placeholder="Password Confirmation" /></p>
                                    {/* <p className="col l6 m6">&nbsp;</p> */}
                                </Card.Text>
                                    <button
                                        
                                        style={{
                                            width: "110px",
                                            borderRadius: "3px",
                                            letterSpacing: "1.5px",
                                            marginTop: "1rem",
                                            left: "87%"
                                        }}
                                        type="submit"
                                        className="btn btn-medium waves-effect waves-light hoverable white-text"
                                        onClick={this._updateUserPassword } 
                                        >
                                            <i className="material-icons right">save</i>
                                        Save
                                    </button>
                            </Card.Body>
                        </Card>
                    </TabPanel>
                </TabContext>
            {/* </Paper> */}
          </div>
        </div>
      </div>
    );
  }
}
UserProfile.propTypes = {
  loadUserProfile: PropTypes.func.isRequired,
  saveUserProfile: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  user_profile: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
  user_profile: state.user_profile
});
export default connect(mapStateToProps, { loadUserProfile, saveUserProfile })(UserProfile);
