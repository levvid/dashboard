import React, { Component } from "react";
import PropTypes from "prop-types";
import { Card } from "react-bootstrap";
import { connect } from "react-redux";
import { loadReports } from "../../actions/reportActions";
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Tab from '@material-ui/core/Tab';
import TabContext from '@material-ui/lab/TabContext';
import TabList from '@material-ui/lab/TabList';
import TabPanel from '@material-ui/lab/TabPanel';
import TextField from '@material-ui/core/TextField';
import ReactLoading from 'react-loading';
import Checkbox from '@material-ui/core/Checkbox';


import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import Favorite from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';

class Reports extends Component {
  constructor(props) {
		super(props);
		this.state = {
            errors: {},
            currentTab: '1',
            
		};
	}

  componentDidMount() {
        console.log("In component didmount");
		this.setState({
            user: this.props.auth.user,
		});

		const filters = {
			user: this.props.auth.user,
		};


        console.log("Calling load user.");
		this.props.loadReports(filters);
	}

    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors,
            });
        }
        
    }

    _handleChange = (event, newValue) => {
        this.setState({
            currentTab: newValue
        })
    }


  render() {

    // console.log("reports: " + JSON.stringify(this.props));
    let reports = this.props.reports.reports.reports;
    let reportPreferences = this.props.reports.reports.reportPreferences;

    console.log("Reports: " + JSON.stringify(reports))

    if (!reports || !reportPreferences) {
			return (
				<div>
					<ReactLoading
						type={'spinningBubbles'}
						color={'#58cbe1'}
						height={this.state.width < 800 ? '55%' : '20%'}
						width={this.state.width < 800 ? '55%' : '20%'}
						className="center-align dash-progress-spinner"
					/>
				</div>
			);
		}


    return (
      <div style={{ marginTop: "1%", width: "100%" }} className="valign-wrapper">
        <div className="row" style={{ marginTop: "1%", width: "100%" }}>
          <div className="">
            {/* <h4>
              <b>Hey there,</b> {user.name.split(" ")[0]} {user.name.split(" ")[1]}
            </h4>
            <p><b>UUID: </b>{user.uuid}</p>
             */}
             {/* <Paper square className="animate__animated animate__zoomIn"> */}
                <TabContext value={this.state.currentTab}>
                    <AppBar position="static" style={{ backgroundColor: "transparent", color: "black" }}>
                    <TabList onChange={ this._handleChange } aria-label="" variant="">
                        <Tab label="Reports" value="1"/>
                        <Tab label="Preferences" value="2" />
                    </TabList>
                    </AppBar>
                    <TabPanel value="1">
                        <Card
						style={{ padding: "2%" }}
						className="animate__animated animate__zoomIn"
					>
						<Card.Body>
							{/* <Card.Title style={{  }}>User Profile</Card.Title> */}
							<Card.Text style={{  }}>
							Report List
                            </Card.Text>
						</Card.Body>
					</Card>
                    </TabPanel>

                    {/* Preferences  */}
                    <TabPanel value="2">
                        <Card
                            style={{ padding: "2%" }}
                            className="animate__animated animate__zoomIn"
                        >
                            <Card.Body>
                                {/* <Card.Title style={{  }}>User Profile</Card.Title> */}
                                <Card.Text
                                    style={{  }}
                                    
                                >
                                    Preferences
                                    <div>
                                        <FormGroup row>
                                            <FormControlLabel
                                                control={<Checkbox checked={this.state.weekly} onChange={this._handleWeeklyChange} name="weekly" />}
                                                label="Weekly"
                                            />
                                            <FormControlLabel
                                                control={
                                                <Checkbox
                                                    checked={this.state.monthly}
                                                    onChange={this._handleMonthlyChange}
                                                    name="monthly"
                                                    color="primary"
                                                />
                                                }
                                                label="Monthly"
                                            />

                                            <FormControlLabel
                                                control={
                                                <Checkbox
                                                    checked={this.state.yearly}
                                                    onChange={this._handleYearlyChange}
                                                    name="monthly"
                                                    color="primary"
                                                />
                                                }
                                                label="Yearly"
                                            />
                                            </FormGroup>
                                    </div>
                                </Card.Text>
                                
                            </Card.Body>
                        </Card>
                    </TabPanel>
                </TabContext>
            {/* </Paper> */}
          </div>
        </div>
      </div>
    );
  }
}
Reports.propTypes = {
  loadReports: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  reports: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
  reports: state.reports
});
export default connect(mapStateToProps, { loadReports })(Reports);
