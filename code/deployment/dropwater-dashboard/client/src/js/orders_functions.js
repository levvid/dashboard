import moment from 'moment-timezone';

const TimeZones = {
	PDT: 'America/Los_Angeles',
	PST: 'America/Los_Angeles',
	EST: 'America/New_York',
	EDT: 'America/New_York',
	CDT: 'America/Chicago',
	MDT: 'America/Denver',
	MST: 'America/Phoenix',
	ADT: 'America/Anchorage',
	HST: 'Pacific/Honolulu'
};

export const getUTCDateTime = (startDate, timeZone, zero) => {
	console.log('Start date passed ' + startDate + ' start: ' + zero);
	// startDate - true, endDate - false (zero)
	// console.log("End date: ")
	startDate = new Date(startDate);
	if (!zero) { // endDate
		// startDate = startDate + 1;
		
		startDate.setDate(startDate.getDate() + 1);
	}
	var year = startDate.getFullYear();
	var month = startDate.getMonth() + 1;
	var day = startDate.getDate();
	var hours = 0;
	var minutes = 0;
	var seconds = 0;
	var milliseconds = 0;

	var time_string = year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;
	console.log('Timezones: ' + TimeZones[timeZone]);
	var converted_date = moment.tz(new Date(time_string), TimeZones[timeZone]);
	if (!zero) {
		startDate.setSeconds(startDate.getSeconds() - 1);
	}
	console.log('Converted date:' + converted_date.format());
	converted_date.set({ hour: hours, minute: minutes, second: seconds, millisecond: milliseconds });
	console.log('Date b4 conversion' + converted_date.format());
	console.log('Time string is ' + time_string);
	var utc_date = new moment(converted_date).utc().format();
	console.log('UTC converted day ' + utc_date);

	return utc_date;
};

export const getTimezoneAbbreviation = () => {
	var zone = new Date().toLocaleTimeString('en-us', { timeZoneName: 'short' }).split(' ')[2];
	console.log('This is the timezone: ' + zone);
	var timeZone = moment.tz.guess(true);
	var time = new Date();

	var timeZoneOffset = time.getTimezoneOffset();
	var abbr = moment.tz.zone(timeZone).abbr(timeZoneOffset);

	console.log("TIMEZONE IS: " + abbr);
	if(abbr === 'PST') {
		abbr = 'PDT';
	}
	// return zone;
	return abbr;
};



