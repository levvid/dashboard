import { combineReducers } from "redux";
import authReducer from "./authReducers";
import errorReducer from "./errorReducers";
import orderReducer from "./orderReducers";
import overviewReducer from "./overviewReducers";
import userReducer from "./userReducers";
import inventoryReducer from "./inventoryReducers";
import userInteractionsReducer from "./userInteractionsReducers";
import orderCreateReducer from "./orderCreateReducers";
import KioskReducer from "./kioskReducers";
import PropagateReducer from "./PropagateReducer";
import MiscTestsReducer from './miscTestsReducers';
import UserProfileReducer from './userProfileReducers';
import ReportReducer from './reportReducers';


export default combineReducers({
  auth: authReducer,
  all_users: userReducer,
  kiosk: KioskReducer,
  errors: errorReducer,
  all_orders: orderReducer,
  dashboard_metrics: overviewReducer,
  inventory_list: inventoryReducer,
  user_interactions_list: userInteractionsReducer,
  create_order: orderCreateReducer,
  kioskDetails: PropagateReducer,
  misc: MiscTestsReducer,
  user_profile: UserProfileReducer,
  reports: ReportReducer,
});