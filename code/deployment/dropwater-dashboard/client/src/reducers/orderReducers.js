import { ALL_ORDERS, ORDER_TABLE_LOADING } from "../actions/types";

const initialState = {
  all_orders: {},
  loading: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case ALL_ORDERS:
      return {
        ...state,
        all_orders: action.payload,
      };
    case ORDER_TABLE_LOADING:
      return {
        ...state,
        loading: true,
      };
    default:
      return state;
  }
}
