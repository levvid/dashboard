const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const CompanySchema = new Schema({
    name: {
        type: String,
        required: true,
        index: true
    },
    date_created: {
        type: Date,
        default: Date.now
    },
    address: {
        type: String,
        required: false
    },
    city: {
        type: String,
        required: false
    },
    state: {
        type: String,
        required: false
    },
    poc: {
        type: String,
        required: false
    },
    email: {
        type: String,
        required: false
    },
    phone: {
        type: String,
        required: false
    },
    company_ID: {
        type: Number,
        required: false,
        index: { unique: true }
    },
    alias: {
        type: String,
        required: false
    }
});

module.exports = Company = mongoose.model("companies", CompanySchema);

