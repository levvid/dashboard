const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const DrinkPriceSchema = new Schema({
    kiosk_ID: {
        type: String,
        required: true,
        index: { unique: true }
    },
    last_updated: {
        type: Date,
        default: Date.now
    },
    dropBottleWater: {
        type: Number,
        required: false
    },
    dropBottleFlavor: {
        type: Number,
        required: false
    },
    dropBottleSupplement: {
        type: Number,
        required: false
    },
    refillWater: {
        type: Number,
        required: false
    },
    refillFlavor: {
        type: Number,
        required: false
    },
    refillSupplement: {
        type: Number,
        required: false
    },
});

module.exports = DrinkPrice = mongoose.model("drinkprice", DrinkPriceSchema);
