const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const InventoryMiniSchema = new Schema({
    kiosk_ID: {
        type: String,
        required: true,
        index: { unique: true }
    },
    last_updated: {
        type: Date,
        default: Date.now
    },
    guava: {
        type: Number,
        required: false
    },
    cucumber: {
        type: Number,
        required: false
    },
    lemonade: {
        type: Number,
        required: false
    },
    cusaMango: {
        type: Number,
        required: false
    },
    cusaLemon: {
        type: Number,
        required: false
    },
    mint: {
        type: Number,
        required: false
    },
    towerSelected: {
        type: String,
        required: false,
        default: "1"
    },
    towerOneRemaining: {
        type: Number,
        required: false
    },
    towerTwoRemaining: {
        type: Number,
        required: false
    },
    towerThreeRemaining: {
        type: Number,
        required: false
    },
    caffeine: {
        type: Number,
        required: false
    },
    drainTankStatus: {
        type: String,
        required: false,
        default: "Okay", // Values - Okay, Full, Unresponsive...?
    },
    syncStatus: {
        type: Boolean,
        default: false
    }
});

module.exports = InventoryMini = mongoose.model("inventorymini", InventoryMiniSchema);