const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const KioskSchema = new Schema({
    kiosk_ID: {
        type: String,
        required: true,
        index: { unique: true }
    },
    date_created: {
        type: Date,
        default: Date.now
    },
    alias: {
        type: String,
        required: true
    },
    company_ID: {
        type: Number,
        required: false,
        index: true
    },
    location: {
        type: String,
        required: false
    },
    locationAlias: {
        type: String,
        required: false
    },
    losantId: {
        type: String,
        required: false,
        index: { unique: true }
    },
    nickname: {
        type: String,
        required: false,
        default: "aqua",
        index: { unique: true }
    },
    active: {
        type: Boolean,
        required: false,
        default: true,
        index: true
    },
    syncStatus: {
        type: Boolean,
        default: false
    }
});

// function findActiveMiddleware(next) {
//     this.where('active').equals(true);
//     next();
// }

// function findAllMiddleware(next) {
//     // this.where('active').equals(true);
//     next();
// }

KioskSchema.query.active = function() {
  return this.find({ active: true });
};

KioskSchema.query.activeOne = function() {
  return this.findOne({ active: true });
};


// KioskSchema.pre('find', findActiveMiddleware);
// KioskSchema.pre('findOne', findActiveMiddleware);
// KioskSchema.pre('findAll', findActiveMiddleware);


module.exports = Kiosk = mongoose.model("kiosks", KioskSchema);

