const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const KioskDataUseSchema = new Schema({
    kiosk_ID: {
        type: String,
        required: true,
        index: { unique: true }
    },
    date: {
        type: Date,
        required: true
    },
    day_total: {
        type: Number,
        required: true
    },
    tx: {
        type: Number,
        required: true
    },
    rx: {
        type: Number,
        required: true
    }
});

KioskDataUseSchema.index({ date: -1 })


module.exports = KioskDataUse = mongoose.model("kioskdatause", KioskDataUseSchema);

