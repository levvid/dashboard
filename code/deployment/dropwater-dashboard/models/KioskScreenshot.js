const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const KioskScreenshotSchema = new Schema({
    kiosk_ID: {
        type: String,
        required: true,
        index: { unique: true }
    },
    date_created: {
        type: Date,
        default: Date.now,
        index: true
    },
    name: {
        type: String,
        required: false,
        index: { unique: true }
    },
});

module.exports = KioskScreenshot = mongoose.model("kioskscreenshots", KioskScreenshotSchema);

