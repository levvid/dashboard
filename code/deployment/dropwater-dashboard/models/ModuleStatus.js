const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const ModuleStatusSchema = new Schema({
    kiosk_ID: {
        type: String,
        required: true,
        index: { unique: true }
    },
    last_updated: {
        type: Date,
        default: Date.now
    },
    drainTankStatus: {
        type: String,
        required: false,
        default: "Okay", // Values - Okay, Full, Unresponsive...?
    },
    dropBottleStatus: {
        type: String,
        required: false,
        default: "Okay", // Values - On, Off
    },
    refillStatus: {
        type: String,
        required: false,
        default: "Okay", // Values - On, Off
    },
    uvFilterStatus: {
        type: String,
        required: false,
        default: "Okay", // Values - On, Off
    },
    syncStatus: {
        type: Boolean,
        default: false
    }
});

module.exports = ModuleStatus = mongoose.model("modulestatus", ModuleStatusSchema);