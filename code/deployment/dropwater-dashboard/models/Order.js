const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const OrderSchema = new Schema({
    kiosk_ID: {
        type: String,
        required: true
    },
    date_created: {
        type: Date,
        default: Date.now,
        index: true
    },
    drink_price: {
        type: Number,
        required: true
    },
    flavor: {
        type: String,
        required: true
    },
    caffeine_level: {
        type: Number,
        required: true
    },
    drop_bottle: {  // to be deprecated once order_type becomes standard
        type: Boolean,
        // required: true,
        default: true
    },
    refill_amount: {    
        type: Number,
        required: true
    },
    promo: {
        type: Boolean,
        default: false
    },
    order_type: {
        type: String, // btn, 
        enum: ['Drop Bottle', 'Screen Refill', 'Button Refill']
    }, 
    order_medium: {
        type: String,
        enum: ['Mobile', 'Kiosk'],
        default: 'Kiosk'
    },
    syncStatus: {
        type: Boolean,
        default: false
    }
});


function findActiveMiddleware(next) {
    this.where('active').equals(true);
    next();
}

function findAllMiddleware(next) {
    // this.where('active').equals(true);
    next();
}

OrderSchema.query.screenOrders = function() {
  return this.find({$or:[
        {order_type : "Drop Bottle"},
        {order_type : "Screen Refill"}
        ]
    });
};

OrderSchema.query.paidOrders = function() {
  return this.find({ drink_price : { '$gt': 0 }}).sort( { date_created: -1 });
};

OrderSchema.query.freeOrders = function() {
  return this.find({ drink_price : { '$eq': 0 }}).sort( { date_created: -1 });
};

OrderSchema.query.testOrders = function() {
  return this.find({ order_type : "Drop Bottle"});
};

OrderSchema.query.refillButtonOrders = function() {
  return this.find({ order_type : "Button Refill"}).sort( { date_created: -1 });
};

OrderSchema.query.allOrders = function() {
  return this.find().sort( { date_created: -1 });
};


OrderSchema.index({ date_created: -1});
OrderSchema.index({ date_created: -1, kiosk_ID: 1 });
OrderSchema.index({ kiosk_ID: 1 });


module.exports = Order = mongoose.model("orders", OrderSchema);

