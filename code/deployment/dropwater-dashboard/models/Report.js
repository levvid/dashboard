const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const ReportSchema = new Schema({
    uuid: {
        type: String,
        required: true,
        index: { unique: true }
    },
    date_created: {
        type: Date,
        default: Date.now,
        index: true
    },
    name: {
        type: String,
        required: false,
        index: { unique: true }
    },
    users: {
        type: Array,
        default: []
    }
});

module.exports = Report = mongoose.model("report", ReportSchema);

