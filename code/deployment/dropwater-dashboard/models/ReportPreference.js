const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const ReportPreferenceSchema = new Schema({
    uuid: {
        type: String,
        required: true,
        index: { unique: true }
    },
    date_created: {
        type: Date,
        default: Date.now,
    },
    date_modified: {
        type: Date,
        default: Date.now,
    },
    frequencies: {
        type: Array,
        // enum: ['weekly', 'monthly', 'yearly']
    },
    user: {
        type: String,  // change to foreign key
        index: true
    },
});

module.exports = ReportPreference = mongoose.model("reportpreferences", ReportPreferenceSchema);

