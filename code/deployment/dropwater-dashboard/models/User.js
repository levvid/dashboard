const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const UserSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    date_created: {
        type: Date,
        default: Date.now
    },
    uuid: {
        type: String,
        required: true,
        index: true
    },
    is_admin: {
        type: Boolean,
        default: false
    },
    is_super_admin: {
        type: Boolean,
        default: false
    },
    role: {
        type: String,  // admin && super admin to be deprecated
        default: "external-user"
    },
    profile_picture: {
        type: String,
        required: false
    },
    phone_number: {
        type: String,
        required: false
    },
    country: {
        type: String,
        required: false
    },
    state: {
        type: String,
        required: false
    },
    city: {
        type: String,
        required: false
    }, 
    company_ID: {
        type: Number,
        required: false,
        index: true
    },
});

module.exports = User = mongoose.model("users", UserSchema);

