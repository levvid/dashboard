const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const InteractionSchema = new Schema({
    kiosk_ID: {
        type: String,
        required: true
    },
    date_created: {
        type: Date,
        default: Date.now
    },
    order_ID: {
        type: String,
        required: false
    },
    events: {
        type: Array,
        required: false
    }
});

InteractionSchema.index({ date_created: -1 })
InteractionSchema.index({ date_created: -1, kiosk_ID: 1 })


module.exports = Interaction = mongoose.model("interactions", InteractionSchema);

