const express = require('express');
const router = express.Router();

// Load Inventory model
const Inventory = require('../../models/Inventory');
const Kiosk = require('../../models/Kiosk');
const Company = require('../../models/Company');
const KioskDataUse = require('../../models/KioskDataUse');
const InventoryMini = require('../../models/InventoryMini');


const colors = [
	'#C0392B',
	'#884EA0',
	'#2980B9',
	'#5DADE2',
	'#148F77',
	'#F7DC6F',
	'#AF601A',
	'#D35400',
	'#34495E',
]

const TimeZones = {
	PDT: 'America/Los_Angeles',
	PST: 'America/Los_Angeles',
	EST: 'America/New_York',
	EDT: 'America/New_York',
	CDT: 'America/Chicago',
	MDT: 'America/Denver',
	MST: 'America/Phoenix',
	ADT: 'America/Anchorage',
	HST: 'Pacific/Honolulu'
};

const convertDateToTimeZone = (date, timezone_abbr) => {
	return new Date(date).toLocaleString('en-US', { timeZone: TimeZones[timezone_abbr] });
}

const formatDate = (date) => {
	// process.env.DEBUG === "true" && console.log("this is the original date formatting" + date);
	let converted_date = new Date(date).toLocaleDateString();
	// console.log("Converted date is: " + converted_date);
	return converted_date;
}

function formatAMPM(converted_date) {
	var date = new Date(converted_date);
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var ampm = hours >= 12 ? 'pm' : 'am';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0' + minutes : minutes;
	var strTime = hours + ':' + minutes + ' ' + ampm;

	return strTime.toUpperCase();
}


function generateRandomColors(kiosks) {
	const randomColor = Math.floor(Math.random()*16777215).toString(16);
	let colorSet = new Set();

	while (colorSet.size < kiosks.length) {
		const randomColor = Math.floor(Math.random()*16777215).toString(16);
		const hexColorCode = '#' + randomColor;
		colorSet.add(hexColorCode);
	}
	return colorSet;
}


function assignKioskColors(kiosks) {

	let colorSetList = [...generateRandomColors(kiosks)];
	console.log('Color set is: ' + JSON.stringify(colorSetList));
	let kioskColorDict = {};
	count = 0;
	kiosks.forEach(kiosk => {
		kioskColorDict[kiosk] = colorSetList[count];
		count++;
	});
	return kioskColorDict;
	
}



function parseSelectedTower(tower) {
	if (tower == 'R') {
		return 'Right';
	} else if (tower =='L') {
		return 'Left';
	} else if (tower == '1') {
		return 'One';
	} else if (tower == '2') {
		return 'Two';
	} else if (tower == '3') {
		return 'Three';
	}
	return 'Left';
}



function convertValue(value) {
	if(value) {
		return value.toFixed(2);
	}
	return 0;
	
}

function getDailyLineGraph(kioskDataDict, kiosks) { // { date: [{kiosk_ID: dataUse }]}
	var labels = []; // { 'P3-3': ['Sep 20', ...] }
	var data = {};  // { 'P3-3': ['4GB', ...] }

	var normalizedKioskDataDict = normalizeDict(kioskDataDict, kiosks);

	// console.log("Test: " + JSON.stringify(kiosks));

	// normalizedKioskDataDict = kioskDataDict;
	// console.log("KIOSK DATAT DICT: " + JSON.stringify(kioskDataDict));
	var kioskLabels = {};
	let kiosk_IDs = [];
	Object.keys(normalizedKioskDataDict).forEach(function(formattedDate) {
		var dateList = normalizedKioskDataDict[formattedDate];
		labels.push(formattedDate);


		for (var i = 0; i < dateList.length; i++) {
			var kioskDataObj = dateList[i];
			Object.entries(kioskDataObj).forEach(([kiosk, dataUsed]) => {
				if (kiosk in data) {
					data[kiosk].push(Math.round((dataUsed/977 + Number.EPSILON) * 100) / 100);
				} else {
					data[kiosk] = [Math.round((dataUsed/977 + Number.EPSILON) * 100) / 100];
				}

				kioskLabels[kiosk] = kiosk;
			});
		}

	});

	// console.log("Data is: " + JSON.stringify(data));

	// Object.values(data).forEach(element => {
	// 	console.log("Data is: " + JSON.stringify(element));
	// });

	// console.log('Labels are: ' + JSON.stringify(kioskLabels));


	var backgroundColors = ['#2471A3', '#138D75', '#884EA0', '#A93226'];
	var borderColors = ['#ee5549', '#138D75', '#884EA0', '#A93226'];
	let dataset = [];
	let kioskColorDict = assignKioskColors(kiosks);

	kiosks.forEach(kiosk => {
		dataset.push(
			{
				label: kiosk,
				type: 'line',
				fill: false,
				lineTension: 0.4,
				backgroundColor: kioskColorDict[kiosk],
				borderColor: kioskColorDict[kiosk],
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: '#2471A3',
				pointBackgroundColor: '#fff',
				pointBorderWidth: 1,
				pointHoverRadius: 5,
				pointHoverBackgroundColor: '#2471A3',
				pointHoverBorderColor: '#2471A3',
				pointHoverBorderWidth: 2,
				pointRadius: 1,
				pointHitRadius: 10,
				data: data[kiosk]
			}
		)
	});

	console.log('KioskIDs: ' + JSON.stringify(kiosk_IDs))
	const graph = {
		labels: labels,
		datasets: dataset
	};

	const graphAndColors = {
		'graph': graph,
		'kioskColorDict': kioskColorDict
	}

	return graphAndColors;
}


function My_array_compare(a,b){
	// console.log("A: " + JSON.stringify(a) + ' B: ' + JSON.stringify(b))
   var not_in_a=[];
   var j=0;
   for(var i=0; i<=b.length-1; i++){
	//    console.log("B1 " + JSON.stringify(b[i]) + ' ' + a.indexOf(b[i]))
		if(a.indexOf(b[i]) === -1){
			not_in_a.push(b[i]);
			j++;
		}

   }

	return not_in_a;
}

function normalizeDict(data, kiosks) {
	let normalizedData = {};
	Object.keys(data).forEach((dataDate) => {
		var tmp = data[dataDate];
		// console.log("Data: " + JSON.stringify(tmp));
		var mapped_kiosks = [];
		tmp.forEach(element => {
			// console.log("TMP: " + JSON.stringify(element));
			for (const [kiosk, total_data] of Object.entries(element)) {
				// console.log(`${kiosk}: ${total_data} HIUHSIUHIUSHIUHSIUH`);
				mapped_kiosks.push(kiosk);

			}
		});
		var not_mapped = My_array_compare(mapped_kiosks, kiosks);

		let tmpList = [];
		not_mapped.forEach(kiosk => {
			tmpList.push({ [kiosk]  : 0 })
		});

		let newList = tmpList.concat(tmp);

		// console.log("New list is: " + JSON.stringify(newList));
		normalizedData[dataDate] = newList;


	});
	return normalizedData;
}

function getKioskDataUseHelper(res, kiosk_data, mapped_inventory, kiosks, companies, kioskObjects, timezone) {
	var kiosk_data_dict = {};
	var kioskGraphDict = {}; // { date: [{kiosk_ID: dataUse }]}
	// console.log('This is kiosk data: ' + JSON.stringify(kiosk_data));
	for (var i = 0; i < kiosk_data.length; i++) {
		if (kiosk_data[i].kiosk_ID in kiosk_data_dict) {
			kiosk_data_dict[kiosk_data[i].kiosk_ID] += kiosk_data[i].day_total;
		} else {
			kiosk_data_dict[kiosk_data[i].kiosk_ID] = kiosk_data[i].day_total;
		}

		var dataDate = formatDate(kiosk_data[i].date);
		var tmp = {};
		tmp[kiosk_data[i].kiosk_ID] = kiosk_data[i].day_total;

		if (dataDate in kioskGraphDict) {
			kioskGraphDict[dataDate].push(tmp)
		} else {
			kioskGraphDict[dataDate] = [tmp];
		}
		//console.log('Date: ' + kiosk_data[i].date.toISOString() + ' data: ' + kiosk_data[i].day_total/(2**10));
	}

	var kioskIdDict = {};

	for (var i =0; i < kioskObjects.length; i++) {
		kioskIdDict[kioskObjects[i].kiosk_ID] = kioskObjects[i].losantId;
	}
	var lineGraphAddColors = getDailyLineGraph(kioskGraphDict, kiosks);
	let kioskColorDict = lineGraphAddColors['kioskColorDict'];

	var mapped_kiosk_data = [];
	for (var i = 0; i < mapped_inventory.length; i++) {
		var tmp = mapped_inventory[i];
		if (Object.keys(kiosk_data_dict).length) {
			tmp['dataUsed'] = Math.round((kiosk_data_dict[tmp.kiosk_ID]/977 + Number.EPSILON) * 100) / 100;
		} else {
			tmp['dataUsed'] = 0;
		}


		if (tmp['dataUsed'] > 1000) {
			tmp['dataUsed'] = Math.round((kiosk_data_dict[tmp.kiosk_ID]/976562 + Number.EPSILON) * 100) / 100;
			tmp['dataUsed'] = tmp['dataUsed'].toString() + ' GB'
		} else {
			tmp['dataUsed'] = tmp['dataUsed'].toString() + ' MB'
		}

		tmp['losantId'] = kioskIdDict[tmp.kiosk_ID];
		tmp['kioskColor'] = kioskColorDict[tmp.kiosk_ID];

		mapped_kiosk_data.push(tmp);
	}

	// console.log('THis is kioskGraph DIct: ' + JSON.stringify(kioskGraphDict));
	
	// console.log('This is test: ' + JSON.stringify(dailyLineGraph))
	// console.log(mapped_kiosk_data)
	InventoryMini.find({ }).lean().then((inventorymini) => {
		const mapped_inventory_mini =  inventorymini.map((kiosk_inventory) => ({
			key: kiosk_inventory.id,
			kiosk_ID: kiosk_inventory.kiosk_ID,
			kioskColor: kioskColorDict[kiosk_inventory.kiosk_ID],
			towerSelected: parseSelectedTower(kiosk_inventory.towerSelected),
			towerOneRemaining: kiosk_inventory.towerOneRemaining,
			towerOneBgColor: kiosk_inventory.towerOneRemaining < 30 ?"#FF7E65":"",
			
			towerTwoRemaining: kiosk_inventory.towerTwoRemaining,
			towerTwoBgColor: kiosk_inventory.towerTwoRemaining < 30 ?"#FF7E65":"",
			
			towerThreeRemaining: kiosk_inventory.towerThreeRemaining,
			towerThreeBgColor: kiosk_inventory.towerThreeRemaining < 30 ?"#FF7E65":"",
			
			caffeine: convertValue(kiosk_inventory.caffeine),
			caffeineBgColor: kiosk_inventory.caffeine < 75?"#FF7E65":"",
			
			guava: convertValue(kiosk_inventory.guava),
			guavaBgColor: kiosk_inventory.guava < 75?"#FF7E65":"",
			
			cucumber: convertValue(kiosk_inventory.cucumber),
			cucumberBgColor: kiosk_inventory.cucumber < 75?"#FF7E65":"",
			lemonade: convertValue(kiosk_inventory.lemonade),
			lemonadeBgColor: kiosk_inventory.lemonade < 75?"#FF7E65":"",
			lastInteraction: lastInteraction[kiosk_inventory.kiosk_ID],
		}));
		const summary = {
			mapped_inventory: mapped_kiosk_data,
			dailyLineGraph: lineGraphAddColors['graph'],
			kiosks: kiosks,
			companies: companies,
			mapped_inventory_mini: mapped_inventory_mini
		};
		getLastInteraction(res, summary, timezone)

	});

}

function getKioskDataUse (res, mapped_inventory, kiosks, companies, timezone) {
	var date = new Date();
	var firstDay = Date.UTC(date.getFullYear(), date.getMonth(), 1);
	// console.log('This is first_date: ' + firstDay);
	// console.log('This is first_date: ' + new Date(firstDay));

	//console.log("Kiosks are: " + JSON.stringify(kiosks));
	KioskDataUse.find({ date: { $gte: firstDay }, kiosk_ID: { $in: kiosks}}).sort({ date: 1 }).lean().then((kiosk_data) => {
		Kiosk.find({ kiosk_ID: { $in: kiosks }}).lean().active().then((kioskObjects) => {
			Company.find({}).lean().distinct('name', function(err, companies) {
				getKioskDataUseHelper(res, kiosk_data, mapped_inventory, kiosks, companies, kioskObjects, timezone);
			});
		});
	});
}


function mapInventory(res, inventory, companyNames, kioskIds, timezone) {
	// Check if orders exist
	if (!inventory) {
		return res.status(404).json({ inventorynotfound: 'No Inventory Found' });
	}

	const mapped_inventory = inventory.map((kiosk_inventory) => ({
		key: kiosk_inventory.id,
		kiosk_ID: kiosk_inventory.kiosk_ID,
		towerSelected: parseSelectedTower(kiosk_inventory.towerSelected),
		leftTowerRemaining: kiosk_inventory.leftTowerRemaining,
		leftTowerBgColor: kiosk_inventory.leftTowerRemaining < 75?"#FF7E65":"",
		rightTowerRemaining: kiosk_inventory.rightTowerRemaining,
		rightTowerBgColor: kiosk_inventory.rightTowerRemaining < 75?"#FF7E65":"",
		caffeine: convertValue(kiosk_inventory.caffeine),
		caffeineBgColor: kiosk_inventory.caffeine < 75?"#FF7E65":"",
		guava: convertValue(kiosk_inventory.guava),
		guavaBgColor: kiosk_inventory.guava < 75?"#FF7E65":"",
		cucumber: convertValue(kiosk_inventory.cucumber),
		cucumberBgColor: kiosk_inventory.cucumber < 75?"#FF7E65":"",
		lemonade: convertValue(kiosk_inventory.lemonade),
		lemonadeBgColor: kiosk_inventory.lemonade < 75?"#FF7E65":"",
		drainTankStatus: kiosk_inventory.drainTankStatus,
		drainTankStatusBgColor: kiosk_inventory.drainTankStatus !== 'Okay'?"#FF7E65":"",
		lastInteraction: lastInteraction[kiosk_inventory.kiosk_ID],
	}));
	//console.log(mapped_inventory)
	getKioskDataUse(res, mapped_inventory, kioskIds, companyNames, timezone);

}


function getLastInteraction(res, summary, timezone) {
	let ordersDict = {};

	Order.aggregate([
            { $sort: { date_created : -1, kiosk_ID: 1 } },
            { $group: { _id: "$kiosk_ID", date_created: { $first: "$date_created" } } }
          ], function(err, orders) {

			  //console.log(" are: " + JSON.stringify(orders));
			  orders.forEach(order => {
				  // console.log("Order is: " + JSON.stringify(order))
					ordersDict[order._id] = order;
					//console.log(JSON.stringify(Object.keys(ordersDict)) + ' is ID');

			});
			// console.log("DICT: " + JSON.stringify(ordersDict));
			let mapped_inventory = summary.mapped_inventory;
			let tmp_mapped_inventory = [];
			mapped_inventory.forEach(inventoryObj => {
				// console.log(JSON.stringify(ordersDict[inventoryObj.kiosk_ID]) + " IS KEY");
				// console.log(JSON.stringify(inventoryObj.kiosk_ID))
				if(ordersDict[inventoryObj.kiosk_ID]) {
					inventoryObj['lastInteraction'] = formatDate(convertDateToTimeZone(ordersDict[inventoryObj.kiosk_ID]['date_created'], timezone)) + ' ' + formatAMPM(convertDateToTimeZone(ordersDict[inventoryObj.kiosk_ID]['date_created'], timezone));
				}
				tmp_mapped_inventory.push(inventoryObj);
			});

			summary.mapped_inventory = tmp_mapped_inventory;


			let mapped_inventory_mini = summary.mapped_inventory_mini;
			let tmp_mapped_inventory_mini = [];
			mapped_inventory_mini.forEach(inventoryObj => {
				// console.log(JSON.stringify(ordersDict[inventoryObj.kiosk_ID]) + " IS KEY");
				// console.log(JSON.stringify(inventoryObj.kiosk_ID))
				if(ordersDict[inventoryObj.kiosk_ID]) {
					inventoryObj['lastInteraction'] = formatDate(convertDateToTimeZone(ordersDict[inventoryObj.kiosk_ID]['date_created'], timezone)) + ' ' + formatAMPM(convertDateToTimeZone(ordersDict[inventoryObj.kiosk_ID]['date_created'], timezone));
				}
				tmp_mapped_inventory_mini.push(inventoryObj);
			});


			summary.mapped_inventory_mini = tmp_mapped_inventory_mini;

			return res.json(summary);
		
	});

}



function arrayIntersection(a, b){
  var ai=0, bi=0;
  var result = [];

  while( ai < a.length && bi < b.length )
  {
     if      (a[ai] < b[bi] ){ ai++; }
     else if (a[ai] > b[bi] ){ bi++; }
     else /* they're equal */
     {
       result.push(a[ai]);
       ai++;
       bi++;
     }
  }

  return result;
}

let lastInteraction = {};

router.post('/inventory/', (req, res) => {
	const selected_kiosk = req.body.kiosk;
	const company = req.body.company;

	const companies = req.body.companies;
	const kiosks = req.body.kiosks;
	const timezone = req.body.timezone;
	// getLastInteraction(kiosks, timezone);
	//console.log("Companies: " + JSON.stringify(companies));
	//console.log("Kiosks: " + JSON.stringify(kiosks));

	var kioskQuery = {};
	var companyQuery = {};

	if (companies !== undefined && companies.length > 0) {  // no company selected, send all
		companyQuery = { ...companyQuery, name: { $in: companies }};
	} 
	// data usage
	// inventory
	// companies
	// kiosks
	Company.find(companyQuery).lean().distinct('company_ID', function(err, companyNames) {
		Kiosk.find({ company_ID: { $in: companyNames } }).lean().active().distinct('kiosk_ID', function(err, kioskIds) {

			var kioskIntersection = kioskIds;

			if (kiosks && kiosks.length > 0) {
				//console.log(kiosks.length + ' setting this');
				kioskIntersection = arrayIntersection(kioskIntersection, kiosks);
				if (kioskIntersection.length === 0) {
					kioskIntersection = kioskIds;
				}
			}
			
			//console.log("Kiosk intersect: " + JSON.stringify(kioskIntersection));
			Inventory.find({ kiosk_ID: { $in: kioskIntersection } }).lean().then((inventory) => {
				mapInventory(res, inventory, companyNames, kioskIntersection, timezone);
			});

			// Inventory.find(query).stream()
			// .on('data', function(doc){
			// 	// handle doc
			// })
			// .on('error', function(err){
			// 	// handle error
			// })
			// .on('end', function(){
			// 	// final callback
			// });
		});
	});

	// process.env.DEBUG === "true" && console.log('selected kiosk is ' + selected_kiosk + ' company: ' + company);
});

module.exports = router;
