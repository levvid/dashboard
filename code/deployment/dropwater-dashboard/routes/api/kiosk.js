const express = require('express');
const router = express.Router();

const Kiosk = require('../../models/Kiosk');
const Inventory = require('../../models/Inventory');
const DrinkPrice = require('../../models/DrinkPrice');
const KioskScreenshot = require('../../models/KioskScreenshot');
const ModuleStatus = require('../../models/ModuleStatus');
const Order = require('../../models/Order');


const AWS = require('aws-sdk');

require('dotenv').config();

AWS.config.update({
	accessKeyId: process.env.AWSAccessKeyId,
	secretAccessKey: process.env.AWSSecretKey
});

let s3 = new AWS.S3();

var BUCKET_NAME = 'dropwater-staging';

if (process.env.ENV === 'prod') {
	BUCKET_NAME = 'dropwater';
}



const TimeZones = {
	PDT: 'America/Los_Angeles',
	PST: 'America/Los_Angeles',
	EST: 'America/New_York',
	EDT: 'America/New_York',
	CDT: 'America/Chicago',
	MDT: 'America/Denver',
	MST: 'America/Phoenix',
	ADT: 'America/Anchorage',
	HST: 'Pacific/Honolulu'
};

function convertDateToTimeZone(date, timezone_abbr) {
	return new Date(date).toLocaleString('en-US', { timeZone: TimeZones[timezone_abbr] });
}

function formatDate(date) {
	var options = { month: 'short', day: 'numeric' };
	return new Date(date).toLocaleDateString("en-US", options);
}


async function retrieveS3Images(res, orderQuery, inventory, drinkPrice, kiosk, kioskScreenshots, moduleStatus, timezone, limit) {
	var screenshotImages = kioskScreenshots.map(getObject);
	Promise.all(screenshotImages).then((screenshotList) => {
		
		mapOrders(res, orderQuery, inventory, drinkPrice, kiosk, screenshotList, moduleStatus, timezone, limit);
	});
}


async function getObject(kiosk) {
	var keyName = 'kiosk_screenshots/' + kiosk.name + '.jpg';
	// console.log('Key is: ' + keyName + ' bucket: ' + BUCKET_NAME);

	const params = { Bucket: BUCKET_NAME, Key: keyName }; // keyname can be a filename
	try {
		const data = await s3.getObject(params).promise();

		// console.log("Image retrieved: " + JSON.stringify(data.Body.toString('utf-8')))
		// return data.Body.toString('utf-8');

		return {
			name: kiosk.name,
			date_created: kiosk.date_created,
			image: 'data:image/png;base64, ' + data.Body.toString('utf-8')
		};
	} catch (e) {
		throw new Error(`Could not retrieve file from S3: ${e.message}`);
	}
}

function getS3Image(key) {
	var keyName = 'kiosk_screenshots/' + key;
	var params = { Bucket: BUCKET_NAME, Key: keyName }; // keyname can be a filename

	s3.getObject(params, function(err, data) {
		if (err) {
			return null; // fix for this?? - handle this better
		}

		return data;
	});
}

function calcTimeDiff(date1, date2) {
	var diff = Math.floor(date1.getTime() - date2.getTime());
	var day = 1000 * 60 * 60 * 24;

	var totalDays = Math.floor(diff / day);

	var totalYears = Math.floor(totalDays / (12 * 31));

	var daysLeft = totalDays - totalYears * 12 * 31;
	var months = Math.floor(daysLeft / 31);

	days = daysLeft - months * 31;

	var timeDiff = '';
	if (totalYears > 0) {
		timeDiff += totalYears + ' years ';
	}

	if (months > 0) {
		timeDiff += months + ' months ';
	}

	if (days > 0) {
		timeDiff += days + ' days';
	}

	return timeDiff;
}

function calcDiffInDays(first, second) {
	// Take the difference between the dates and divide by milliseconds per day.
	// Round to nearest whole number to deal with DST.
	return Math.round((second - first) / (1000 * 60 * 60 * 24));
}

function getFlavorChartGraph(ordersPerFlavor) {
	var labels = [];
	var data = [];
	// var total = 0;
	const bgColor = {
		cucumber: '#73AF59',
		water: '#5DADE2',
		lemonade: '#fff44f',
		guava: '#ee5549',
		caffeine: '#AF7AC5'
	};

	const hoverBgColor = {
		cucumber: '#73AF59',
		water: '#5DADE2',
		lemonade: '#fff44f',
		guava: '#ee5549',
		caffeine: '#AF7AC5'
	};

	var bgColorArr = [];
	var hoverBgColorArr = [];

	Object.keys(ordersPerFlavor).forEach(function(flavor) {
		labels.push(flavor[0].toUpperCase() + flavor.slice(1));
		data.push(ordersPerFlavor[flavor]);
		bgColorArr.push(bgColor[flavor]);
		hoverBgColorArr.push(hoverBgColor[flavor]);
	});
	const graph = {
		labels: labels,
		datasets: [
			{
				data: data,
				// backgroundColor: [ '#FF6384', '#36A2EB', '#FFCE56', '#117864', '#8E44AD' ],  // legacy
				backgroundColor: bgColorArr,
				// hoverBackgroundColor: [ '#FF6384', '#36A2EB', '#FFCE56', '#117864', '#8E44AD' ],
				hoverBackgroundColor: hoverBgColorArr
			}
		]
	};

	return graph;
}

function mapOrders(res, orderQuery, inventory, drinkPrice, kiosk, screenshotList, moduleStatus, timezone, limit) {
	// console.log('Screenshots: ' + JSON.stringify(screenshotList));
	var daysInOperation = '';
	var numDaysInOperation = 0;
	var compostableTransactions = 0;
	var compostableSales = 0;
	var refillTransactions = 0;
	var refillSales = 0;
	var refillPaidTransactions = 0;
	var waterOunces = 0;

	var ordersPerFlavor = {};
	var caffeinePerFlavor = {};
	let dailyServingsDict = {};
	

	function updateDicts(order) {
		if (!(order.flavor in ordersPerFlavor)) {
			ordersPerFlavor[order.flavor] = 1;
		} else {
			ordersPerFlavor[order.flavor] += 1;
		}
		if (order.caffeine_level > 0) { // caffeine transactions
			if (!('caffeine' in caffeinePerFlavor)) {
				caffeinePerFlavor['caffeine'] = 1;
			} else {
				caffeinePerFlavor['caffeine'] += 1;
			}
		} else { // non caffeine trans
			if (!('water' in caffeinePerFlavor)) {
				caffeinePerFlavor['water'] = 1;
			} else {
				caffeinePerFlavor['water'] += 1;
			}
		}
	}

	let counter = 0;
	function processOrder(order) {
		counter += 1;
		// var order = orders[i];
		var order_date = formatDate(convertDateToTimeZone(order.date_created, timezone));
		// console.log("Order is: " + JSON.stringify(order));
		if (counter === 1) {
			daysInOperation = calcTimeDiff(new Date(), new Date(order.date_created));
			numDaysInOperation = calcDiffInDays(new Date(order.date_created), new Date());
		}
		if (order.order_type === 'Drop Bottle') {
			compostableTransactions += 1;
			compostableSales += order.drink_price;
			updateDicts(order);
		} else {
			if(order.refill_amount > 0) {
				if(order.order_type === 'Screen Refill') {
					refillSales += order.drink_price;
					refillTransactions += 1;
					if (order.drink_price > 0) {
						refillPaidTransactions += 1;
					}
					updateDicts(order);
				} else if (order.order_type === 'Button Refill') {
					if(!(order_date in dailyServingsDict)) {
						dailyServingsDict[order_date] = order.refill_amount;
					} else {
						dailyServingsDict[order_date] += order.refill_amount;
					}
				}
			} else {
				// counter += 1;
				// console.log("Order is 00000" + counter);
			}
		}
		waterOunces += order.refill_amount;
	}


	function finalizeOrders() {
		Object.keys(dailyServingsDict).forEach(function(serving_date) {
			// console.log('Adding');
			let num_servings = Math.ceil(dailyServingsDict[serving_date]/16);
			refillTransactions += num_servings;	
		});
		// console.log("Caffeine: " + JSON.stringify(caffeinePerFlavor));
		const flavor_graph = getFlavorChartGraph(ordersPerFlavor);
		const caffeine_flavor_graph = getFlavorChartGraph(caffeinePerFlavor);

		const summary = {
			daysInOperation: daysInOperation,
			compostableTransactions: compostableTransactions,
			compostableSales: (compostableSales / 100).toFixed(2),
			refillPaidTransactions: refillPaidTransactions,
			refillSales: (refillSales / 100).toFixed(2),
			refillTransactions: refillTransactions,
			averageCompostableTransactions: (compostableTransactions / numDaysInOperation).toFixed(2),
			averageRefillTransactions: (refillTransactions / numDaysInOperation).toFixed(2),
			averageCompostableSales: (compostableSales / 100 / numDaysInOperation).toFixed(2),
			averageRefillSales: (refillSales / 100 / numDaysInOperation).toFixed(2),
			sales: ((refillSales + compostableSales) / 100).toFixed(2),
			transactions: refillTransactions + compostableTransactions,
			waterOunces: (waterOunces * 0.0295735).toFixed(2), // convert to litres
			inventory: inventory,
			drinkPrice: drinkPrice,
			kiosk: kiosk,
			flavor_graph: flavor_graph,
			caffeine_flavor_graph: caffeine_flavor_graph,
			screenshotList: screenshotList,
			moduleStatus: moduleStatus
		};

		console.timeEnd('kiosk_qry');
		return res.json(summary);
	}

	const cursor = Order.find({ ...orderQuery, kiosk_ID: kiosk.kiosk_ID })
	.select({
		date_created: -1,
		kiosk_ID: 1,
		drink_price: 1,
		flavor: 1,
		caffeine_level: 1,
		drop_bottle: 1,
		refill_amount: 1,
		order_type: 1,
	})
	.lean()
	.limit(limit)
	.cursor();
	
	// cursor.limit(limit);
		
	cursor.on('data', function(order) { 
		// console.log("received order." + JSON.stringify(order));
		processOrder(order);

	});

	
	cursor.on('end', function() { 
		console.log('All orders loaded.'); 
		cursor.close();
		finalizeOrders();
	});	
}

router.post('/kiosk/', (req, res) => {
	const selected_kiosk = req.body.uuid;
	const compostable = req.body.compostable;
	const refill = req.body.refill;
	const timezone = req.body.timezone;
	const limit = req.body.limit;

	var orderQuery = {};

	console.time('kiosk_qry')
	// console.log("Order query is: " + JSON.stringify(orderQuery));
	// console.log('kiosk called' + JSON.stringify(selected_kiosk));
	// console.log('selected kiosk is ' + selected_kiosk);

	Kiosk.findOne({ losantId: selected_kiosk }).lean().activeOne().then((kiosk) => {
		// console.log('Returned kiosk is ' + JSON.stringify(kiosk.kiosk_ID));
		Inventory.findOne({ kiosk_ID: kiosk.kiosk_ID }).lean().then((inventory) => {
			// console.log('Inventory  is ' + JSON.stringify(inventory));
			DrinkPrice.findOne({ kiosk_ID: kiosk.kiosk_ID }).lean().then((drinkPrice) => {
				KioskScreenshot.find({ kiosk_ID: kiosk.kiosk_ID })
					.sort({ date_created: 'descending' })
					.limit(4)
					.lean()
					.then((screenshots) => {
						ModuleStatus.findOne({ kiosk_ID: kiosk.kiosk_ID}).lean().then((moduleStatus) => {
							retrieveS3Images(res, orderQuery, inventory, drinkPrice, kiosk, screenshots, moduleStatus, timezone, limit);
						});
					});
			});
		});
	});
});

router.post('/info/', (req, res) => {
	// const order = req.body.order;
	const kioskNickname = req.body.kioskNickname;

	console.log('Called with this one: ' + kioskNickname);

	Kiosk.findOne({ nickname: kioskNickname })
		.activeOne()
		.then((kiosk) => {
			// console.log('The kiosk is: ' + JSON.stringify(kiosk));
			DrinkPrice.findOne({ kiosk_ID: kiosk.kiosk_ID }).lean().then((drinkprice) => {
				// console.log('The price is: ' + JSON.stringify(drinkprice));
				Inventory.findOne({ kiosk_ID: kiosk.kiosk_ID }).lean().then((inventory) => {
					ModuleStatus.findOne({ kiosk_ID: kiosk.kiosk_ID }).lean().then((moduleStatus) => {
						// console.log('The inventory is: ' + JSON.stringify(inventory));
						const summary = {
							kiosk: kiosk,
							inventory: inventory,
							drinkprice: drinkprice,
							moduleStatus: moduleStatus
						};
						// console.log("Found kiosk nickname: " + JSON.stringify(summary));
						if (!kiosk) {
							console.log('No kiosk found.');
						}

						if (!drinkprice) {
							console.log('No drinkprice found.');
						}

						if (!drinkprice) {
							console.log('No drinkprice found.');
						}

						return res.json(summary);
					})
					
				});
			});
		})
		.catch((err) => {
			console.log('Errored out.' + JSON.stringify(err));
			return res.status(404).json({ kiosknotfound: 'No Kiosk Found' });
		});
});

// wait to see if this is necessary
router.post('/authenticate/', (req, res) => {
	const order = req.body.order;
	const kioskNickname = req.body.kioskNickname;

	console.log('Called: ' + kioskNickname);
	// if (!kioskNickname) {
	// 	return res.status(404).json({ ordersnotfound: 'No Transactions Found' });
	// }
	return res.json('Authenticate called');
});

module.exports = router;
