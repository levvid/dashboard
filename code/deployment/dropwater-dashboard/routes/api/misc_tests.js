const express = require('express');
const router = express.Router();
var SerialPort = require("serialport");

let modules = {};

let ports = ["/dev/ttyACM0", "/dev/ttyACM1", "/dev/ttyACM2"];
// const WebSocket = require('ws');


var cycle = false;
var counter = 111;

function registerModules() {
	console.log("Registering modules....");
	
	for (var i = 0; i<ports.length; i++) {
		newSerialPort(ports[i]);
	}
	console.log("Set port.");
	// console.log("Modules set: " + JSON.stringify(modules));
	// modules.module2 = newSerialPort(paths[1]);
	// modules.module3 = newSerialPort(paths[2]);
}

function newSerialPort(portPath) {
  var port = new SerialPort(portPath);
  console.log("Creating new serial port.");
  port.on("open", function() {
    // Then flush buffers
    port.flush(function (error) {
      if (error) {
        console.log("error", "Serial port flush error: ", error);
        return;
      }
      else {
        // Then wait 400ms then send reset command
        // setTimeout(function() {
		// 	sendSignal(port, portPath);
        // }, 400);
		console.log("Port is: ");
		// return {
		// 	port: port,
		// 	portPath: portPath
		// }
		modules[portPath] = {
			port: port,
			portPath: portPath
		}
		return;
	// 	return new Promise((resolve, reject) => {  
		
	// 	if(string === "K") {    
	// 		resolve({
	// 			port: port,
	// 			portPath: portPath
	// 		});  
	// 	} else {    
	// 		reject("None");  
	// 	}
	// });
      }
    });
  });
};


// registerModules();

function sendSignal (module) {
	let port = module.port;
  // Send reset command
  console.log("Sending signal....");
  if (port) {

	  port.write("!", function (error) {  // change to C
		if (error) {
			console.log("error", "ModuleManager performReset write error: ", error);
			}
		});
  }
}


function checkStatus(module) {
	let port = module.port;
	

  return new Promise((resolve, reject) => { 
	port.on("data", function (data) {
		var string = data.toString();
		console.log("Module returned." + JSON.stringify(string));
		if(string === "K" || string === "A") {    
			resolve(string);  
		} else {    
			console.log("Rejecting promise. " + string);
			reject(string);  
		}
		
	});
	// resolve("K");
		
	});
}
var prod = true;

function runCycle() {
	if(prod) {
		sendSignal(modules[ports[0]]);
		checkStatus(modules[ports[0]]).then((status) => {
			console.log("Check status returned " + status);
			sendSignal(modules[ports[1]]);
			checkStatus(modules[ports[1]]).then((status) => {
				console.log("Check status returned " + status);
				sendSignal(modules[ports[2]]);
				checkStatus(modules[ports[2]]).then((status) => {
					console.log("Check status returned " + status);
					counter += 1;
					console.log("Counter update: " + counter);
					if(cycle) {
						runCycle();
					}
				});
			});
		});

	} else {
		counter += 1;
		
		if(cycle  && counter < 10) {
			runCycle();
		}
	}
	
}


function testWS() {
	// io.emit("Counter", counter);
	// sendMessage(counter);
	
}

router.post('/additive_tests/', (req, res) => {
	console.log("Additive test called.");

	let command = req.body.command;

	console.log("Command is: " + command);
	// console.log("Modules are: " + JSON.stringify(modules));
	if (command === "Start") {
		if(!cycle) {  // only start if not running
			cycle = true;
			runCycle(res);
		} 
	} else {
		cycle = false;
	}
	

	// process.env.DEBUG === "true" && console.log('selected kiosk is ' + selected_kiosk + ' company: ' + company);
});

router.post('/additive_counter/', (req, res) => {
	console.log("Additive counter called.");
	const summary = {
		counter: counter
	}
	return res.json(summary);
	

	// process.env.DEBUG === "true" && console.log('selected kiosk is ' + selected_kiosk + ' company: ' + company);
});

router.get('/additive_counter/', (req, res) => {
	console.log("Additive GET counter called.");
	const summary = {
		counter: counter
	}
	return res.json(summary);
	

	// process.env.DEBUG === "true" && console.log('selected kiosk is ' + selected_kiosk + ' company: ' + company);
});

module.exports = router;
