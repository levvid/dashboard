const moment = require('moment-timezone');

const express = require('express');
const router = express.Router();

// const cachegoose = require('cachegoose');
// const mongoose = require('mongoose');

// cachegoose(mongoose, {
//   engine: 'redis',    /* If you don't specify the redis engine,      */
// //   port: 23139,         /* the query results will be cached in memory. */
//   url: 'redis://h:pf2d9b470239b33168e54926b9e4d1d18922f567429eab663243369099b11621f@ec2-3-227-73-83.compute-1.amazonaws.com:23139',
// 	// host: 'localhost'
// });

// Load Order model
const Order = require('../../models/Order');
const Kiosk = require('../../models/Kiosk');
const Company = require('../../models/Company');
const Inventory = require('../../models/Inventory');
const DrinkPrice = require('../../models/DrinkPrice');

const passport = require('passport');
function getBottleType(drop_bottle) {
	if (drop_bottle) {
		return 'Compostable';
	}
	return 'Refill';
}

function getPaymentStatus(drink_price) {
	if (drink_price == 0) {
		return 'Free';
	}
	return 'Paid';
}

function formatAMPM(converted_date) {
	var date = new Date(converted_date);
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var ampm = hours >= 12 ? 'pm' : 'am';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0' + minutes : minutes;
	var strTime = hours + ':' + minutes + ' ' + ampm;

	return strTime.toUpperCase();
}

// const TimeZones = {
// 	PDT: 'America/Los_Angeles',
// 	PST: 'America/Los_Angeles',
// 	EST: 'America/New_York',
// 	EDT: 'America/New_York',
// 	CDT: 'America/Chicago',
// 	MDT: 'America/Denver',
// 	MST: 'America/Phoenix',
// 	ADT: 'America/Anchorage',
// 	HST: 'Pacific/Honolulu'
// };

// const TimeZones = moment.tz.zonesForCountry('US');

function getTimeZoneLongname(abbr) {
	// let zones = moment.tz.zonesForCountry('US');
	let zones = moment.tz.names();

	zones.forEach(zone => {
		if (moment.tz(zone).zoneAbbr() === 'abbr') {
			return zone;
		}	
	});
}

function getTimezoneAbbreviations() {
  var regExp = /[a-zA-Z]/g;
  var i = 0;
  var timezones = [];
  let zones = moment.tz.names()
  let zone = '';
  for (i=0; i < zones.length; i++) {
    zone = moment.tz(zones[i]).zoneAbbr();

    if (regExp.test(zone)) {
      timezones.push(zone);
    }
    
  }

  return Array.from(new Set(timezones)).sort();

//   return ['PDT', 'EDT', 'CDT', 'MDT', 'MST', 'ADT', 'HST']
}


function convertDateToTimeZone(date, timezone_abbr) {
	var tmp =  new Date(date).toLocaleString('en-US', { timeZone: getTimeZoneLongname(timezone_abbr) });


	// console.log("TEst is: " + tmp)
	return tmp;
}

function formatDate(date) {
	// process.env.DEBUG === "true" && console.log("this is the original date formatting" + date);
	let converted_date = new Date(date).toLocaleDateString();
	// console.log("Converted date is: " + converted_date);
	return converted_date;
}

function getKioskSummary(concourse, order, converted_date) {
	// process.env.DEBUG === "true" && console.log("Concourse: " + JSON.stringify(locations));
	if (order.drop_bottle) {
		concourse['num_compostable'] += 1;
		concourse['revenue_compostable'] += order.drink_price / 100;
	} else {
		

		if(order.order_type === 'Button Refill') {
			if(converted_date in concourse['button_refill']) {
				concourse['button_refill'][converted_date] += order.refill_amount;
			} else {
				concourse['button_refill'][converted_date] = order.refill_amount;
			}
		} else if(order.order_type === 'Screen Refill') {
			// else if(order.order_type === 'Screen Refill') {
			if (order.refill_amount > 0) {
				if (order.drink_price > 0) {
					concourse['num_paid_refills'] += 1;
					concourse['revenue_refill'] += order.drink_price / 100;
				} else {
					concourse['num_free_refills'] += 1;
				}
			} else {
				// console.log("Order amt is 0: " + JSON.stringify(order));
			}
			
		}
	}
}

function getKioskLocationsDicts(orders, kiosks, timeZone, companies, res) {
	var locations = {};
	var kioskNames = [];
	// console.log("KIosks: " + JSON.stringify(kiosks));
	kiosks.forEach((kiosk) => {
		kioskNames.push(kiosk.kiosk_ID);
		process.env.DEBUG === 'true' && console.log('Kiosk:' + kiosk.kiosk_ID);
		locations[kiosk.kiosk_ID] = {
			location: kiosk.alias,
			num_free_refills: 0,
			num_paid_refills: 0,
			num_compostable: 0,
			revenue_refill: 0,
			revenue_compostable: 0,
			average_refill: 0,
			average_compostable: 0,
			total_revenue: 0,
			button_refill: {},
		};
	});

	var mapped_orders = [];
	let btn_refill = 0;
	process.env.DEBUG === 'true' && console.log('Locations: ' + JSON.stringify(locations));
	orders.forEach(function(order) {
		// console.log('Locations[order].....' + JSON.stringify(locations[order.kiosk_ID]));
		if (locations[order.kiosk_ID]) {
			let converted_date = formatDate(convertDateToTimeZone(order.date_created, timeZone))
			getKioskSummary(locations[order.kiosk_ID], order, converted_date);
		}
		btn_refill += order.refill_amount;
		

		mapped_orders.push({
			key: order._id,
			date_created: order.date_created,
			time_created: formatAMPM(order.date_created),
			kiosk_ID: order.kiosk_ID,
			drink_price: (order.drink_price / 100).toFixed(2),
			flavor: order.flavor[0].toUpperCase() + order.flavor.slice(1),
			caffeine_level: order.caffeine_level,
			order_type: order.order_type,
			refill_amount: order.refill_amount.toFixed(2),
			promo: getPaymentStatus(order.drink_price),
			converted_date: formatDate(convertDateToTimeZone(order.date_created, timeZone)),
			converted_time: formatAMPM(convertDateToTimeZone(order.date_created, timeZone))
		});
	});
	console.log("Btn refill: " + Math.ceil(btn_refill/16));

	kiosks.forEach((kiosk) => {
		getAverages(locations[kiosk.kiosk_ID]);
	});

	// kioskNames.unshift('Kiosk');
	// companies.unshift('Company');
	const summary = {
		mapped_orders: mapped_orders,
		kioskSummary: Object.values(locations),
		kiosks: kioskNames,
		companies: companies,
		timezones: getTimezoneAbbreviations()
	};
	// process.env.DEBUG === "true" && console.timeEnd('Getting_kiosks');
	process.env.DEBUG === 'false' && console.timeEnd('Start_order_query');
	//console.log("Mapped Orders: " + JSON.stringify(mapped_orders[0]));
	return res.json(summary);
}

function getAverages(concourse) {
	//console.log("Free refills b4: " + concourse['num_free_refills'] + ' kiosk:');
	// concourse['num_free_refills'] += concourse['button_refill'] > 0 ? Math.ceil(concourse['button_refill'] / 16 ): 0;
	//console.log("Free refills after: " + concourse['num_free_refills']);
	// console.log("Concourse " + JSON.stringify(concourse['button_refill']))
	// concourse['button_refill'].forEach(order_date => {
	// 	concourse['num_free_refills'] += Math.ceil(concourse['button_refill'][date_created]/16)
	// });

	Object.keys(concourse['button_refill']).forEach(function(order_date) {
		concourse['num_free_refills'] += Math.ceil(concourse['button_refill'][order_date]/16)
	});
	
	if (!concourse['revenue_refill']) {
		return;
	}

	if (concourse['num_paid_refills'] > 0) {
		concourse['average_refill'] = (concourse['revenue_refill'] / concourse['num_paid_refills']).toFixed(2);
	} else {
		concourse['average_refill'] = concourse['revenue_refill'].toFixed(2);
	}

	if (concourse['num_compostable'] > 0) {
		concourse['average_compostable'] = (concourse['revenue_compostable'] / concourse['num_compostable']).toFixed(2);
	} else {
		concourse['average_compostable'] = concourse['revenue_compostable'].toFixed(2);
	}

	concourse['revenue_refill'] = concourse['revenue_refill'].toFixed(2);
	
	
	
	concourse['revenue_compostable'] = concourse['revenue_compostable'].toFixed(2);
	concourse['total_revenue'] = (parseFloat(concourse['revenue_refill']) +
		parseFloat(concourse['revenue_compostable'])).toFixed(2);
}

function mapOrders(orders, res, timeZone, companies) {
	// Check if orders exist
	if (!orders) {
		return res.status(404).json({ ordersnotfound: 'No Transactions Found' });
	}

	/////////////////////////////////////////////
	if (companies && companies.length > 0) {
		// process.env.DEBUG === "true" && console.time('Getting_kiosks');
		Company.find({ name: { $in: companies } }).lean().distinct('company_ID', function(err, companyNames) {
			Kiosk.find({ company_ID: { $in: companyNames } }).lean().active().sort({ kiosk_ID: 1 }).then((kiosks) => {
				Company.find({}).lean().distinct('name', function(err, companies) {
					getKioskLocationsDicts(orders, kiosks, timeZone, companies, res);
				});
			});
		});
	} else {
		// process.env.DEBUG === "true" && console.time('Getting_kiosks');
		Kiosk.find({}).sort({ kiosk_ID: 1 }).lean().active().then((kiosks) => {
			Company.find({}).lean().distinct('name', function(err, companies) {
				getKioskLocationsDicts(orders, kiosks, timeZone, companies, res);
			});
		});
	}
}



function arrayIntersection(a, b){
  var ai=0, bi=0;
  var result = [];

  while( ai < a.length && bi < b.length )
  {
     if      (a[ai] < b[bi] ){ ai++; }
     else if (a[ai] > b[bi] ){ bi++; }
     else /* they're equal */
     {
       result.push(a[ai]);
       ai++;
       bi++;
     }
  }

  return result;
}


router.post('/all_orders/', (req, res) => {
	//console.log("All orders called");
	const startDate = req.body.startDate;
	const endDate = req.body.endDate;
	process.env.DEBUG === 'true' && console.log('Start date: ' + startDate + ' endDate: ' + endDate);
	const selected_kiosk = req.body.kiosk;
	const timeZone = req.body.timeZone;
	const company = req.body.company;

	const companies = req.body.companies;
	const bottleTypes = req.body.bottleTypes;
	const kiosks = req.body.kiosks;
	const transactionTypes = req.body.transactionTypes;

	const order_category = req.body.order_category;

	//console.log('Companies: ' + JSON.stringify(companies));
	//console.log('bTypes: ' + JSON.stringify(bottleTypes));
	//console.log('Kiosks: ' + JSON.stringify(kiosks));
	//console.log('tType: ' + JSON.stringify(transactionTypes));
	//console.log('Category: ' + JSON.stringify(order_category))

	var query = {
		date_created: {
			$gte: startDate,
			$lt: endDate
		}
	};

	if (bottleTypes && bottleTypes.length === 1) {
		var dropBottle = true;

		if (bottleTypes[0] === 'Refill') {
			dropBottle = false;
		}
		query = { ...query, drop_bottle: dropBottle };
	}

	if (transactionTypes && transactionTypes.length === 1) {
		if (transactionTypes[0] === 'Paid') {
			// paid drinks
			query = { ...query, drink_price: { $gt: 0 } };
		} else {
			query = { ...query, drink_price: { $eq: 0 } }; // free drinks
		}
	}

	if (companies === undefined || companies.length === 0) {
		// company not selected
		if (kiosks && kiosks.length > 0) {
			query = { ...query, kiosk_ID: { $in: kiosks } };
		}

		//console.log("Query is: " + JSON.stringify(query));
		process.env.DEBUG === 'false' && console.time('Start_order_query');
		if(order_category === 'paid') {
			Order.find(query)
			.paidOrders()
			.select({
				date_created: -1,
				kiosk_ID: 1,
				drink_price: 1,
				flavor: 1,
				caffeine_level: 1,
				drop_bottle: 1,
				refill_amount: 1,
				promo: 1
			})
			// .sort({ date_created: 1 })
			.lean()
			.then((orders) => {
				// process.env.DEBUG === "true" && console.log("these are orders " + orders);
				process.env.DEBUG === 'true' && console.log('Number of orders: ' + orders.length);
				return mapOrders(orders, res, timeZone, companies);
			});
		} else if(order_category === 'free') {
			Order.find(query)
			.freeOrders()
			.select({
				date_created: -1,
				kiosk_ID: 1,
				drink_price: 1,
				flavor: 1,
				caffeine_level: 1,
				drop_bottle: 1,
				refill_amount: 1,
				promo: 1
			})
			// .sort({ date_created: 1 })
			.lean()
			.then((orders) => {
				// process.env.DEBUG === "true" && console.log("these are orders " + orders);
				process.env.DEBUG === 'true' && console.log('Number of orders: ' + orders.length);
				return mapOrders(orders, res, timeZone, companies);
			});
		} else if (order_category === 'gui') {
			Order.find(query)
			.screenOrders()
			.select({
				date_created: -1,
				kiosk_ID: 1,
				drink_price: 1,
				flavor: 1,
				caffeine_level: 1,
				drop_bottle: 1,
				refill_amount: 1,
				promo: 1
			})
			// .sort({ date_created: 1 })
			.lean()
			.then((orders) => {
				// process.env.DEBUG === "true" && console.log("these are orders " + orders);
				process.env.DEBUG === 'true' && console.log('Number of orders: ' + orders.length);
				return mapOrders(orders, res, timeZone, companies);
			});
		} else if( order_category === 'refill_button') {
			Order.find(query)
			.refillButtonOrders()
			.select({
				date_created: -1,
				kiosk_ID: 1,
				drink_price: 1,
				flavor: 1,
				caffeine_level: 1,
				drop_bottle: 1,
				refill_amount: 1,
				promo: 1
			})
			// .sort({ date_created: 1 })
			.lean()
			.then((orders) => {
				// process.env.DEBUG === "true" && console.log("these are orders " + orders);
				process.env.DEBUG === 'true' && console.log('Number of orders: ' + orders.length);
				return mapOrders(orders, res, timeZone, companies);
			});
		}
	} else {
		Company.find({ name: { $in: companies } }).lean().distinct('company_ID', function(err, companyNames) {
			Kiosk.find({ company_ID: { $in: companyNames } }).lean().active().distinct('kiosk_ID', function(err, kioskIds) {
				
				var kioskIntersection = kioskIds;

				if (kiosks && kiosks.length > 0) {
					console.log(kiosks.length + ' setting this')
					kioskIntersection = arrayIntersection(kioskIntersection, kiosks);
					if (kioskIntersection.length === 0) {
						kioskIntersection = kioskIds;
					}
				}
				query = { ...query, kiosk_ID: { $in: kioskIntersection } };
				Order.find(query)
					// .screenOrders()
					.select({
						date_created: -1,
						kiosk_ID: 1,
						drink_price: 1,
						flavor: 1,
						caffeine_level: 1,
						drop_bottle: 1,
						refill_amount: 1,
						promo: 1
					})
					// .sort({ date_created: 1 })
					.lean()
					.then((orders) => {
						// Check if orders exist
						//console.log("These are the orders: " + JSON.stringify(orders));
						mapOrders(orders, res, timeZone, companies);
					});
			});
		});
	}
});

router.post('/new_order', (req, res) => {
	const newOrder = new Order({
		kiosk_ID: req.body.kiosk_ID,
		drink_price: req.body.drink_price,
		flavor: req.body.flavor,
		caffeine_level: req.body.caffeine_level,
		drop_bottle: req.body.drop_bottle,
		refill_amount: req.body.refill_amount
	});
	newOrder
		.save()
		.then((order) => {
			return res.json(order);
		})
		.catch((err) => {
			return res.status(404).json(err);
		});
});

router.post('/orderdetails', (req, res) => {
	const order = req.body.order;
	const kioskNickname = req.body.kioskNickname;

	//console.log('Called: ' + kioskNickname);

	return res.status(200);
});


router.post('/paid/', (req, res) => {
	console.log("Paid orders called");
	const startDate = req.body.startDate;
	const endDate = req.body.endDate;
	process.env.DEBUG === 'true' && console.log('Start date: ' + startDate + ' endDate: ' + endDate);
	const selected_kiosk = req.body.kiosk;
	const timeZone = req.body.timeZone;
	const company = req.body.company;

	const companies = req.body.companies;
	const types = req.body.types;
	const kiosks = req.body.kiosks;
	const transactionTypes = req.body.transactionTypes;

	//console.log('Companies: ' + JSON.stringify(companies));
	//console.log('bTypes: ' + JSON.stringify(types));
	//console.log('Kiosks: ' + JSON.stringify(kiosks));
	//console.log('tType: ' + JSON.stringify(transactionTypes));

	var query = {
		date_created: {
			$gte: startDate,
			$lt: endDate
		}
	};

	if (types) {
		if (types.length === 1) { 
			query = { ...query, order_type: types[0] };
		} 
	} 

	if (transactionTypes && transactionTypes.length === 1) {
		if (transactionTypes[0] === 'Paid') {
			// paid drinks
			query = { ...query, drink_price: { $gt: 0 } };
		} else {
			query = { ...query, drink_price: { $eq: 0 } }; // free drinks
		}
	}

	if (companies === undefined || companies.length === 0) {
		// company not selected
		if (kiosks && kiosks.length > 0) {
			query = { ...query, kiosk_ID: { $in: kiosks } };
		}

		//console.log("Query is: " + JSON.stringify(query));
		process.env.DEBUG === 'false' && console.time('Start_order_query');
		
		Order.find(query)
			.select({
				date_created: -1,
				kiosk_ID: 1,
				drink_price: 1,
				flavor: 1,
				caffeine_level: 1,
				order_type: 1,
				refill_amount: 1,
				promo: 1,
				drop_bottle: 1,
			})
			// .sort({ date_created: 1 })
			.lean()
			.paidOrders()
			.then((orders) => {
				// process.env.DEBUG === "true" && console.log("these are orders " + orders);
				process.env.DEBUG === 'true' && console.log('Number of orders: ' + orders.length);
				return mapOrders(orders, res, timeZone, companies);
			});
	} else {
		Company.find({ name: { $in: companies } }).distinct('company_ID', function(err, companyNames) {
			Kiosk.find({ company_ID: { $in: companyNames } }).active().distinct('kiosk_ID', function(err, kioskIds) {
				
				var kioskIntersection = kioskIds;

				if (kiosks && kiosks.length > 0) {
					//console.log(kiosks.length + ' setting this')
					kioskIntersection = arrayIntersection(kioskIntersection, kiosks);
					if (kioskIntersection.length === 0) {
						kioskIntersection = kioskIds;
					}
				}
				query = { ...query, kiosk_ID: { $in: kioskIntersection } };
				Order.find(query)
					.paidOrders()
					.select({
						date_created: -1,
						kiosk_ID: 1,
						drink_price: 1,
						flavor: 1,
						caffeine_level: 1,
						order_type: 1,
						refill_amount: 1,
						promo: 1,
						drop_bottle: 1,
					})
					.sort({ date_created: -1 })
					.lean()
					.then((orders) => {
						// Check if orders exist
						mapOrders(orders, res, timeZone, companies);
					});
			});
		});
	}
});


router.post('/free/', (req, res) => {
	console.log("Free orders called");
	const startDate = req.body.startDate;
	const endDate = req.body.endDate;
	process.env.DEBUG === 'true' && console.log('Start date: ' + startDate + ' endDate: ' + endDate);
	const selected_kiosk = req.body.kiosk;
	const timeZone = req.body.timeZone;
	const company = req.body.company;

	const companies = req.body.companies;
	const types = req.body.types;
	const kiosks = req.body.kiosks;
	const transactionTypes = req.body.transactionTypes;

	console.log('Companies: ' + JSON.stringify(companies));
	console.log('bTypes: ' + JSON.stringify(types));
	console.log('Kiosks: ' + JSON.stringify(kiosks));
	console.log('tType: ' + JSON.stringify(transactionTypes));

	var query = {
		date_created: {
			$gte: startDate,
			$lt: endDate
		}
	};

	if (types) {
		if (types.length === 1) { 
			query = { ...query, order_type: types[0] };
		} 
	} 

	if (transactionTypes && transactionTypes.length === 1) {
		if (transactionTypes[0] === 'Paid') {
			// paid drinks
			query = { ...query, drink_price: { $gt: 0 } };
		} else {
			query = { ...query, drink_price: { $eq: 0 } }; // free drinks
		}
	}

	if (companies === undefined || companies.length === 0) {
		// company not selected
		if (kiosks && kiosks.length > 0) {
			query = { ...query, kiosk_ID: { $in: kiosks } };
		}

		console.log("Query is: " + JSON.stringify(query));
		process.env.DEBUG === 'false' && console.time('Start_order_query');
		
		Order.find(query)
			.freeOrders()
			.select({
				date_created: -1,
				kiosk_ID: 1,
				drink_price: 1,
				flavor: 1,
				caffeine_level: 1,
				order_type: 1,
				refill_amount: 1,
				promo: 1,
				drop_bottle: 1,  // to be deprecated
			})
			.sort({ date_created: -1 })
			.lean()
			.then((orders) => {
				// process.env.DEBUG === "true" && console.log("these are orders " + orders);
				process.env.DEBUG === 'true' && console.log('Number of orders: ' + orders.length);
				return mapOrders(orders, res, timeZone, companies);
			});
	} else {
		Company.find({ name: { $in: companies } }).distinct('company_ID', function(err, companyNames) {
			Kiosk.find({ company_ID: { $in: companyNames } }).active().distinct('kiosk_ID', function(err, kioskIds) {
				
				var kioskIntersection = kioskIds;

				if (kiosks && kiosks.length > 0) {
					//console.log(kiosks.length + ' setting this')
					kioskIntersection = arrayIntersection(kioskIntersection, kiosks);
					if (kioskIntersection.length === 0) {
						kioskIntersection = kioskIds;
					}
				}
				query = { ...query, kiosk_ID: { $in: kioskIntersection } };
				Order.find(query)
					.freeOrders()
					.select({
						date_created: -1,
						kiosk_ID: 1,
						drink_price: 1,
						flavor: 1,
						caffeine_level: 1,
						order_type: 1,
						refill_amount: 1,
						promo: 1,
						drop_bottle: 1,
					})
					.sort({ date_created: -1 })
					.lean()
					.then((orders) => {
						// Check if orders exist
						mapOrders(orders, res, timeZone, companies);
					});
			});
		});
	}
});


router.post('/all/', (req, res) => {
	//console.log("All orders (new) called");
	const startDate = req.body.startDate;
	const endDate = req.body.endDate;
	process.env.DEBUG === 'true' && console.log('Start date: ' + startDate + ' endDate: ' + endDate);
	const selected_kiosk = req.body.kiosk;
	const timeZone = req.body.timeZone;
	// const timeZone = 'GMT-7'
	const company = req.body.company;

	const companies = req.body.companies;
	const types = req.body.types;
	const kiosks = req.body.kiosks;
	const transactionTypes = req.body.transactionTypes;

	//console.log('Companies: ' + JSON.stringify(companies));
	console.log('bTypes: ' + JSON.stringify(types));
	//console.log('Kiosks: ' + JSON.stringify(kiosks));
	//console.log('tType: ' + JSON.stringify(transactionTypes));

	var query = {
		date_created: {
			$gte: startDate,
			$lt: endDate
		}
	};

	if (types) {
		if (types.length === 1) { 
			query = { ...query, order_type: types[0] };
		} else if (types.length > 1) {
			query = { ...query, order_type: { $in: types }}
		}
	} 

	if (transactionTypes && transactionTypes.length === 1) {
		if (transactionTypes[0] === 'Paid') {
			// paid drinks
			query = { ...query, drink_price: { $gt: 0 } };
		} else {
			query = { ...query, drink_price: { $eq: 0 } }; // free drinks
		}
	}

	if (companies === undefined || companies.length === 0) {
		// company not selected
		if (kiosks && kiosks.length > 0) {
			query = { ...query, kiosk_ID: { $in: kiosks } };
		}

		//console.log("Query is: " + JSON.stringify(query));
		process.env.DEBUG === 'false' && console.time('Start_order_query');
		
		Order.find(query)
			.select({
				date_created: -1,
				kiosk_ID: 1,
				drink_price: 1,
				flavor: 1,
				caffeine_level: 1,
				order_type: 1,
				refill_amount: 1,
				promo: 1,
				drop_bottle: 1,
			})
			.lean()
			.then((orders) => {
				process.env.DEBUG === 'true' && console.log('Number of orders: ' + orders.length);
				return mapOrders(orders, res, timeZone, companies);
			});
	} else {
		Company.find({ name: { $in: companies } }).distinct('company_ID', function(err, companyNames) {
			Kiosk.find({ company_ID: { $in: companyNames } }).active().distinct('kiosk_ID', function(err, kioskIds) {
				
				var kioskIntersection = kioskIds;

				if (kiosks && kiosks.length > 0) {
					//console.log(kiosks.length + ' setting this')
					kioskIntersection = arrayIntersection(kioskIntersection, kiosks);
					if (kioskIntersection.length === 0) {
						kioskIntersection = kioskIds;
					}
				}
				query = { ...query, kiosk_ID: { $in: kioskIntersection } };
				Order.find(query)
					// .paidOrders()
					.select({
						date_created: 1,
						kiosk_ID: 1,
						drink_price: 1,
						flavor: 1,
						caffeine_level: 1,
						order_type: 1,
						refill_amount: 1,
						promo: 1,
						drop_bottle: 1,
					})
					.sort({ date_created: -1 })
					.lean()
					.then((orders) => {
						// Check if orders exist
						mapOrders(orders, res, timeZone, companies);
					});
			});
		});
	}
});

module.exports = router;
