const express = require('express');
const router = express.Router();

// Load Order model
const Order = require('../../models/Order');
const Kiosk = require('../../models/Kiosk');
const Company = require('../../models/Company');
const ModuleStatus = require('../../models/ModuleStatus');
// const { convertDateToTimeZone, formatDate } = require('../js/functions');
const redis = require("redis");


function getSimpleDate(startDate) {
	var date = new Date(startDate);

	return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}

router.get('/dashboard', (req, res) => {
	process.env.DEBUG === "true" && console.log('Get dash called');
});


const TimeZones = {
	PDT: 'America/Los_Angeles',
	PST: 'America/Los_Angeles',
	EST: 'America/New_York',
	EDT: 'America/New_York',
	CDT: 'America/Chicago',
	MDT: 'America/Denver',
	MST: 'America/Phoenix',
	ADT: 'America/Anchorage',
	HST: 'Pacific/Honolulu'
};

function convertDateToTimeZone(date, timezone_abbr) {
	return new Date(date).toLocaleString('en-US', { timeZone: TimeZones[timezone_abbr] });
}

function formatDate(date) {
	var options = { month: 'short', day: 'numeric' };
	return new Date(date).toLocaleDateString("en-US", options);
}


// function returnProps() {}
const mapDashboard = (res, orders, timezone, companies) => {
	if (!orders) {
		return res.status(404).json({ ordersnotfound: 'Orders not found' });
	}

	var i = 0;
	var num_orders = 0;
	var num_paid_orders = 0;
	var tot_earnings = 0;
	var num_refills = 0;
	var num_compostable = 0;
	var ordersPerFlavor = {};
	var caffeinePerFlavor = {};
	// var ordersPerKiosk = {};
	// var salesPerKiosk = {};
	var kiosks = [];
	// Compostable transactions
	// var compostableWaterTransactionsPerKiosk = {};
	// var compostableWaterSalesPerKiosk = {};
	// var compostableAdditiveTransactionsPerKiosk = {};
	// var compostableAdditiveSalesPerKiosk = {};
	// var compostableGrossTransactionsPerKiosk = {};
	// var compostableGrossSalesPerKiosk = {};

	// Refill transactions
	// var refillWaterTransactionsPerKiosk = {};
	// var refillWaterSalesPerKiosk = {};
	// var refillAdditiveTransactionsPerKiosk = {};
	// var refillAdditiveSalesPerKiosk = {};
	// var refillGrossTransactionsPerKiosk = {};
	// var refillGrossSalesPerKiosk = {};
	var dailySalesDict = {};
	var dailyTransactionsDict = {};
	var dailyServingsDict = {};


	function updateDicts(order, order_date) {
		if (!(order_date in dailySalesDict)) {
				dailySalesDict[order_date] = order.drink_price
				dailyTransactionsDict[order_date] = 1;
			} else {
				dailySalesDict[order_date] += order.drink_price;
				dailyTransactionsDict[order_date] += 1;
			}

			if (!(order.flavor in ordersPerFlavor)) {
				ordersPerFlavor[order.flavor] = 1;
			} else {
				ordersPerFlavor[order.flavor] += 1;
			}

			if (order.caffeine_level > 0) { // caffeine transactions
				if (!('caffeine' in caffeinePerFlavor)) {
					caffeinePerFlavor['caffeine'] = 1;
				} else {
					caffeinePerFlavor['caffeine'] += 1;
				}
			} else { // non caffeine trans
				if (!('water' in caffeinePerFlavor)) {
					caffeinePerFlavor['water'] = 1;
				} else {
					caffeinePerFlavor['water'] += 1;
				}
			}
	}

	for (i = 0; i < orders.length; i++) {
		const order = orders[i];
		var order_date = formatDate(convertDateToTimeZone(order.date_created, timezone));
		if (order.drink_price > 0) {
			num_paid_orders += 1;
		}
		if (!kiosks.includes(order.kiosk_ID)) {
			kiosks.push(order.kiosk_ID);
		}
		if (order.order_type === 'Drop Bottle') {
			// compostable bottles
			num_compostable += 1;
			num_orders += 1;
			updateDicts(order, order_date);
		} else {
			// Refill transactions
			if (order.order_type === 'Button Refill') {
				if(!(order_date in dailyServingsDict)) {
					dailyServingsDict[order_date] = order.refill_amount;
				} else {
					dailyServingsDict[order_date] += order.refill_amount;
				}
			} else if(order.order_type === 'Screen Refill') {
				if (order.refill_amount > 0) { // only count orders that are more than 0 
					num_refills += 1;
					num_orders += 1;
					updateDicts(order, order_date);
				}
			}
			
		}

		tot_earnings += order.drink_price;
	}

	Object.keys(dailyServingsDict).forEach(function(serving_date) {
		// console.log("Num: " + dailyServingsDict[serving_date])
		let num_servings = Math.ceil(dailyServingsDict[serving_date]/16);
		// console.log("num servings" + num_servings)
		if(!(serving_date in  dailyTransactionsDict)) {
			
			dailyTransactionsDict[serving_date] = num_servings;
			// console.log("Test: " + dailyTransactionsDict[serving_date]);
		} else {
			dailyTransactionsDict[serving_date] += num_servings;
			// console.log("Adding to daily: " + dailyTransactionsDict[serving_date])
		}
		num_refills += num_servings;
		num_orders += num_servings;		
	});
	// console.log("Daily trans: " + JSON.stringify(dailyTransactionsDict))
	let tmp = 0;
	Object.keys(dailyTransactionsDict).forEach(transaction_date => {
		// num_refills += dailyTransactionsDict[transaction_date];
		tmp += dailyTransactionsDict[transaction_date]
	});


	const flavor_graph = getFlavorChartGraph(ordersPerFlavor);
	// console.log("")
	const caffeine_flavor_graph = getFlavorChartGraph(caffeinePerFlavor);

	if (companies && companies.length > 0) {
		Company.find({ name: { $in: companies } }).lean().distinct('company_ID', function(err, companyNames) {
			Kiosk.find({ company_ID: { $in: companyNames }  }).lean().active().distinct('kiosk_ID', function(err, kiosks) {
				Company.find({}).lean().distinct('name', function(err, companies) {
					const dashboard_objects = {
						num_orders: num_orders,
						num_paid_orders: num_paid_orders,
						tot_earnings: Math.trunc(tot_earnings / 100),
						num_refills: num_refills,
						num_compostable: num_compostable,
						flavor_graph: flavor_graph,
						caffeine_flavor_graph: caffeine_flavor_graph,
						daily_sales_graph: getDailyLineGraph(dailySalesDict, 'Sales', true),
						daily_transactions_graph: getDailyLineGraph(dailyTransactionsDict, 'Servings', false),
						companies: companies,
						kiosks: kiosks
					};
					console.timeEnd("overview");
					return res.json(dashboard_objects);
				});
			});
		});
	} else {
		Kiosk.find({}).lean().sort({ kiosk_ID: 1 }).active().distinct('kiosk_ID', function(err, kiosks) {
			Company.find({}).lean().distinct('name', function(err, companies) {
				const dashboard_objects = {
						num_orders: num_orders,
						num_paid_orders: num_paid_orders,
						tot_earnings: Math.trunc(tot_earnings / 100),
						num_refills: num_refills,
						num_compostable: num_compostable,
						flavor_graph: flavor_graph,
						caffeine_flavor_graph: caffeine_flavor_graph,
						daily_sales_graph: getDailyLineGraph(dailySalesDict, 'Sales', true),
						daily_transactions_graph: getDailyLineGraph(dailyTransactionsDict, 'Servings', false),
						companies: companies,
						kiosks: kiosks
					};
					console.timeEnd("overview");
					return res.json(dashboard_objects);
			});
		});
	}
};



function arrayIntersection(a, b){
  var ai=0, bi=0;
  var result = [];

  while( ai < a.length && bi < b.length )
  {
     if      (a[ai] < b[bi] ){ ai++; }
     else if (a[ai] > b[bi] ){ bi++; }
     else /* they're equal */
     {
       result.push(a[ai]);
       ai++;
       bi++;
     }
  }

  return result;
}


function createModuleStatus() {
	var moduleStatusUpdate = {
      drainTankStatus: "Okay",
      dropBottleStatus: "Off",
      refillStatus: "On",
      unFilterStatus: "On",
    };

    ModuleStatus.findOneAndUpdate(
      {kiosk_ID: "P3-8" }, // find a document with that filter
      moduleStatusUpdate, // document to insert when nothing was found
      {upsert: true, new: true, runValidators: true, useFindAndModify: false }, // options
      function (err, doc) { // callback
          if (err) {
              // handle error
            //   winston.log("debug", "Error updating module status to " );
              console.log("Error is: "  + JSON.stringify(err));
          } else {
              // handle document
              console.log("debug", "Successfully updated module status to ");
          }
        //   mongoose.connection.close();
      }
  );
}


router.post('/overview', (req, res) => {
	// createModuleStatus();
	console.time("overview");
	const startDate = req.body.startDate;
	const endDate = req.body.endDate;
	const company = req.body.company;
	const timezone = req.body.timezone;
	const bottleType = req.body.bottleType;


	const companies = req.body.companies;
	const orderTypes = req.body.orderTypes;
	const kiosks = req.body.kiosks;
	const transactionTypes = req.body.transactionTypes;

	var terms = startDate + endDate + company + timezone + bottleType;

	var query = { 
		date_created: { 
			$gte: startDate, 
			$lt: endDate  
		}
	};

	if (orderTypes && orderTypes.length === 1) {
		query = { ...query, order_type: { $in: orderTypes } };
	}

	

	if (transactionTypes && transactionTypes.length === 1) {
		if(transactionTypes[0] === 'Paid') {  // paid drinks
			query = { ...query, drink_price: { $gt: 0 }};
		} else {
			query = { ...query, drink_price: { $eq: 0 }};  // free drinks
		}
	}

	if (companies === undefined || companies.length === 0) {
		if (kiosks && kiosks.length > 0) {
			query = { ...query, kiosk_ID: { $in: kiosks }};
		}

		Order.find(query)
			.select({
				date_created: 1,
				kiosk_ID: 1,
				drink_price: 1,
				flavor: 1,
				caffeine_level: 1,
				drop_bottle: 1,
				refill_amount: 1,
				order_type: 1,
				// promo: 1
			})
			.lean()
			// .paidOrders()
			// .refillButtonOrders()
			// .screenOrders()
			.sort({ date_created: 1 })
			.then((orders) => {
				// Check if orders exist
				mapDashboard(res, orders, timezone, companies);
			});
	} else {
		Company.find({ name: { $in: companies } }).lean().distinct('company_ID', function(err, companyNames) {
			Kiosk.find({ company_ID: { $in: companyNames } }).lean().active().distinct('kiosk_ID', function(err, kioskIds) {
				var kioskIntersection = kioskIds;

				if (kiosks && kiosks.length > 0) {
					kioskIntersection = arrayIntersection(kioskIntersection, kiosks);
					if (kioskIntersection.length === 0) {
						kioskIntersection = kioskIds;
					}
				}
				query = { ...query, kiosk_ID: { $in: kioskIntersection } };
				
				Order.find(query)
					.select({
						date_created: 1,
						kiosk_ID: 1,
						drink_price: 1,
						flavor: 1,
						caffeine_level: 1,
						drop_bottle: 1,
						refill_amount: 1,
						order_type: 1,
						// promo: 1
					})
					// .screenOrders()
					.sort({ date_created: 1 })
					.lean()
					.then((orders) => {
						// Check if orders exist
						mapDashboard(res, orders, timezone, companies);
					});
			});
		});
	}
});

function getFlavorChartGraph(ordersPerFlavor) {
	var labels = [];
	var data = [];
	// var total = 0;
	const bgColor = {
		'cucumber': '#73AF59', 
		'water': '#5DADE2',
		'lemonade': '#fff44f',
		'guava': '#ee5549',
		'caffeine': '#AF7AC5'
	}

	const hoverBgColor = {
		'cucumber': '#73AF59', 
		'water': '#5DADE2',
		'lemonade': '#fff44f',
		'guava': '#ee5549',
		'caffeine': '#AF7AC5'
	}

	var bgColorArr = [];
	var hoverBgColorArr = [];

	Object.keys(ordersPerFlavor).forEach(function(flavor) {
		labels.push(flavor[0].toUpperCase() + flavor.slice(1));
		data.push(ordersPerFlavor[flavor]);
		bgColorArr.push(bgColor[flavor]);
		hoverBgColorArr.push(hoverBgColor[flavor]);
	});
	const graph = {
		labels: labels,
		datasets: [
			{
				data: data,
				// backgroundColor: [ '#FF6384', '#36A2EB', '#FFCE56', '#117864', '#8E44AD' ],  // legacy
				backgroundColor: bgColorArr,
				// hoverBackgroundColor: [ '#FF6384', '#36A2EB', '#FFCE56', '#117864', '#8E44AD' ],
				hoverBackgroundColor: hoverBgColorArr,
			}
		]
	};

	// console.log(JSON.stringify(Object.keys(graph).length));
	return graph;
}

function roundSaleAmount(amount) {
	return (amount / 100).toFixed(2);
}
function getKioskPieChart(ordersPerKiosk, sale = false) {
	var labels = [];
	var data = [];

	Object.keys(ordersPerKiosk).forEach(function(kiosk) {
		labels.push(kiosk);
		var num = ordersPerKiosk[kiosk];
		if (sale) {
			num = roundSaleAmount(num);
		}
		data.push(num);
	});

	const graph = {
		labels: labels,
		datasets: [
			{
				data: data,
				backgroundColor: [ '#FF6384', '#36A2EB', '#FFCE56', '#117864', '#8E44AD' ],
				hoverBackgroundColor: [ '#FF6384', '#36A2EB', '#FFCE56', '#117864', '#8E44AD' ]
			}
		]
	};

	return graph;
}

function getValue(dataDict, key, sale) {
	if (key in dataDict) {
		if (sale) {
			return (dataDict[key] / 100).toFixed(2);
		}
		return dataDict[key];
	}
	return 0;
}

function getKioskTransactionChart(waterTransactions, additiveTransactions, totalTransactions, kiosks, sale) {
	const datasets = [];
	const arbitraryStackKey = 'stack1';
	const colors = [
		[ '#FF6384', '#FF6384', '#FF6384' ],
		[ '#36A2EB', '#36A2EB', '#36A2EB' ],
		[ '#117864', '#117864', '#117864' ],
		[ '#8E44AD', '#8E44AD', '#8E44AD' ]
	];
	const hover_bg_color = [ [ '#FFCE56', '#FFCE56', '#FFCE56' ], [ '#0E6655', '#0E6655', '#0E6655' ] ];

	for (i = 0; i < kiosks.length; i++) {
		if (kiosks[i] in waterTransactions) {
			const dataset = {
				stack: arbitraryStackKey,
				label: kiosks[i],
				data: [
					getValue(waterTransactions, kiosks[i], sale),
					getValue(additiveTransactions, kiosks[i], sale),
					getValue(totalTransactions, kiosks[i], sale)
				],
				backgroundColor: colors[i]
				//hoverBackgroundColor: hover_bg_color[i],
			};
			datasets.push(dataset);
		} else {
			if (kiosks[i] in additiveTransactions) {
				const dataset = {
					stack: arbitraryStackKey,
					label: kiosks[i],
					data: [
						getValue(waterTransactions, kiosks[i], sale),
						getValue(additiveTransactions, kiosks[i], sale),
						getValue(totalTransactions, kiosks[i], sale)
					],
					backgroundColor: colors[i]
					//hoverBackgroundColor: hover_bg_color[i],
				};
				datasets.push(dataset);
			}
		}
	}

	const data = {
		labels: [ 'Water', 'Additives', 'Total' ],
		datasets: datasets
	};
	return data;
}





function getDailyLineGraph(orderDict, type, sale) {
	var labels = [];
	var data = [];
	

	Object.keys(orderDict).forEach(function(formattedDate) {
		labels.push(formattedDate);
		if (sale) {
			data.push((orderDict[formattedDate]/100).toFixed(0));
		} else {
			data.push(orderDict[formattedDate]);
		}
		Object.keys(orderDict[formattedDate]).forEach(function(kiosk) {
			if (sale) {
				data[kiosk].push((orderDict[formattedDate][kiosk]/100).toFixed(0));
			} else {
				data[kiosk].push(orderDict[formattedDate][kiosk]);
			}
			
			
		});
	});

	const graph = {
		labels: labels,
		datasets: [
			{
				label: type,
				type: 'line',
				fill: false,
				lineTension: 0.4,
				backgroundColor: '#19C7E8',
				borderColor: '#19C7E8',
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: '#19C7E8',
				pointBackgroundColor: '#fff',
				pointBorderWidth: 1,
				pointHoverRadius: 5,
				pointHoverBackgroundColor: '#19C7E8',
				pointHoverBorderColor: '#19C7E8',
				pointHoverBorderWidth: 2,
				pointRadius: 1,
				pointHitRadius: 10,
				data: data
			},
		]
	};

	return graph;
}

function getDailyBarGraph(orderDict, type, sale) {
	var labels = [];
	var data = [];
	Object.keys(orderDict).forEach(function(formattedDate) {
		labels.push(formattedDate);
		if (sale) {
			data.push((orderDict[formattedDate] / 100).toFixed(0));
		} else {
			data.push(orderDict[formattedDate]);
		}
	});

	const graph = {
		labels: labels,
		datasets: [
			{
				label: type,
				backgroundColor: 'rgba(255,99,132,0.2)',
				borderColor: 'rgba(255,99,132,1)',
				borderWidth: 1,
				hoverBackgroundColor: 'rgba(255,99,132,0.4)',
				hoverBorderColor: 'rgba(255,99,132,1)',
				data: data
			}
		]
	};
	return graph;
}

module.exports = router;
