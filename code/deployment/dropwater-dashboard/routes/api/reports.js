const express = require('express');
const router = express.Router();

const Reports = require('../../models/Report');
const ReportPreference = require('../../models/ReportPreference');

const AWS = require('aws-sdk');

require('dotenv').config();

AWS.config.update({
	accessKeyId: process.env.AWSAccessKeyId,
	secretAccessKey: process.env.AWSSecretKey
});

let s3 = new AWS.S3();

var BUCKET_NAME = 'dropwater-staging';

if (process.env.ENV === 'prod') {
	BUCKET_NAME = 'dropwater';
}

async function retrieveS3Images(res, orderQuery, inventory, drinkPrice, kiosk, kioskScreenshots, moduleStatus) {
	var screenshotImages = kioskScreenshots.map(getObject);
	Promise.all(screenshotImages).then((screenshotList) => {
		// console.log()
		console.log('Screenshotlist: ' + screenshotList.length);
		Order.find({ ...orderQuery, kiosk_ID: kiosk.kiosk_ID })
			.select({
				date_created: -1,
				kiosk_ID: 1,
				drink_price: 1,
				flavor: 1,
				caffeine_level: 1,
				drop_bottle: 1,
				refill_amount: 1
			})
			.lean()
			.then((orders) => {
				return mapOrders(res, orders, inventory, drinkPrice, kiosk, screenshotList, moduleStatus);
			});
	});
}

async function getObject(kiosk) {
	var keyName = 'kiosk_screenshots/' + kiosk.name + '.jpg';
	console.log('Key is: ' + keyName + ' bucket: ' + BUCKET_NAME);

	const params = { Bucket: BUCKET_NAME, Key: keyName }; // keyname can be a filename
	try {
		const data = await s3.getObject(params).promise();

		// console.log("Image retrieved: " + JSON.stringify(data.Body.toString('utf-8')))
		// return data.Body.toString('utf-8');

		return {
			name: kiosk.name,
			date_created: kiosk.date_created,
			image: 'data:image/png;base64, ' + data.Body.toString('utf-8')
		};
	} catch (e) {
		throw new Error(`Could not retrieve file from S3: ${e.message}`);
	}
}

function getS3Image(key) {
	var keyName = 'kiosk_screenshots/' + key;
	var params = { Bucket: BUCKET_NAME, Key: keyName }; // keyname can be a filename

	s3.getObject(params, function(err, data) {
		if (err) {
			return null; // fix for this?? - handle this better
		}

		return data;
	});
}

router.post('/reports/', (req, res) => {
	const user_uuid = req.body.user.uuid;
    console.log("reports called");

	var orderQuery = {};

	

	Reports.find({ users: user_uuid }).then((reports) => {
		console.log('Returned reports: ' + JSON.stringify(reports));
        
		ReportPreference.find({ user: user_uuid}).then((reportPreferences) => {
			console.log('Inventory  is ' + JSON.stringify(reportPreferences));


            let payload = {
                reports: reports,
                reportPreferences: reportPreferences
            }
            return res.json(payload);
		});
	});
});

module.exports = router;
