const express = require("express");
const router = express.Router();
// const AWS = require('aws-sdk');
var AWS = require('aws-sdk/dist/aws-sdk-react-native');
require('dotenv').config();


var bucketName = process.env.STAGING_BUCKET;
if (process.env.ENV != 'dev') {
  bucketName = process.env.PROD_BUCKET;
  process.env.DEBUG === "true" && console.log("Setting bucket name: " + bucketName);
  process.env.DEBUG === "true" && console.log("ENV:" + JSON.stringify(process.env.ENV));
} else {
  console.log("Using DEV DB");
  console.log(process.env.ENV === "dev");
}

AWS.config.update({
  accessKeyId: process.env.AWSAccessKeyId,
  secretAccessKey: process.env.AWSSecretKey,
}); 
AWS.config.logger = process.env.DEBUG === "true" && console.log
// s3Config = {
//   bucketName: bucketName,
//   // dirName: 'media', /* optional */
//   region: 'us-west-1',
//   accessKeyId: process.env.AWSAccessKeyId,
//   secretAccessKey: process.env.AWSSecretKey,
//   // s3Url: 'https:/your-custom-s3-url.com/', /* optional */
// }

// Load Inventory model
const UserInteraction = require("../../models/UserInteraction");


function convertValue(value) {
  return value.toFixed(2);
}

function getSignedURL(myKey) {

  const signedUrlExpireSeconds = 60 * 5
  const s3 = new AWS.S3();

  process.env.DEBUG === "true" && console.log("This is params:" + bucketName + " key: " + myKey);
  const url = s3.getSignedUrl('getObject', {
      Bucket: bucketName,
      Key: myKey,
      Expires: signedUrlExpireSeconds
  });
  return url;

  
}
function mapUserInteractions(user_interactions, res) {
  process.env.DEBUG === "true" && console.log('Mapped called')
  // Check if orders exist
  if (!user_interactions) {
    return res.status(404).json({ userinteractionsnotfound: "No user Interactions Found" });
  }
  // var open = {};
  // user_interactions.forEach(function(interaction) {
  //   open[interaction.order_ID] = false;
  //   process.env.DEBUG === "true" && console.log('Events ' + interaction.events);
  // });

  const mapped_user_interactions = user_interactions.map(user_interaction => ({
    key: user_interaction.id,
    kiosk_ID: user_interaction.kiosk_ID,
    date_created: user_interaction.date_created,
    events: getSignedURL(user_interaction.events),
    order_ID: user_interaction.order_ID
  }));

  const ret_interactions = {
    mapped_user_interactions: mapped_user_interactions,
    // open: open
  }

  process.env.DEBUG === "true" && console.timeEnd('User_interaction');
  return res.json(ret_interactions);
}


function downloadFileFromS3(fileName) {
  
}
router.post("/interaction_video/", (req, res) => {
  const s3 = new AWS.S3({ params: { Bucket: bucketName }});
  const events_link = req.body.s3URL.events;
  process.env.DEBUG === "true" && console.log("Events link  " + JSON.stringify(events_link));
  process.env.DEBUG === "true" && console.log(events_link)
  // cons

  // s3.getObject(
  //   { Bucket: bucketName, Key: events_link }, (d, e) => {
  //     process.env.DEBUG === "true" && console.log("D is: " + JSON.stringify(d));
  //   }
  // );


  var params = {
    Bucket: bucketName, 
    Key: events_link
   };
   s3.getObject(params, function(err, data) {
     if (err) process.env.DEBUG === "true" && console.log(err, err.stack); // an error occurred
     else     process.env.DEBUG === "true" && console.log(data);           // successful response
     /*
     data = {
      AcceptRanges: "bytes", 
      ContentLength: 3191, 
      ContentType: "image/jpeg", 
      ETag: "\"6805f2cfc46c0f04559748bb039d69ae\"", 
      LastModified: <Date Representation>, 
      Metadata: {
      }, 
      TagCount: 2, 
      VersionId: "null"
     }
     */
   });
});
router.post("/interactions/", (req, res) => {
    const selected_kiosk = req.body.kiosk;
    const new_interaction = req.body.new_interaction;
    process.env.DEBUG === "true" && console.log('interactions called');
  process.env.DEBUG === "true" && console.log("selected kiosk is " + selected_kiosk);
  process.env.DEBUG === "true" && console.time("User_interaction");
  if (new_interaction) {
      process.env.DEBUG === "true" && console.log("This is a new interaction...saving...");
      const newUserInteraction = new UserInteraction({
          kiosk_ID: req.body.kiosk_ID,
          events: req.body.events
      });
      newUserInteraction.save().then((user_interaction) => res.json(user_interaction)).catch((err) => process.env.DEBUG === "true" && console.log(err));
  }
  else if (selected_kiosk == 'All') {
    // UserInteraction.find().sort({'date_created': -1}).then((user_interactions) => {
    //   mapUserInteractions(user_interactions, res);
    // });

    // Optimized
    UserInteraction.find().select({'date_created': -1, 'kiosk_ID': 1, 'order_ID': 1, 'events': 1}).lean().then((user_interactions) => {
      mapUserInteractions(user_interactions, res);
    });
  } else {
    UserInteraction.find({"kiosk_ID": selected_kiosk }).select({'date_created': -1, 'kiosk_ID': 1, 'order_ID': 1, 'events': 1}).lean().then((user_interactions) => {
      mapUserInteractions(user_interactions, res);
    });


    // // Optimized
    UserInteraction.find({"kiosk_ID": selected_kiosk }).sort({'date_created': -1}).then((user_interactions) => {
      mapUserInteractions(user_interactions, res);
    });
  }
  
});

module.exports = router;
