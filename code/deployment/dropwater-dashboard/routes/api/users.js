const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
const { v4: uuidv4 } = require('uuid');
const qrcode = require('qrcode');
var nodemailer = require('nodemailer');



// Load input validation
const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");
// Load User model
const User = require("../../models/User");
// const { logoutUser } = require("../../client/src/actions/authActions");

const Company = require("../../models/Company");


emailQRCode = (uuid, userEmail) => {
  
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'dropwater.menlo@gmail.com',
      pass: 'drop@35077'
    }
  });
  run();
  // send qr code as email attachment
  async function run() {
    qrcode.toDataURL(uuid)
    .then(url => {
      var html = `<!DOCTYPE html><html lang="en"></html><head><title>QRCode</title></head><body>`
      var svg = html + `<img src="` + url.split("base64,")[1] + `"/></body></html>`
      var mailOptions = {
        from: 'dropwater.menlo@gmail.com',
        to: userEmail,
        subject: 'DropWater QRCode',
        text: "Welcome to DropWater Kiosk! Please find attached your QRCode. Scan it to access Maintenance mode in your assigned Company's DropWater Station(s).",
        // html: svg, 
        attachments: [
          {   // encoded string as an attachment
            filename: 'qrcode.png',
            // content: url.split("base64,")[1],
            // encoding: 'base64'
            path: url
          }
        ]
      };
      
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          process.env.DEBUG === "true" && console.log(error);
        } else {
          process.env.DEBUG === "true" && console.log("Email sent to " + userEmail + " with response " + info.response);
        }
      });
    })
    .catch(err => {
      console.error(err)
    });

    
  }
};
// @route POST api/users/register
// @desc Register user
// @access Public
router.post("/register", (req, res) => {
  // Form validation
  const { errors, isValid } = validateRegisterInput(req.body);
  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }
  User.findOne({ email: req.body.email }).then((user) => {
    if (user) {
      return res.status(400).json({ email: "Email already exists" });
    } else {
      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        uuid: uuidv4(),
      });
      // Hash password before saving in database
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser
            .save()
            .then((user) => res.json(user))
            .catch((err) => process.env.DEBUG === "true" && console.log(err));


            // this.emailQRCode(newUser.uuid);
            // emailQRCode(newUser.uuid, newUser.email);
        });
      });
    }
  });
});

// @route POST api/users/login
// @desc Login user and return JWT token
// @access Public
router.post("/login", (req, res) => {
  // Form validation
  const { errors, isValid } = validateLoginInput(req.body);
  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }
  const email = req.body.email;
  const password = req.body.password;
  // Find user by email
  User.findOne({ email }).then((user) => {
    // Check if user exists
    if (!user) {
      return res.status(404).json({ emailnotfound: "Email not found" });
    }
    // Check password
    bcrypt.compare(password, user.password).then((isMatch) => {
      if (isMatch) {
        // User matched
        // Create JWT Payload
        const payload = {
          id: user.id,
          name: user.name,
          uuid: user.uuid,
          role: user.role,
          email: user.email,
        };
        // Sign token
        jwt.sign(
          payload,
          keys.secretOrKey,
          {
            expiresIn: 7200, // 20 minutes in seconds
          },
          (err, token) => {
            res.json({
              success: true,
              token: "Bearer " + token,
              uuid: user.uuid
            });
          }
        );
      } else {
        return res
          .status(400)
          .json({ passwordincorrect: "Password incorrect" });
      }
    });
  });
});


router.get("/all_users", (req, res) => {
  // Find user by email
  User.find().then((users) => {
    // Check if user exists
    if (!users) {
      return res.status(404).json({ usersnotfound: "Users not found" });
    }
    // process.env.DEBUG === "true" && console.log("These are the users in router: " + JSON.stringify(users));
    const payload = {
      users: users,
    };

    const mapped_users = users.map(user => ({
      key: user.uuid,
      name: user.name,
      uuid: user.uuid,
      email: user.email
    }))
    // return res.json(payload);
    return res.json(mapped_users);
  });
});


router.post("/email_qrcode/", (req, res) => {
  // Find user by email
  User.findOne({ uuid: req.body.uuid }).then((user) => {
    // Check if user exists
    if (!user) {
      return res.status(404).json({ uuidnotfound: "User not found" });
    }

    emailQRCode(user.uuid, user.email);
    
    // return res.json(payload);
    return res.json(user);
  });
});

router.post("/logout", (req, res) => {
  // logoutUser();
  process.env.DEBUG === "true" && console.log("log out user called");
});


router.post("/validate", (req, res) => {
  // // Form validation
  // const { errors, isValid } = validateLoginInput(req.body);
  // // Check validation
  // if (!isValid) {
  //   return res.status(400).json(errors);
  // }
  const email = req.body.email;
  const password = req.body.password;

  console.log("Email is: " + email + " pwd: " + password)
  console.log("Called: " + email + " " + password);
  if (typeof email === 'undefined' || typeof password === 'undefined') {  // undefined, return error
    return res
          .status(400)
          .json({ invalidcredentials: "Email or password empty" });
  }
  // Find user by email
  User.findOne({ email }).then((user) => {
    // Check if user exists
    if (!user) {
      return res.status(404).json({ emailnotfound: "Email not found" });
    }
    // Check password
    bcrypt.compare(password, user.password).then((isMatch) => {
      if (isMatch) {
        console.log("Is a match");
        // User matched
        // Create JWT Payload
        const payload = {
          id: user.id,
          name: user.name,
          email: user.email,
          uuid: user.uuid,
          role: user.role,
          isValid: true,
        };
        // console.log("Returning..." + JSON.stringify(user));
        return res.json(payload)
      } else {
        console.log("Not a match")
        return res
          .status(400)
          .json({ passwordincorrect: "Password incorrect" });
      }
    });
  });
});


function getUserProfile(req, res) {
  let uuid = req.body.uuid;
  // Find user by email
  User.findOne({ uuid }).then((user) => {
    // Check if user exists
    if (!user) {
      return res.status(404).json({ usersnotfound: "Users not found" });
    }

    console.log("User is: "+ JSON.stringify(user));
    Company.findOne({ company_ID: user.company_ID }).lean().then((company) => {
        // process.env.DEBUG === "true" && console.log("These are the users in router: " + JSON.stringify(users));
      const payload = {
        user: user,
        company: company
      };
      // return res.json(payload);
      return res.json(payload);
    });
  
  });
}

function updateGeneralUserInfo(req, res) {
  let uuid = req.body.user.uuid;

  let user = req.body.user;

  console.log(user);
  // return;
  // Find user by email
  User.findOneAndUpdate(
      { uuid: uuid }, // find a document with that filter
      user, // document to insert when nothing was found
      {upsert: false, new: true, runValidators: true, useFindAndModify: false }, // options
      function (err, doc) { // callback
          if (err) {
              // handle error
            //   winston.log("debug", "Error updating module status to " );
              console.log("Error is: "  + JSON.stringify(err));
              return res.status(404).json({ usersnotfound: "Users not found" });
          } else {
              // handle document
              console.log("debug", "Successfully updated account" + doc.company_ID);
              // return res.json(doc);
              Company.findOne({ company_ID: doc.company_ID }).lean().then((company) => {
                // process.env.DEBUG === "true" && console.log("These are the users in router: " + JSON.stringify(users));
              const payload = {
                user: doc,
                company: company
              };
              // return res.json(payload);
              console.log("returning payload: " + JSON.stringify(payload));
              return res.json(payload);
            });
          }
        //   mongoose.connection.close();
      }
  );
}

function updateCompanyInfo(req, res) {
  let company_ID = req.body.company.company_ID;

  Company.findOneAndUpdate(
      { company_ID: company_ID }, // find a document with that filter
      req.body.company, // document to insert when nothing was found
      {upsert: false, new: true, runValidators: true, useFindAndModify: false }, // options
      function (err, doc) { // callback
          if (err) {
              // handle error
            //   winston.log("debug", "Error updating module status to " );
              console.log("Error is: "  + JSON.stringify(err));
              return res.status(404).json({ usersnotfound: "Users not found" });
          } else {
              // handle document
              console.log("debug", "Successfully updated account" + doc.company_ID);
              // return res.json(doc);
              User.findOne({ uuid: req.body.user.uuid }).lean().then((user) => {
                // process.env.DEBUG === "true" && console.log("These are the users in router: " + JSON.stringify(users));
              const payload = {
                user: user,
                company: doc
              };
              // return res.json(payload);
              console.log("returning payload: " + JSON.stringify(payload));
              return res.json(payload);
            });
          }
        //   mongoose.connection.close();
      }
  );
}


function updateUserPassword(req, res) {
  let user = req.body.user;
  let password = req.body.password;
  console.log("PWD: " + password);
  // Hash password before saving in database
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(password, salt, (err, hash) => {
      if (err) throw err;
      user.password = hash;

      console.log("Hash:" + hash);
    });
  });


  // Find user by email
  User.findOneAndUpdate(
      {uuid: user.uuid }, // find a document with that filter
      user, // document to insert when nothing was found
      {upsert: true, new: true, runValidators: true, useFindAndModify: false }, // options
      function (err, doc) { // callback
          if (err) {
              // handle error
            //   winston.log("debug", "Error updating module status to " );
              console.log("Error is: "  + JSON.stringify(err));
              return res.status(404).json({ usersnotfound: "Users not found" });
          } else {
              // handle document
              console.log("debug", "Successfully updated user pwd");
               Company.findOne({ company_ID: user.company_ID }).lean().then((company) => {
                // process.env.DEBUG === "true" && console.log("These are the users in router: " + JSON.stringify(users));
              const payload = {
                user: doc,
                company: company
              };
              // return res.json(payload);
              console.log("Saved new password.");
              return res.json(payload);
            });
          }
        //   mongoose.connection.close();
      }
  );
}

router.post("/account", (req, res) => {
  console.log("User prof called");
  let type = req.body.type;

  if (type === 'Load') {
    console.log("Getting user prof.");
    getUserProfile(req, res)
  } else if (type === 'User') {
    console.log("updating user prof.");
    updateGeneralUserInfo(req, res)
  } else if (type === 'Company') {
    updateCompanyInfo(req, res);
  } else if (type === 'Password') {
    updateUserPassword(req, res);
  }
  
});



router.post("/account/save", (req, res) => {
  console.log("User account save called");
  let uuid = req.body.uuid;

  let user = {
    uuid: uuid,
    name: req.body.name,
    email: req.body.email,
    phone_number: req.body.phone_number,
    country: req.body.country,
    state: req.body.state,
    city: req.body.city,
  }

  console.log(user);
  // return;
  // Find user by email
  User.findOneAndUpdate(
      { uuid: uuid }, // find a document with that filter
      user, // document to insert when nothing was found
      {upsert: false, new: true, runValidators: true, useFindAndModify: false }, // options
      function (err, doc) { // callback
          if (err) {
              // handle error
            //   winston.log("debug", "Error updating module status to " );
              console.log("Error is: "  + JSON.stringify(err));
              return res.status(404).json({ usersnotfound: "Users not found" });
          } else {
              // handle document
              console.log("debug", "Successfully updated account");
              // return res.json(doc);
              Company.findOne({ company_ID: user.company_ID }).lean().then((company) => {
                // process.env.DEBUG === "true" && console.log("These are the users in router: " + JSON.stringify(users));
              const payload = {
                user: doc,
                company: company
              };
              // return res.json(payload);
              return res.json(payload);
            });
          }
        //   mongoose.connection.close();
      }
  );
});



router.post("/account/password", (req, res) => {
  console.log("User account save called");
  let uuid = req.body.uuid;

  
  let user = {
    password: req.body.password,
  }


  // Hash password before saving in database
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) throw err;
      user.password = hash;
    });
  });


  // Find user by email
  User.findOneAndUpdate(
      {uuid: req.body.uuid }, // find a document with that filter
      user, // document to insert when nothing was found
      {upsert: true, new: true, runValidators: true, useFindAndModify: false }, // options
      function (err, doc) { // callback
          if (err) {
              // handle error
            //   winston.log("debug", "Error updating module status to " );
              console.log("Error is: "  + JSON.stringify(err));
              return res.status(404).json({ usersnotfound: "Users not found" });
          } else {
              // handle document
              console.log("debug", "Successfully updated user pwd");
               Company.findOne({ company_ID: user.company_ID }).lean().then((company) => {
                // process.env.DEBUG === "true" && console.log("These are the users in router: " + JSON.stringify(users));
              const payload = {
                user: doc,
                company: company
              };
              // return res.json(payload);
              return res.json(payload);
            });
          }
        //   mongoose.connection.close();
      }
  );
});

module.exports = router;
