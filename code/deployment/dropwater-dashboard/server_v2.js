const throng = require('throng');
const WORKERS = process.env.WEB_CONCURRENCY || 1;

throng(
	{
		workers: WORKERS,
		lifetime: Infinity
	},
	start
);

function start() {
	const mongoose = require('mongoose');
	const bodyParser = require('body-parser');
	const passport = require('passport');
	const users = require('./routes/api/users');
	const orders = require('./routes/api/orders');
	const overview = require('./routes/api/overview');
	const inventory = require('./routes/api/inventory');
	const kiosk = require('./routes/api/kiosk');
	const miscTests = require('./routes/api/misc_tests');
	const reports = require('./routes/api/reports');
	const user_interactions = require('./routes/api/user_interactions');
	const crypto = require('crypto');
	const express = require('express');
	const app = express();
	const path = require('path');
	
	// app.use(sslRedirect);

	require('dotenv').config();
	require('./config/passport')(passport);

	// Bodyparser middleware
	app.use(
		bodyParser.urlencoded({
			extended: false
		})
	);

	// ... other app.use middleware for heroku
	app.use(express.static(path.join(__dirname, 'client', 'build')));

	app.use(bodyParser.json());

	var db = '';
	var db_in_use = 'Dev DB';
	if (process.env.ENV === 'dev') {
		db = process.env.STAGING_DB;
	} else {
		db = process.env.PROD_DB;
		db_in_use = 'Prod DB';
	}
	// Connect to MongoDB
	mongoose
		.connect(db, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false })
		.then(() => process.env.DEBUG === 'true' && console.log('MongoDB successfully connected to ' + db_in_use))
		.catch((err) => process.env.DEBUG === 'true' && console.log(err));

	// Passport middleware
	app.use(passport.initialize());
	// Passport config

	// Routes
	app.use('/api/users', users);
	app.use('/api/orders', orders);
	app.use('/api/overview', overview);
	app.use('/api/inventory', inventory);
	app.use('/api/kiosk', kiosk);
	app.use('/api/user_interactions', user_interactions);
	app.use('/api/misc_tests', miscTests);
	app.use('/api/reports', reports);

	const port = process.env.PORT || 5000; // process.env.port is Heroku's port if you choose to deploy the app there

	// heroku
	app.get('*', (req, res) => {
		res.sendFile(path.join(__dirname, 'client', 'build', 'index.html'));
	});

	app.listen(port, () => process.env.DEBUG === 'true' && console.log(`Server up and running on port ${port} !`));
}
